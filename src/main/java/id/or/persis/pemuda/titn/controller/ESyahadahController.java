package id.or.persis.pemuda.titn.controller;

import static org.springframework.http.ResponseEntity.ok;

import id.or.persis.pemuda.titn.payload.EsyahadahRequestDto;
import id.or.persis.pemuda.titn.payload.ParticipantResponseDto;
import id.or.persis.pemuda.titn.service.ESyahadahService;
import id.or.persis.pemuda.titn.utils.BaseResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * Project: titn-service
 * <p>
 * User: hasan.sanusi
 * Email: hasan.sanusi@dansmultipro.com
 * Telegram: @hasansanusi
 * Date: 05/10/24
 * Time: 19.58
 * <p>
 * Created with IntelliJ IDEA
 */
@RestController
@Tag(name = "E-Syahadah", description = "Endpoint for e-syahadah for Participant")
@RequestMapping(value = Endpoint.E_SYAHADAH)
@Slf4j
@AllArgsConstructor
public class ESyahadahController {
    private final ESyahadahService eSyahadahService;

    @PostMapping(value = "/event")
    @Operation(
            summary = "Generate and Send E-Syahadah kompetisi",
            description = "Generate and Send E-Syahadah kompetisi",
            tags = {"E-Syahadah"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ParticipantResponseDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    public ResponseEntity<BaseResponse<String>> create(
            @RequestBody @Valid EsyahadahRequestDto esyahadahRequestDto, HttpServletRequest request) {
        BaseResponse<String> baseResponse = eSyahadahService.generateEsyahadah(esyahadahRequestDto, request);
        return ok(baseResponse);
    }

    @PostMapping(value = "/{paticipantId}")
    @Operation(
            summary = "Generate and Send E-Syahadah Peserta Or Panitia",
            description = "Generate and Send E-Syahadah kompetisi",
            tags = {"E-Syahadah"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = String.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    public ResponseEntity<BaseResponse<String>> generatePeserta(
            HttpServletRequest request, @PathVariable String paticipantId) {
        BaseResponse<String> baseResponse = eSyahadahService.generateEsyahadahPeserta(paticipantId, request);
        return ok(baseResponse);
    }

    @PostMapping(value = "/recap/{recapId}")
    @Operation(
            summary = "Generate and Send E-Syahadah Peserta Or Panitia",
            description = "Generate and Send E-Syahadah kompetisi",
            tags = {"E-Syahadah"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = String.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    public ResponseEntity<BaseResponse<String>> generateByRecapId(
            HttpServletRequest request, @PathVariable String recapId) {
        BaseResponse<String> baseResponse = eSyahadahService.generateEsyahadahByRecapId(recapId, request);
        return ok(baseResponse);
    }

    @PostMapping(path = "/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @Operation(
            summary = "Generate and Send E-Syahadah Upload",
            description = "Generate and Send E-Syahadah Upload",
            tags = {"E-Syahadah"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = String.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    public ResponseEntity<BaseResponse<String>> upload(
            HttpServletRequest request, @RequestPart("file") MultipartFile file) {
        BaseResponse<String> baseResponse = eSyahadahService.generateUpload(file, request);
        return ok(baseResponse);
    }
}
