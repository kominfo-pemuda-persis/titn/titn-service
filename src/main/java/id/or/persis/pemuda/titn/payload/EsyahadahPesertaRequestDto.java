package id.or.persis.pemuda.titn.payload;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Project: titn-service
 * <p>
 * User: hasan.sanusi
 * Email: hasan.sanusi@dansmultipro.com
 * Telegram: @hasansanusi
 * Date: 05/10/24
 * Time: 20.04
 * <p>
 * Created with IntelliJ IDEA
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class EsyahadahPesertaRequestDto {
    private String participantId;
}
