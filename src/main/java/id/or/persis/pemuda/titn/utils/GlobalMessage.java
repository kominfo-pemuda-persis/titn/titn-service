package id.or.persis.pemuda.titn.utils;

public enum GlobalMessage {
    SUCCESS("SUCCESS", "SUCCESS"),
    ERROR("ERROR", "ERROR");

    public String code;
    public String message;

    GlobalMessage(String code, String message) {
        this.code = code;
        this.message = message;
    }
}
