package id.or.persis.pemuda.titn.service;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import java.awt.image.BufferedImage;
import org.springframework.stereotype.Service;

/**
 * Created by IntelliJ IDEA. Project : titn-service User: hendisantika Email:
 * hendisantika@gmail.com Telegram : @hendisantika34 Date: 5/23/23 Time: 13:22
 * To change this template use File | Settings | File Templates.
 */
@Service
public class QRCodeService {
    public BufferedImage generateQRCode(String text, int width, int height) throws Exception {
        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        BitMatrix bitMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, width, height);
        return MatrixToImageWriter.toBufferedImage(bitMatrix);
    }
}
