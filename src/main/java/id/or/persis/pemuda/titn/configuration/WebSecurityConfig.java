package id.or.persis.pemuda.titn.configuration;

import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

/**
 * Created by IntelliJ IDEA. Project : titn-service User: hendisantika Email:
 * hendisantika@gmail.com Telegram : @hendisantika34 Date: 6/3/24 Time: 21:11 To
 * change this template use File | Settings | File Templates.
 */
@Configuration
public class WebSecurityConfig {

    @Autowired
    private AppConfig appConfig;

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        String[] listMethods = appConfig.getWhitelistCrossMethod().split("\\|");
        String[] listOrigins = appConfig.getWhitelistCrossUrl().split("\\|");
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowCredentials(false);
        configuration.setAllowedOrigins(Arrays.asList(listOrigins));
        configuration.setAllowedMethods(Arrays.asList(listMethods));
        configuration.setAllowedHeaders(List.of("*"));
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }

    @Bean
    public SecurityFilterChain configure(HttpSecurity http, JwtAuthenticationFilter jwtAuthenticationFilter)
            throws Exception {
        String[] whitelistUrl = appConfig.getWhitelistJwtPathUrl().split("\\|");
        http.authorizeHttpRequests(authorize -> authorize
                        .requestMatchers(whitelistUrl)
                        .permitAll()
                        .anyRequest()
                        .authenticated())
                .addFilterBefore(jwtAuthenticationFilter, UsernamePasswordAuthenticationFilter.class)
                .csrf(csrf -> csrf.ignoringRequestMatchers("/api/**"))
                .cors(config -> {
                    config.configurationSource(corsConfigurationSource());
                });
        return http.build();
    }
}
