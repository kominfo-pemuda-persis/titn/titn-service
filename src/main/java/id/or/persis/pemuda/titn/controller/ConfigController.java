package id.or.persis.pemuda.titn.controller;

import static org.springframework.http.ResponseEntity.ok;

import id.or.persis.pemuda.titn.payload.ConfigurationResponseDto;
import id.or.persis.pemuda.titn.service.ConfigurationService;
import id.or.persis.pemuda.titn.utils.BaseResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Project: titn-service
 * <p>
 * User: hasan.sanusi
 * Email: hasan.sanusi@dansmultipro.com
 * Telegram: @hasansanusi
 * Date: 11/08/24
 * Time: 22.49
 * <p>
 * Created with IntelliJ IDEA
 */
@RestController
@Tag(name = "Configuration", description = "Endpoint for managing Configuration")
@RequestMapping(value = Endpoint.CONFIGURATION)
@Slf4j
@AllArgsConstructor
public class ConfigController {
    private final ConfigurationService configurationService;

    @GetMapping(value = "")
    @Operation(
            summary = "Get Configuration Expired Registration",
            description = "Get Configuration Expired Registration.",
            tags = {"Configuration"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ConfigurationResponseDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    public ResponseEntity<BaseResponse<ConfigurationResponseDto>> getExpiredRegistration() {

        BaseResponse<ConfigurationResponseDto> response = configurationService.getExpiredRegistration();
        return ok(response);
    }
}
