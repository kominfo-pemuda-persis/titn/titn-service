package id.or.persis.pemuda.titn.controller;

import static id.or.persis.pemuda.titn.utils.QRCodeGenerator.readQR;
import static id.or.persis.pemuda.titn.utils.QRCodeGenerator.writeQR;

import com.google.zxing.WriterException;
import id.or.persis.pemuda.titn.payload.CreateAccountRequest;
import java.io.IOException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by IntelliJ IDEA. Project : titn-service User: hendisantika Email:
 * hendisantika@gmail.com Telegram : @hendisantika34 Date: 5/21/23 Time: 13:34
 * To change this template use File | Settings | File Templates.
 */
@Controller
@Slf4j
public class QRCodeController {

    @PostMapping("/createAccount")
    public String createNewAccount(@ModelAttribute("request") CreateAccountRequest request, Model model)
            throws WriterException, IOException {
        String qrCodePath = writeQR(request);
        model.addAttribute("code", qrCodePath);
        return "QRCode";
    }

    @GetMapping("/readQR")
    public String verifyQR(@RequestParam("qrImage") String qrImage, Model model) throws Exception {
        model.addAttribute("content", readQR(qrImage));
        model.addAttribute("code", qrImage);
        return "QRCode";
    }
}
