package id.or.persis.pemuda.titn.service;

import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.*;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring6.SpringTemplateEngine;

@Service
@RequiredArgsConstructor
public class MailService {
    private final JavaMailSender javaMailSender;

    @Value("classpath:/static/images/logo_titn2024_lite.png")
    private Resource resourceFile;

    private final SpringTemplateEngine templateEngine;

    public void sendMail(Map<String, Object> content, String subject, String to, String template, String from)
            throws MessagingException {
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(
                message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, StandardCharsets.UTF_8.name());
        helper.setSubject(subject);
        helper.setFrom(from);
        helper.setTo(to);
        Context context = new Context();
        context.setVariables(content);
        String html = templateEngine.process(template, context);
        helper.setText(html, true);
        helper.addInline("logo_titn2024_lite", resourceFile);
        javaMailSender.send(message);
    }

    public void sendMailWithQRCode(
            Map<String, Object> content,
            String subject,
            String to,
            String template,
            String from,
            ByteArrayResource imageQRSource,
            String imageCid)
            throws MessagingException {
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(
                message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, StandardCharsets.UTF_8.name());
        helper.setSubject(subject);
        helper.setFrom(from);
        helper.setTo(to);
        Context context = new Context();
        context.setVariables(content);
        String html = templateEngine.process(template, context);
        helper.setText(html, true);
        helper.addInline("logo_titn2024_lite", resourceFile);
        helper.addInline("qrcode", imageQRSource, "image/png");
        helper.addAttachment(imageCid, new ByteArrayResource(imageCid.getBytes(StandardCharsets.UTF_8)));
        javaMailSender.send(message);
    }

    public void sendMailWithQRCodeAndNameTag(
            Map<String, Object> content,
            String subject,
            String to,
            String template,
            String from,
            ByteArrayResource nameTagPdf,
            ByteArrayResource nameTag,
            String name)
            throws MessagingException {
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(
                message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, StandardCharsets.UTF_8.name());
        helper.setSubject(subject);
        helper.setFrom(from);
        helper.setTo(to);
        Context context = new Context();
        context.setVariables(content);
        String html = templateEngine.process(template, context);
        helper.setText(html, true);
        helper.addInline("logo_titn2024_lite", resourceFile);
        helper.addInline("nameTagOld", nameTag, "image/png");
        helper.addAttachment(name.concat(".png"), nameTag);
        helper.addAttachment(name.concat(".pdf"), nameTagPdf);

        javaMailSender.send(message);
    }

    public void sendMailWithQRCodeAndNameTag(
            Map<String, Object> content,
            String subject,
            String to,
            String template,
            String from,
            ByteArrayResource imageQRSource,
            ByteArrayResource nameTag,
            ByteArrayResource nameTagOld,
            String name)
            throws MessagingException {
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(
                message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, StandardCharsets.UTF_8.name());
        helper.setSubject(subject);
        helper.setFrom(from);
        helper.setTo(to);
        Context context = new Context();
        context.setVariables(content);
        String html = templateEngine.process(template, context);
        helper.setText(html, true);
        helper.addInline("logo_titn2024_lite", resourceFile);
        helper.addInline("qrcode", imageQRSource, "image/png");
        helper.addInline("nameTag", nameTag, "image/png");
        helper.addInline("nameTagOld", nameTagOld, "image/png");
        helper.addAttachment("BACK_".concat(name), nameTag);
        helper.addAttachment(name, imageQRSource);
        helper.addAttachment("NEW_".concat(name), nameTagOld);
        javaMailSender.send(message);
    }

    public void sendMailEsyahadah(
            Map<String, Object> content,
            String subject,
            String to,
            String template,
            String from,
            ByteArrayResource eSyahadah,
            String name)
            throws MessagingException {
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(
                message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, StandardCharsets.UTF_8.name());
        helper.setSubject(subject);
        helper.setFrom(from);
        helper.setTo(to);
        Context context = new Context();
        context.setVariables(content);
        String html = templateEngine.process(template, context);
        helper.setText(html, true);
        helper.addInline("logo_titn2024_lite", resourceFile);
        helper.addAttachment(name.concat(".pdf"), eSyahadah);
        javaMailSender.send(message);
    }
}
