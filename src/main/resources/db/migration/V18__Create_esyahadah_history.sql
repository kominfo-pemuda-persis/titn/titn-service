create table t_syahadah_history
(
    id                     bigint auto_increment
        primary key,
    npa                    varchar(10)  null,
    juara                  varchar(100) null,
    contest                varchar(280) null,
    competition            varchar(200) null,
    event                  varchar(200) null,
    send_email_status      varchar(50)  null,
    send_email_status_desc varchar(100) null,
    created_at             timestamp(6) null,
    created_by             varchar(100) null,
    updated_at             timestamp(6) null,
    updated_by             varchar(100) null,
    version                int          null,
    participant_id         varchar(100) null
);


