alter table t_participant
    modify pc_code varchar(30) null;

alter table t_participant
    modify pd_code varchar(30) null;

alter table t_participant
    modify pw_code varchar(30) null;

alter table t_participant_recap
    modify pc_code varchar(30) null;

alter table t_participant_recap
    modify pd_code varchar(30) null;

alter table t_participant_recap
    modify pw_code varchar(30) null;