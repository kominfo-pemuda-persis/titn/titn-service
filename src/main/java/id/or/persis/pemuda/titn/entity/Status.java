package id.or.persis.pemuda.titn.entity;

/**
 * Project: titn-service
 * <p>
 * User: hasan.sanusi Email: hasan.sanusi@dansmultipro.com
 * Telegram: @hasansanusi Date: 09/06/24 Time: 12.53
 * <p>
 * Created with IntelliJ IDEA
 */
public enum Status {
    SUBMITTED("SUBMITTED", "SUBMITTED"),
    APPROVED("APPROVED", "APPROVED"),
    CHECKED_IN("CHECKED_IN", "CHECKED IN"),
    REJECTED("REJECTED", "REJECTED");

    public final String code;
    public final String desc;

    Status(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
