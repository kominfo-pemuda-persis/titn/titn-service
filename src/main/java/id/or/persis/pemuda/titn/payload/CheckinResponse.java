package id.or.persis.pemuda.titn.payload;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import id.or.persis.pemuda.titn.entity.Status;
import id.or.persis.pemuda.titn.utils.TimeGmtUtils;
import java.time.LocalDateTime;
import java.util.Optional;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * Project: titn-service
 * <p>
 * User: hasan.sanusi
 * Email: hasan.sanusi@dansmultipro.com
 * Telegram: @hasansanusi
 * Date: 29/06/24
 * Time: 09.50
 * <p>
 * Created with IntelliJ IDEA
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class CheckinResponse {
    private String participantId;
    private String fullName;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime checkedAt;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime spinnerAt;

    private Status status;

    public LocalDateTime getSpinnerAt() {
        return Optional.ofNullable(spinnerAt)
                .map(TimeGmtUtils::convertToGmtPlus7)
                .orElse(spinnerAt);
    }

    public LocalDateTime getCheckedAt() {
        return Optional.ofNullable(checkedAt)
                .map(TimeGmtUtils::convertToGmtPlus7)
                .orElse(checkedAt);
    }
}
