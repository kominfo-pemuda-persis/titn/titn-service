package id.or.persis.pemuda.titn.payload;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import id.or.persis.pemuda.titn.entity.ClothSize;
import id.or.persis.pemuda.titn.utils.EventType;
import id.or.persis.pemuda.titn.utils.TimeGmtUtils;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.springframework.format.annotation.DateTimeFormat;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class ParticipantResponseDto {
    private String participantId;
    private String fullName;
    private String npa;
    private String email;
    private String phone;
    private String nik;
    private List<EventDetail> eventDetails;
    private ClothSize clothesSize;
    private String pcCode;
    private String pcName;
    private String pdCode;
    private String pdName;
    private String pwCode;
    private String pwName;
    private String status;
    private String photo;
    private String emailNotificationStatus;
    private String emailNotificationStatusDesc;
    private String waNotificationStatus;
    private String walNotificationStatusDesc;
    protected String updatedBy;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    protected LocalDateTime updatedAt;

    protected String createdBy;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    protected LocalDateTime createdAt;

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class EventDetail {
        private String id;
        private String name;
        private String description;
        private EventType type;
    }

    public LocalDateTime getCreatedAt() {
        return Optional.ofNullable(createdAt)
                .map(TimeGmtUtils::convertToGmtPlus7)
                .orElse(createdAt);
    }

    public LocalDateTime getUpdatedAt() {
        return Optional.ofNullable(updatedAt)
                .map(TimeGmtUtils::convertToGmtPlus7)
                .orElse(updatedAt);
    }
}
