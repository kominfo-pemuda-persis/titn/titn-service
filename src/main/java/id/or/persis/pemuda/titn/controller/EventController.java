package id.or.persis.pemuda.titn.controller;

import static org.springframework.http.ResponseEntity.ok;

import id.or.persis.pemuda.titn.payload.EventRequestDto;
import id.or.persis.pemuda.titn.payload.EventResponseDto;
import id.or.persis.pemuda.titn.service.EventService;
import id.or.persis.pemuda.titn.utils.BaseResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Tag(name = "Event", description = "Endpoint for managing Event")
@RequestMapping(value = Endpoint.EVENTS)
@Slf4j
@AllArgsConstructor
public class EventController {
    private final EventService eventService;
    private static final int DEFAULT_PAGE_SIZE = 10;

    @PostMapping
    @Operation(
            summary = "Add New Event Data",
            description = "Add New Event Data.",
            tags = {"Event"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = EventResponseDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    public ResponseEntity<BaseResponse<EventResponseDto>> create(@RequestBody @Valid EventRequestDto eventRequestDto) {
        BaseResponse<EventResponseDto> baseResponse = eventService.create(eventRequestDto);
        return ok(baseResponse);
    }

    @GetMapping
    @Operation(
            summary = "Get all Event Data",
            description = "Get all Event Data.",
            tags = {"Event"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = EventResponseDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    public ResponseEntity<BaseResponse<Page<EventResponseDto>>> getEvents(
            @Parameter(description = "The size of the page to be returned")
                    @RequestParam(required = false, defaultValue = "10")
                    Integer pageSize,
            @Parameter(description = "Zero-based page index") @RequestParam(required = false, defaultValue = "0")
                    Integer pageNo,
            @Parameter(description = "Zero-based page sort") @RequestParam(required = false, defaultValue = "createdAt")
                    String sortBy) {
        if (pageSize == null) {
            pageSize = DEFAULT_PAGE_SIZE;
        }
        if (pageNo == null) {
            pageNo = 0;
        }
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
        BaseResponse<Page<EventResponseDto>> listBaseResponse = eventService.getAllEvent(paging);
        return ok(listBaseResponse);
    }

    @PutMapping
    @Operation(
            summary = "Update Event Data",
            description = "Update Event Data.",
            tags = {"Event"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = EventResponseDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    public ResponseEntity<BaseResponse<EventResponseDto>> update(@RequestBody @Valid EventRequestDto eventRequestDto) {
        BaseResponse<EventResponseDto> baseResponse = eventService.update(eventRequestDto);
        return ok(baseResponse);
    }
}
