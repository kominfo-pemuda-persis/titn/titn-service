package id.or.persis.pemuda.titn.utils;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

/**
 * Project: titn-service
 * <p>
 * User: hasan.sanusi
 * Email: hasan.sanusi@dansmultipro.com
 * Telegram: @hasansanusi
 * Date: 22/09/24
 * Time: 20.16
 * <p>
 * Created with IntelliJ IDEA
 */
public class TimeGmtUtils {
    public static LocalDateTime convertToGmtPlus7(LocalDateTime localDateTime) {
        ZoneId gmtPlus7 = ZoneId.of("Asia/Jakarta");
        ZonedDateTime zonedDateTime = localDateTime.atZone(ZoneId.systemDefault());
        ZonedDateTime gmtPlus7Time = zonedDateTime.withZoneSameInstant(gmtPlus7);
        return gmtPlus7Time.toLocalDateTime();
    }
}
