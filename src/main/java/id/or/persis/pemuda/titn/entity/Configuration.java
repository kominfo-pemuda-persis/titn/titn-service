package id.or.persis.pemuda.titn.entity;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.SuperBuilder;

/**
 * Project: titn-service
 * <p>
 * User: hasan.sanusi
 * Email: hasan.sanusi@dansmultipro.com
 * Telegram: @hasansanusi
 * Date: 11/08/24
 * Time: 22.57
 * <p>
 * Created with IntelliJ IDEA
 */
@Entity
@Table(name = "t_configuration")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class Configuration extends Updatable {
    @Id
    private String id;

    private String name;

    private String description;

    private String value;
}
