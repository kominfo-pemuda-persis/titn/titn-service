package id.or.persis.pemuda.titn.payload;

import id.or.persis.pemuda.titn.entity.Status;
import jakarta.servlet.http.HttpServletRequest;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Pageable;

/**
 * Project: titn-service
 * <p>
 * User: hasan.sanusi
 * Email: hasan.sanusi@dansmultipro.com
 * Telegram: @hasansanusi
 * Date: 22/09/24
 * Time: 19.27
 * <p>
 * Created with IntelliJ IDEA
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FilterParticipantDto {
    private String keyword;
    private Boolean isAnggota;
    private Boolean isSpinner;
    private Status status;
    private Pageable paging;
    private HttpServletRequest request;
    private String pd;
    private String pw;
    private String pc;
    private Boolean isRecap;
    private String recapId;
}
