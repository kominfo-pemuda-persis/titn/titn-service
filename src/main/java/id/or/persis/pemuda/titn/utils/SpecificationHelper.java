package id.or.persis.pemuda.titn.utils;

import id.or.persis.pemuda.titn.entity.Event;
import id.or.persis.pemuda.titn.entity.Participant;
import id.or.persis.pemuda.titn.entity.ParticipantEvent;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.JoinType;
import jakarta.persistence.criteria.Path;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

public class SpecificationHelper<T> {

    public static Pageable sortByCreatedDateDesc(Pageable pageable) {
        Sort sort = Sort.by("createdDate").descending();
        Pageable pageableSort = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), sort);
        return pageableSort;
    }

    public static Pageable sortByCreatedDateAsc(Pageable pageable) {
        Sort sort = Sort.by("createdDate").ascending();
        Pageable pageableSort = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), sort);
        return pageableSort;
    }

    public static Pageable sortByTransactionDateDesc(Pageable pageable) {
        Sort sort = Sort.by("transactionDate").descending();
        Pageable pageableSort = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), sort);
        return pageableSort;
    }

    public static Specification searchAttributeContains(String attribute, String value) {
        final String finalText = value;
        return (root, query, cb) -> {
            if (StringUtils.isEmpty(finalText)) {
                return null;
            }
            return cb.like(cb.lower(root.get(attribute)), contains(finalText.toLowerCase()));
        };
    }

    private static String contains(String expression) {
        return MessageFormat.format("%{0}%", expression);
    }

    public static Specification searchAttributeEqual(String attribute, String value) {
        final String finalText = value;
        return (root, query, cb) -> {
            if (StringUtils.isEmpty(finalText)) {
                return null;
            }
            return cb.equal(root.get(attribute), finalText);
        };
    }

    public static Specification searchAttributeLocalDate(String attribute, LocalDate value) {
        return (root, query, cb) -> {
            if (Objects.isNull(value)) {
                return null;
            }
            return cb.equal(root.get(attribute), value);
        };
    }

    public static Specification searchBetweenAttributeLocalDate(String attribute, LocalDate from, LocalDate to) {
        return (root, query, cb) -> {
            if (Objects.isNull(from) || Objects.isNull(to)) {
                return null;
            }
            return cb.between(root.get(attribute), from, to);
        };
    }

    public static Specification searchBetweenAttributeSqlDate(String attribute, Date from, Date to) {
        return (root, query, cb) -> {
            if (Objects.isNull(from) || Objects.isNull(to)) {
                return null;
            }
            return cb.between(root.get(attribute), from, to);
        };
    }

    public static Specification searchBetweenAttributeLocalDateTime(
            String attribute, LocalDateTime from, LocalDateTime to) {
        return (root, query, cb) -> {
            if (Objects.isNull(from) || Objects.isNull(to)) {
                return null;
            }
            return cb.between(root.get(attribute), from, to);
        };
    }

    public static Specification searchBetweenAttributeTimestamp(String attribute, Timestamp from, Timestamp to) {
        return (root, query, cb) -> {
            if (Objects.isNull(from) || Objects.isNull(to)) {
                return null;
            }
            return cb.between(root.get(attribute), from, to);
        };
    }

    public static Specification searchAttributeInteger(String attribute, Integer value) {
        final Integer finalText = value;
        return (root, query, cb) -> {
            if (finalText == null) {
                return null;
            }
            return cb.equal(root.get(attribute), finalText);
        };
    }

    public static Specification searchAttributeBoolean(String attribute, Boolean value) {
        final Boolean finalText = value;
        return (root, query, cb) -> {
            if (finalText == null) {
                return null;
            }
            return cb.equal(root.get(attribute), finalText);
        };
    }

    public static Specification searchNested(String reference, String attribute, String value) {
        final String finalText = value;
        return (root, query, cb) -> {
            if (StringUtils.isEmpty(finalText)) {
                return null;
            }
            Path<?> u = root.get(reference);
            return cb.equal(u.get(attribute), value);
        };
    }

    public static Specification searchIsNotEmpty(String reference) {
        return (root, query, cb) -> {
            Path<Collection<?>> u = root.get(reference);
            return cb.isNotEmpty(u);
        };
    }

    public static Specification searchNotEqual(String attribute, String value) {
        final String finalText = value;
        return (root, query, cb) -> {
            if (Objects.isNull(finalText)) {
                return null;
            }
            return cb.notEqual(root.get(attribute), finalText);
        };
    }

    public static Specification searchNestedIn(String reference, String attribute, List<String> values) {
        final List<String> finalList = values;
        return (root, query, cb) -> {
            if (Objects.isNull(finalList) || finalList.isEmpty()) {
                return null;
            }
            Path<?> u = root.get(reference);
            return u.get(attribute).in(finalList);
        };
    }

    public static Specification searchNested2Level(
            String reference1, String reference2, String attribute, String value) {
        final String finalText = value;
        return (root, query, cb) -> {
            if (StringUtils.isEmpty(finalText)) {
                return null;
            }
            Path<?> u = root.get(reference1);
            return cb.equal(cb.lower(u.get(reference2).get(attribute)), value);
        };
    }

    public static Specification searchNested2LevelLike(
            String reference1, String reference2, String attribute, String value) {
        final String finalText = value;
        return (root, query, cb) -> {
            if (StringUtils.isEmpty(finalText)) {
                return null;
            }
            Path<?> u = root.get(reference1);
            return cb.like(
                    cb.lower(u.get(reference2).get(attribute)).as(String.class), cb.lower(cb.literal(contains(value))));
        };
    }

    public static Specification searchNestedLike(String reference, String attribute, String value) {
        final String finalText = value;
        return (root, query, cb) -> {
            if (StringUtils.isEmpty(finalText)) {
                return null;
            }
            Path<?> u = root.get(reference);
            return cb.like(cb.lower(u.get(attribute)), contains(value.toLowerCase()));
        };
    }

    public static Specification<Participant> eventLike(String attribute, String value) {
        return (root, query, cb) -> {
            if (StringUtils.isEmpty(value)) {
                return null;
            }
            // Join with the Event entity
            Join<Participant, ParticipantEvent> participantEventJoin = root.join("events", JoinType.INNER);
            Join<ParticipantEvent, Event> eventJoin = participantEventJoin.join("event", JoinType.INNER);

            // Add the where clause to filter by event ID
            return cb.like(cb.lower(eventJoin.get(attribute)), contains(value.toLowerCase()));
        };
    }

    public static Specification searchNestedBoolean(String reference, String attribute, Boolean value) {
        final Boolean finalText = value;
        return (root, query, cb) -> {
            if (finalText == null) {
                return null;
            }
            Path<?> u = root.get(reference);
            return cb.equal(u.get(attribute), finalText);
        };
    }

    public static Specification searchNestedAttributeLocalDateLessThanOrEqualTo(
            String reference, String attribute, LocalDate value) {
        return (root, query, cb) -> {
            if (Objects.isNull(value)) {
                return null;
            }
            Path<?> u = root.get(reference);
            return cb.lessThanOrEqualTo(u.get(attribute), value);
        };
    }

    public static Specification searchNestedEquals(String reference, String attribute, String value) {
        final String finalText = value;
        return (root, query, cb) -> {
            if (StringUtils.isEmpty(finalText)) {
                return null;
            }
            Path<?> u = root.get(reference);
            return cb.equal(u.get(attribute), value);
        };
    }

    public static Specification searchAttributeNotNull(String attribute) {
        return (root, query, cb) -> cb.isNotNull(root.get(attribute));
    }

    public static Specification searchAttributeIsNull(String attribute) {
        return (root, query, cb) -> cb.isNull(root.get(attribute));
    }

    public static Specification searchAttributeIsNull(String attribute, Boolean value) {
        if (Objects.isNull(value)) {
            return null;
        }
        if (value)
            return (root, query, criteriaBuilder) -> {
                root.join(attribute, JoinType.LEFT);
                return criteriaBuilder.isNotNull(root.get(attribute));
            };
        return (root, query, criteriaBuilder) -> {
            root.join(attribute, JoinType.LEFT);
            return criteriaBuilder.isNull(root.get(attribute));
        };
    }

    public static Specification searchNestedNotNull(String reference, String attribute) {
        return (root, query, cb) -> {
            Path<?> u = root.get(reference);
            return cb.isNotNull(u.get(attribute));
        };
    }

    public static Specification searchNested3LevelLike(
            String reference1, String reference2, String reference3, String attribute, String value) {
        final String finalText = value;
        return (root, query, cb) -> {
            if (StringUtils.isEmpty(finalText)) {
                return null;
            }
            Path<?> u = root.get(reference1);
            return cb.like(
                    cb.lower(u.get(reference2).get(reference3).get(attribute)).as(String.class),
                    cb.lower(cb.literal(contains(value))));
        };
    }

    public static Specification searchNested3LevelNotLike(
            String reference1, String reference2, String reference3, String attribute, String value) {
        final String finalText = value;
        return (root, query, cb) -> {
            if (StringUtils.isEmpty(finalText)) {
                return null;
            }
            Path<?> u = root.get(reference1);
            return cb.notLike(
                    cb.lower(u.get(reference2).get(reference3).get(attribute)).as(String.class),
                    cb.lower(cb.literal(contains(value))));
        };
    }

    public static Pageable sortByAnotherAscOrDescAndCreatedDateDesc(
            Pageable pageable, String anotherSortBy, boolean isAsc, boolean isFromNative) {

        Sort createdDate = Sort.by(Sort.Direction.DESC, "createdDate");
        if (isFromNative) {
            createdDate = Sort.by(Sort.Direction.DESC, "created_date");
        }

        Sort sort = createdDate.and(Sort.by(Sort.Direction.DESC, anotherSortBy));
        if (isAsc) {
            sort = createdDate.and(Sort.by(Sort.Direction.ASC, anotherSortBy));
        }
        return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), sort);
    }
}
