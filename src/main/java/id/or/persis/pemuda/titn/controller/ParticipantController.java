package id.or.persis.pemuda.titn.controller;

import static org.springframework.http.ResponseEntity.ok;

import id.or.persis.pemuda.titn.entity.Status;
import id.or.persis.pemuda.titn.payload.FilterParticipantDto;
import id.or.persis.pemuda.titn.payload.ParticipantRequestDto;
import id.or.persis.pemuda.titn.payload.ParticipantResponseDto;
import id.or.persis.pemuda.titn.service.ParticipantService;
import id.or.persis.pemuda.titn.utils.BaseResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@Tag(name = "Participant", description = "Endpoint for managing Participant")
@RequestMapping(value = Endpoint.PARTICIPANTS)
@Slf4j
@AllArgsConstructor
public class ParticipantController {
    private final ParticipantService participantService;
    private static final int DEFAULT_PAGE_SIZE = 10;

    @PostMapping
    @Operation(
            summary = "Add New Pemuda Persis Participant Data",
            description = "Add New Pemuda Persis Participant Data.",
            tags = {"Participant"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ParticipantResponseDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    public ResponseEntity<BaseResponse<ParticipantResponseDto>> create(
            @RequestBody @Valid ParticipantRequestDto participantRequestDto, HttpServletRequest request) {
        BaseResponse<ParticipantResponseDto> baseResponse =
                participantService.create(participantRequestDto, request, false);
        return ok(baseResponse);
    }

    @GetMapping
    @Operation(
            summary = "Get all Pemuda Persis Participant Data",
            description = "Get all Pemuda Persis Participant Data.",
            tags = {"Participant"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ParticipantResponseDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    public ResponseEntity<BaseResponse<Page<ParticipantResponseDto>>> getParticipants(
            @Parameter(description = "The size of the page to be returned")
                    @RequestParam(required = false, defaultValue = "10")
                    Integer pageSize,
            @Parameter(description = "Zero-based page index") @RequestParam(required = false, defaultValue = "0")
                    Integer pageNo,
            @Parameter(description = "Anggota or Participant") @RequestParam(required = false) Boolean isAnggota,
            @Parameter(description = "Spinner ") @RequestParam(required = false) Boolean isSpinner,
            @Parameter(description = "Sudah di recap atau belum") @RequestParam(required = false) Boolean isRecap,
            @Parameter(description = "keyword all") @RequestParam(required = false, defaultValue = "") String keyword,
            @Parameter(description = "Status") @RequestParam(required = false) Status status,
            @Parameter(description = "return summary pd") @RequestParam(required = false) String pd,
            @Parameter(description = "return summary pw") @RequestParam(required = false) String pw,
            @Parameter(description = "return summary pc") @RequestParam(required = false) String pc,
            @Parameter(description = "return summary by recapId") @RequestParam(required = false) String recapId,
            @Parameter(description = "Zero-based page sort") @RequestParam(required = false, defaultValue = "createdAt")
                    String sortBy,
            @Parameter(description = "Sort Order ASC or DESC ")
                    @RequestParam(value = "sortOrder", required = false, defaultValue = "DESC")
                    String sortOrder,
            HttpServletRequest request) {
        if (pageSize == null) {
            pageSize = DEFAULT_PAGE_SIZE;
        }
        if (pageNo == null) {
            pageNo = 0;
        }
        Pageable paging =
                PageRequest.of(pageNo, pageSize, Sort.by(Sort.Direction.valueOf(sortOrder.toUpperCase()), sortBy));
        FilterParticipantDto filterParticipantDto = FilterParticipantDto.builder()
                .isAnggota(isAnggota)
                .keyword(keyword)
                .isRecap(isRecap)
                .status(status)
                .paging(paging)
                .pc(pc)
                .pd(pd)
                .pw(pw)
                .request(request)
                .recapId(recapId)
                .isSpinner(isSpinner)
                .build();
        BaseResponse<Page<ParticipantResponseDto>> listBaseResponse =
                participantService.getAllParticipant(filterParticipantDto);
        return ok(listBaseResponse);
    }

    @PutMapping(value = "/{participantId}")
    @Operation(
            summary = "Update Pemuda Persis Participant Data",
            description = "Update Pemuda Persis Participant Data.",
            tags = {"Participant"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ParticipantResponseDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    public ResponseEntity<BaseResponse<ParticipantResponseDto>> update(
            @PathVariable String participantId,
            @RequestBody ParticipantRequestDto participantRequestDto,
            HttpServletRequest request) {
        BaseResponse<ParticipantResponseDto> baseResponse =
                participantService.update(participantRequestDto, participantId, request);
        return ok(baseResponse);
    }

    @DeleteMapping(value = "/{participantId}")
    @Operation(
            summary = "Delete Pemuda Persis Participant Data",
            description = "Delete Pemuda Persis Participant Data.",
            tags = {"Participant"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ParticipantResponseDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    public ResponseEntity<BaseResponse<ParticipantResponseDto>> delete(
            @PathVariable String participantId, HttpServletRequest request) {
        BaseResponse<ParticipantResponseDto> baseResponse = participantService.delete(participantId, request);
        return ok(baseResponse);
    }

    @GetMapping(value = "/{participantId}")
    @Operation(
            summary = "Get Pemuda Persis Participant Data By ID",
            description = "Get Pemuda Persis Participant Data By ID.",
            tags = {"Participant"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ParticipantResponseDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    public ResponseEntity<BaseResponse<ParticipantResponseDto>> detailByParticipantId(
            @PathVariable String participantId, HttpServletRequest request) {
        BaseResponse<ParticipantResponseDto> baseResponse =
                participantService.getDetailByParticipantId(participantId, request);
        return ok(baseResponse);
    }

    @GetMapping(value = "/npa/{npa}")
    @Operation(
            summary = "Get Pemuda Persis Participant Data By NPA",
            description = "Get Pemuda Persis Participant Data By NPA.",
            tags = {"Participant"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ParticipantResponseDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    public ResponseEntity<BaseResponse<ParticipantResponseDto>> participantByNpa(@PathVariable String npa) {
        return ok(participantService.participantByNpa(npa));
    }

    @PutMapping(value = "/notification/{participantId}")
    @Operation(
            summary = "Send Notification Pemuda Persis Participant Data By participantId",
            description = "Send Notification Persis Participant Data By participantId.",
            tags = {"Participant"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ParticipantResponseDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    public ResponseEntity<BaseResponse<ParticipantResponseDto>> sendNotification(
            @PathVariable String participantId,
            @Parameter(description = "notification type EMAIL,WA,ALL") @RequestParam String type,
            @Parameter(description = "force send notif") @RequestParam(defaultValue = "false") boolean isForce) {
        return ok(participantService.sendNotificationByParticipantId(participantId, type, isForce));
    }
}
