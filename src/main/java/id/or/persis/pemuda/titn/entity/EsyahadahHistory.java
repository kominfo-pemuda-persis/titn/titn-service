package id.or.persis.pemuda.titn.entity;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.SuperBuilder;

/**
 * Project: titn-service
 * <p>
 * User: hasan.sanusi
 * Email: hasan.sanusi@dansmultipro.com
 * Telegram: @hasansanusi
 * Date: 05/10/24
 * Time: 20.10
 * <p>
 * Created with IntelliJ IDEA
 */
@Entity
@Table(name = "t_syahadah_history")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class EsyahadahHistory extends Updatable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String historyId;
    private String participantId;
    private String npa;

    @Column(name = "juara")
    private String rank;

    private String contest;
    private String competition;

    @Column(name = "event")
    private String event;

    @Column(name = "send_email_status")
    private String emailNotificationStatus;

    @Column(name = "send_email_status_desc")
    private String emailNotificationStatusDesc;

    @Transient
    private String nik;

    @Transient
    private String pcName;

    @Transient
    private String fullName;

    @Transient
    private String email;

    @Transient
    private String phone;
}
