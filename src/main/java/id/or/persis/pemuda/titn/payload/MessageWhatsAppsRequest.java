package id.or.persis.pemuda.titn.payload;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Project: titn-service
 * <p>
 * User: hasan.sanusi
 * Email: hasan.sanusi@dansmultipro.com
 * Telegram: @hasansanusi
 * Date: 14/07/24
 * Time: 07.08
 * <p>
 * Created with IntelliJ IDEA
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MessageWhatsAppsRequest {
    private String token;
    private String target;
    private String type;
    private String participantId;
    private String delay;
    private String message;
    private String url;
}
