package id.or.persis.pemuda.titn.service;

import static id.or.persis.pemuda.titn.service.ParticipantService.generateParticipantId;

import id.or.persis.pemuda.titn.configuration.AppConfig;
import id.or.persis.pemuda.titn.entity.*;
import id.or.persis.pemuda.titn.payload.*;
import id.or.persis.pemuda.titn.repository.ParticipantRecapRepository;
import id.or.persis.pemuda.titn.utils.*;
import jakarta.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Project: titn-service
 * <p>
 * User: hasan.sanusi Email: hasan.sanusi@dansmultipro.com
 * Telegram: @hasansanusi Date: 11/06/24 Time: 20.02
 * <p>
 * Created with IntelliJ IDEA
 */
@Service
@Slf4j
@AllArgsConstructor
public class ParticipantRecapService {
    private final ModelMapper modelMapper;
    private final ParticipantRecapRepository participantRecapRepository;
    private final ParticipantService participantService;
    private final MailService mailService;
    private final AppConfig appConfig;

    @Transactional
    public BaseResponse<ParticipantRecapResponseDto> create(
            ParticipantRecapRequestDto participantRequestDto, HttpServletRequest request) {
        final long start = System.nanoTime();
        AnggotaUser angotaUser = (AnggotaUser) request.getAttribute("user");
        validationAndGetDataParticipant(participantRequestDto);
        ParticipantRecap participantRecap = convertToEntity(participantRequestDto);
        participantRecap.setCreatedAt(LocalDateTime.now());
        participantRecap.setCreatedBy(angotaUser.getNama());
        participantRecap.setRecapId(generateParticipantId("RCP"));
        participantRecap.setStatus(Status.SUBMITTED);
        participantRecap.setNums(new HashSet<>(participantRequestDto.getParticipants()).size());
        ParticipantRecap recap = participantRecapRepository.save(participantRecap);
        participantService.updateRecapByParticipantIds(recap.getRecapId(), participantRequestDto.getParticipants());
        Map<String, Object> content = new HashMap<>();
        String emailFrom = appConfig
                .getEmailFromName()
                .concat("<")
                .concat(appConfig.getEmailFromAddress())
                .concat(">");
        try {
            mailService.sendMail(
                    content, "Recap Peserta TITN", participantRecap.getEmail(), "recap-template.html", emailFrom);
            log.info("[SUCCESS_SEND_EMAIL_RECAP] with recap_id:{}", recap.getRecapId());
        } catch (Exception e) {
            log.error("[ERROR_SEND_EMAIL_RECAP] {1} ", e.getCause());
        }
        if (Objects.nonNull(ContactNumberUtil.getValidContactNumber(participantRecap.getPhone())))
            participantService.sendNotificationRecap(participantRecap);
        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(
                HttpStatus.OK,
                ((end - start) / 1000000),
                GlobalMessage.SUCCESS.message,
                convertCreateDto(recap, participantRequestDto));
    }

    private ParticipantRecapResponseDto convertCreateDto(
            ParticipantRecap recap, ParticipantRecapRequestDto participantRequestDto) {
        ParticipantRecapResponseDto responseDto = modelMapper.map(recap, ParticipantRecapResponseDto.class);
        responseDto.setParticipants(participantRequestDto.getParticipants());
        return responseDto;
    }

    private void validationAndGetDataParticipant(ParticipantRecapRequestDto participantRequestDto) {
        List<Participant> participantList = participantService.findAllByIdList(participantRequestDto.getParticipants());
        Set<String> participantIdList =
                participantList.stream().map(Participant::getParticipantId).collect(Collectors.toSet());
        Set<String> participantNotRegister = new HashSet<>();
        participantRequestDto.getParticipants().forEach(participant -> {
            if (!participantIdList.contains(participant)) {
                participantNotRegister.add(participant);
            }
        });
        if (!participantNotRegister.isEmpty()) {
            throw new BusinessException(
                    HttpStatus.BAD_REQUEST,
                    "PATICIPANT_ID_CONTAINS_NOT_REGISTER",
                    "Terdapat list PARTICIPANT ID belum terdaftar di TITN : "
                            + String.join("|", participantNotRegister));
        }
        List<String> participantIdRecap = new ArrayList<>();
        participantList.forEach(participant -> {
            if (Objects.nonNull(participant.getRecap())) {
                participantIdRecap.add(participant.getParticipantId());
            }
        });
        if (!participantIdRecap.isEmpty()) {
            throw new BusinessException(
                    HttpStatus.BAD_REQUEST,
                    "PATICIPANT_ID_CONTAINS_IS_ALREADY_RECAP",
                    "Terdapat list PATICIPANT_ID sudah di recap : " + String.join("|", participantIdRecap));
        }
    }

    private ParticipantRecap convertToEntity(ParticipantRecapRequestDto participantRequestDto) {
        return modelMapper.map(participantRequestDto, ParticipantRecap.class);
    }

    public BaseResponse<Page<ParticipantRecapResponseListDto>> getAllParticipantRecap(
            String keyword,
            Status status,
            Pageable paging,
            HttpServletRequest request,
            String pc,
            String pd,
            String pw) {
        final long start = System.nanoTime();
        AnggotaUser angotaUser = prepareDataUser(request);
        if (Objects.nonNull(angotaUser.getPw())) {
            pw = angotaUser.getPw();
        }
        if (Objects.nonNull(angotaUser.getPc())) {
            pc = angotaUser.getPc();
        }
        if (Objects.nonNull(angotaUser.getPd())) {
            pd = angotaUser.getPd();
        }
        Page<ParticipantRecap> participants = participantRecapRepository.findAll(
                bySearch(
                        keyword,
                        Optional.ofNullable(status).map(status1 -> status1.code).orElse(null),
                        pd,
                        pw,
                        pc),
                paging);
        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(
                HttpStatus.OK,
                ((end - start) / 1000000),
                GlobalMessage.SUCCESS.message,
                participants.map(this::convertToDto));
    }

    private AnggotaUser prepareDataUser(HttpServletRequest request) {
        AnggotaUser anggotaUser = (AnggotaUser) request.getAttribute("user");
        if (anggotaUser == null) {
            throw new BusinessException(HttpStatus.UNAUTHORIZED, "UNAUTHORIZED");
        }
        switch (anggotaUser.getRole().toLowerCase()) {
            case "anggota":
                throw new BusinessException(HttpStatus.UNAUTHORIZED, "UNAUTHORIZED");
            case "admin_pw": {
                anggotaUser.setPc(null);
                anggotaUser.setPd(null);
                break;
            }
            case "admin_pd": {
                anggotaUser.setPc(null);
                break;
            }
            case "admin_pc": {
                break;
            }
            default: {
                anggotaUser.setPw(null);
                anggotaUser.setPc(null);
                anggotaUser.setPd(null);
                break;
            }
        }
        anggotaUser.setNama(Optional.ofNullable(request.getParameter("nama")).orElse(anggotaUser.getNpa()));
        return anggotaUser;
    }

    private ParticipantRecapResponseListDto convertToDto(ParticipantRecap participantRecap) {
        return modelMapper.map(participantRecap, ParticipantRecapResponseListDto.class);
    }

    public Specification<ParticipantRecap> bySearch(String keyword, String status, String pd, String pw, String pc) {
        return SpecificationHelper.searchAttributeEqual("status", status)
                .and(SpecificationHelper.searchAttributeEqual("pwCode", pw))
                .and(SpecificationHelper.searchAttributeEqual("pdCode", pd))
                .and(SpecificationHelper.searchAttributeEqual("pcCode", pc))
                .and((SpecificationHelper.searchAttributeContains("email", keyword))
                        .or(SpecificationHelper.searchAttributeContains("pcName", keyword))
                        .or(SpecificationHelper.searchAttributeContains("pdName", keyword))
                        .or(SpecificationHelper.searchAttributeContains("pwName", keyword))
                        .or(SpecificationHelper.searchAttributeContains("npa", keyword)));
    }

    @Transactional
    public BaseResponse<ParticipantRecapResponseDto> update(
            StatusRecapRequestDto statusRecapRequestDto, String recapId, HttpServletRequest request) {
        final long start = System.nanoTime();
        AnggotaUser angotaUser = prepareDataUser(request);
        if (!(Status.APPROVED.equals(statusRecapRequestDto.getStatus())
                || Status.REJECTED.equals(statusRecapRequestDto.getStatus()))) {
            throw new BusinessException(
                    HttpStatus.BAD_REQUEST, "ONLY_APPROVED_OR_REJECTED_STATUS", "ONLY_APPROVED_OR_REJECTED_STATUS");
        }
        ParticipantRecap participantRecap = participantRecapRepository
                .findByRecapIdAndStatus(recapId, Status.SUBMITTED)
                .orElseThrow(() -> new BusinessException(HttpStatus.BAD_REQUEST, "DATA_NOT_FOUND", "DATA_NOT_FOUND"));

        if (Status.REJECTED.equals(statusRecapRequestDto.getStatus())) {
            if (StringUtils.isEmpty(statusRecapRequestDto.getRejectReason())) {
                throw new BusinessException(
                        HttpStatus.BAD_REQUEST, "REJECT_REASON_NOT_EMPTY", "REJECT_REASON_NOT_EMPTY");
            }
            participantRecap.setRejectReason(statusRecapRequestDto.getRejectReason());
            participantService.updateParticipantRecapId(participantRecap.getRecapId());
            participantRecap.setUpdatedAt(LocalDateTime.now());
            participantRecap.setStatus(statusRecapRequestDto.getStatus());
            participantRecap.setUpdatedBy(angotaUser.getNama());
            participantRecapRepository.save(participantRecap);
        }
        if (Status.APPROVED.equals(statusRecapRequestDto.getStatus())) {
            participantRecap.setUpdatedAt(LocalDateTime.now());
            participantRecap.setStatus(statusRecapRequestDto.getStatus());
            participantRecap.setApprovedAt(LocalDateTime.now());
            participantRecap.setApprovedBy(angotaUser.getNama());
            participantRecap.setUpdatedBy(angotaUser.getNama());
            participantRecapRepository.save(participantRecap);
            participantService.updateStatusParticipant(participantRecap.getRecapId(), Status.APPROVED);
        }

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(
                HttpStatus.OK, ((end - start) / 1000000), GlobalMessage.SUCCESS.message, null);
    }

    public BaseResponse<ParticipantRecapResponseDto> delete(String recapId) {
        final long start = System.nanoTime();
        ParticipantRecap participantRecap = participantRecapRepository
                .findByRecapId(recapId)
                .orElseThrow(() -> new BusinessException(
                        HttpStatus.BAD_REQUEST,
                        "ONLY_SUBMITTED_OR_DELETED_STATUS",
                        "ONLY_SUBMITTED_OR_DELETED_STATUS"));
        if (!(Status.APPROVED.equals(participantRecap.getStatus())
                || Status.REJECTED.equals(participantRecap.getStatus()))) {
            throw new BusinessException(
                    HttpStatus.BAD_REQUEST,
                    "ONLY_SUBMITTED_OR_REJECTED_STATUS_CAN_BE_DELETED",
                    "ONLY_SUBMITTED_OR_REJECTED_STATUS_CAN_BE_DELETED");
        }
        participantService.updateParticipantRecapId(participantRecap.getRecapId());
        participantRecapRepository.delete(participantRecap);
        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(
                HttpStatus.OK, ((end - start) / 1000000), GlobalMessage.SUCCESS.message, null);
    }

    public BaseResponse<ParticipantRecapResponseDto> sendNotification(String recapId) {
        final long start = System.nanoTime();
        ParticipantRecap participantRecap = participantRecapRepository
                .findByRecapIdAndStatus(recapId, Status.APPROVED)
                .orElseThrow(() -> new BusinessException(
                        HttpStatus.BAD_REQUEST,
                        "SEND_NOTIFICATION_FOR_STATUS_APPROVED_ONLY",
                        "Kirim notifikasi hanya untuk status approved"));
        participantService.senEmailAsync(participantRecap.getRecapId(), true);
        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(
                HttpStatus.OK, ((end - start) / 1000000), GlobalMessage.SUCCESS.message, null);
    }
}
