package id.or.persis.pemuda.titn.service;

import java.io.InputStream;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Service;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;

@Service
@Slf4j
public class StorageService {
    @Value("${aws.bucket-name.qr}")
    private String bucketName;

    private final S3Client s3Client;

    public StorageService(S3Client s3Client) {
        this.s3Client = s3Client;
    }

    public String uploadFile(ByteArrayResource resource, String fileName) {
        PutObjectRequest putObjectRequest =
                PutObjectRequest.builder().bucket(bucketName).key(fileName).build();
        try (InputStream inputStream = resource.getInputStream()) {
            s3Client.putObject(
                    putObjectRequest,
                    software.amazon.awssdk.core.sync.RequestBody.fromInputStream(
                            inputStream, resource.contentLength()));
            log.info("Upload File success: {}", fileName);
        } catch (Exception e) {
            log.error("Error while uploading to s3 file {} with {}", e, fileName);
        }
        return fileName;
    }
}
