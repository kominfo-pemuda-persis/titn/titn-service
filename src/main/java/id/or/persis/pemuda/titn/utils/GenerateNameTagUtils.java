package id.or.persis.pemuda.titn.utils;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import javax.imageio.ImageIO;
import net.sf.jasperreports.engine.*;
import org.springframework.core.io.ClassPathResource;

/**
 * Project: titn-service
 * <p>
 * User: hasan.sanusi
 * Email: hasan.sanusi@dansmultipro.com
 * Telegram: @hasansanusi
 * Date: 31/08/24
 * Time: 21.01
 * <p>
 * Created with IntelliJ IDEA
 */
public class GenerateNameTagUtils {

    public static byte[] executeNameTag(Map<String, Object> parameter, String templateName)
            throws JRException, IOException {
        JasperReport jasperMaster = JasperCompileManager.compileReport(getReportTemplate(templateName));
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperMaster, parameter, new JREmptyDataSource());
        BufferedImage bufferedImage = (BufferedImage) JasperPrintManager.printPageToImage(jasperPrint, 0, 2.0f);
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            ImageIO.write(bufferedImage, "png", baos);
            return baos.toByteArray();
        }
    }

    public static InputStream getReportTemplate(String templateName) throws IOException {
        return new ClassPathResource("/templates/".concat(templateName)).getInputStream();
    }

    public static JasperPrint executeJasperPrint(Map<String, Object> parameter, String templateName)
            throws JRException, IOException {
        JasperReport jasperMaster = JasperCompileManager.compileReport(getReportTemplate(templateName));
        return JasperFillManager.fillReport(jasperMaster, parameter, new JREmptyDataSource());
    }
}
