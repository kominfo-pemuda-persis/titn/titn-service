package id.or.persis.pemuda.titn.configuration;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import io.swagger.v3.oas.models.servers.Server;
import java.util.List;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenAPIConfig {

    @Bean
    public OpenAPI titnOpenAPI(
            @Value("${application-description}") String appDescription,
            @Value("${application" + "-version}") String appVersion) {
        OpenAPI openAPI = new OpenAPI();
        openAPI.info(new io.swagger.v3.oas.models.info.Info()
                .title("TITN Service API")
                .description(appDescription)
                .version(appVersion)
                .contact(new io.swagger.v3.oas.models.info.Contact()
                        .name("Hendi Santika")
                        .url("https://s.id/hendisantika")
                        .email("hendisantika@yahoo.co.id"))
                .termsOfService("TOC")
                .license(new io.swagger.v3.oas.models.info.License()
                        .name("License")
                        .url("https://s.id/hendisantika")));

        Server serverLocal = new Server();
        serverLocal.setUrl("http://localhost:8080");
        serverLocal.setDescription("Local server");

        Server dev = new Server();
        dev.setUrl("https://titn-dev.persis.or.id");
        dev.setDescription("development server");

        Server production = new Server();
        production.setUrl("https://titn.persis.or.id");
        production.setDescription("production server");

        Server currentServer = new Server();
        currentServer.setUrl("/");
        currentServer.setDescription("current server");

        openAPI.servers(List.of(currentServer, dev, serverLocal, production));
        openAPI.addSecurityItem(new SecurityRequirement().addList("Bearer Authentication"))
                .components(new Components().addSecuritySchemes("Bearer Authentication", createAPIKeyScheme()));
        return openAPI;
    }

    private SecurityScheme createAPIKeyScheme() {
        return new SecurityScheme()
                .type(SecurityScheme.Type.HTTP)
                .bearerFormat("JWT")
                .scheme("bearer");
    }

    @Bean
    GroupedOpenApi publicGroupOpenApi() {
        return GroupedOpenApi.builder()
                .group("Public API")
                .pathsToMatch("/api/public/**", "/api/events")
                .build();
    }

    @Bean
    GroupedOpenApi privateGroupOpenApi() {
        return GroupedOpenApi.builder()
                .group("Private API")
                .pathsToMatch(
                        "/api/participants", "/api/recaps", "/api/dashboard", "/api/recaps/**", "/api/participants/**")
                .build();
    }

    @Bean
    GroupedOpenApi defaultGroupOpenApi() {
        return GroupedOpenApi.builder()
                .group("Default")
                .packagesToScan("id.or.persis.pemuda.titn.controller")
                .build();
    }
}
