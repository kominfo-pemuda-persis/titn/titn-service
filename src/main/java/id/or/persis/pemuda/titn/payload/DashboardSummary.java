package id.or.persis.pemuda.titn.payload;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Project: titn-service
 * <p>
 * User: hasan.sanusi
 * Email: hasan.sanusi@dansmultipro.com
 * Telegram: @hasansanusi
 * Date: 17/07/24
 * Time: 21.08
 * <p>
 * Created with IntelliJ IDEA
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DashboardSummary {
    private String type;
    private String name;
    private Long value;
}
