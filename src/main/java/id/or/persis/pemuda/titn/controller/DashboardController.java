package id.or.persis.pemuda.titn.controller;

import static org.springframework.http.ResponseEntity.ok;

import id.or.persis.pemuda.titn.payload.DashboardSummaryResponse;
import id.or.persis.pemuda.titn.service.DashboardService;
import id.or.persis.pemuda.titn.utils.BaseResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletRequest;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Project: titn-service
 * <p>
 * User: hasan.sanusi
 * Email: hasan.sanusi@dansmultipro.com
 * Telegram: @hasansanusi
 * Date: 17/07/24
 * Time: 20.01
 * <p>
 * Created with IntelliJ IDEA
 */
@RestController
@Tag(name = "Dashboard", description = "Endpoint for Dashboard ")
@RequestMapping(value = Endpoint.DASHBOARD)
@Slf4j
@AllArgsConstructor
public class DashboardController {
    private final DashboardService dashboardService;

    @GetMapping
    @Operation(
            summary = "Get Summary Pemuda Persis Participant Data",
            description = "Get Summary Pemuda Persis Participant Data.",
            tags = {"Dashboard"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = DashboardSummaryResponse.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    public ResponseEntity<BaseResponse<DashboardSummaryResponse>> getParticipants(
            @Parameter(description = "return summary pd") @RequestParam(required = false) String pd,
            @Parameter(description = "return summary pw") @RequestParam(required = false) String pw,
            @Parameter(description = "return summary pc") @RequestParam(required = false) String pc,
            HttpServletRequest request) {

        BaseResponse<DashboardSummaryResponse> response = dashboardService.getSummary(pd, pc, pw, request);
        return ok(response);
    }

    @GetMapping(value = "/panitia")
    @Operation(
            summary = "Get Summary Pemuda Persis Participant Data",
            description = "Get Summary Pemuda Persis Participant Data.",
            tags = {"Dashboard"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = DashboardSummaryResponse.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    public ResponseEntity<BaseResponse<DashboardSummaryResponse>> getParticipantsPanitia(
            @Parameter(description = "return summary pd") @RequestParam(required = false) String pd,
            @Parameter(description = "return summary pw") @RequestParam(required = false) String pw,
            @Parameter(description = "return summary pc") @RequestParam(required = false) String pc,
            HttpServletRequest request) {

        BaseResponse<DashboardSummaryResponse> response = dashboardService.getSummaryPanitia(pd, pc, pw, request);
        return ok(response);
    }
}
