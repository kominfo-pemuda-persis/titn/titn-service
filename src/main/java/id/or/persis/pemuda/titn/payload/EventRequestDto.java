package id.or.persis.pemuda.titn.payload;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import jakarta.validation.constraints.NotEmpty;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class EventRequestDto {
    private Long id;

    @NotEmpty
    private String name;

    private String description;

    @NotEmpty
    private String type;

    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
}
