package id.or.persis.pemuda.titn.payload;

import id.or.persis.pemuda.titn.utils.validation.ValidateString;
import jakarta.validation.constraints.NotEmpty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * Project: titn-service
 * <p>
 * User: hasan.sanusi
 * Email: hasan.sanusi@dansmultipro.com
 * Telegram: @hasansanusi
 * Date: 14/07/24
 * Time: 07.08
 * <p>
 * Created with IntelliJ IDEA
 */
@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode
public class BaseMessageWaRequest {
    @NotEmpty(message = "MESSAGE_NOT_EMPTY")
    private String message;

    @ValidateString(acceptedValues = {"text", "image", "file"})
    @NotEmpty(message = "MESSAGE_TYPE_NOT_EMPTY")
    private String messageType;

    private Integer delay;

    private String imageUrl;
}
