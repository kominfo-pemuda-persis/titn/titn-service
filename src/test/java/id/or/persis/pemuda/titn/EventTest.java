package id.or.persis.pemuda.titn;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.notNullValue;

import id.or.persis.pemuda.titn.controller.Endpoint;
import id.or.persis.pemuda.titn.entity.Event;
import id.or.persis.pemuda.titn.payload.EventRequestDto;
import id.or.persis.pemuda.titn.repository.EventRepository;
import id.or.persis.pemuda.titn.utils.EventType;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import java.time.LocalDateTime;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.EnabledIfEnvironmentVariable;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.MySQLContainer;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@EnabledIfEnvironmentVariable(named = "APP_ENV", matches = "local")
public class EventTest {

    @Autowired
    EventRepository eventRepository;

    @Autowired
    ModelMapper modelMapper;

    @LocalServerPort
    private Integer port;

    static MySQLContainer<?> container = new MySQLContainer<>("mysql:8.4.0");

    @BeforeAll
    static void beforeAll() {
        container.withReuse(true);
        container.start();
    }

    @AfterAll
    static void afterAll() {
        container.stop();
    }

    @DynamicPropertySource
    static void configureProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", container::getJdbcUrl);
        registry.add("spring.datasource.username", container::getUsername);
        registry.add("spring.datasource.password", container::getPassword);
    }

    @BeforeEach
    void setUp() {
        RestAssured.baseURI = "http://localhost:" + port;
        eventRepository.deleteAll();
    }

    @Test
    void shouldGetAllEvent() {
        String expectedName = "BASKET";
        String expectedDesc = "Basket national cup";
        eventRepository.save(Event.builder()
                .type(EventType.OLAHRAGA)
                .createdAt(LocalDateTime.now())
                .name(expectedName)
                .description(expectedDesc)
                .build());

        given().contentType(ContentType.JSON)
                .when()
                .get(Endpoint.EVENTS)
                .prettyPeek()
                .then()
                .statusCode(200)
                .body("data.content", hasSize(1));
    }

    @Test
    void shouldSaveEvent() {
        String expectedName = "BASKET";
        String expectedDesc = "Basket national cup";
        EventRequestDto payload = EventRequestDto.builder()
                .type(EventType.ILMIAH.name())
                .name(expectedName)
                .description(expectedDesc)
                .build();

        given().contentType(ContentType.JSON)
                .body(payload)
                .when()
                .post(Endpoint.EVENTS)
                .prettyPeek()
                .then()
                .statusCode(200)
                .body("data.id", notNullValue())
                .body("data.name", equalTo(expectedName))
                .body("data.description", equalTo(expectedDesc));
    }

    @Test
    void shouldUpdateEvent() {
        String expectedName = "SEPAK BOLA";
        String expectedDesc = "Sepak bola tournament";
        Event existing = eventRepository.save(
                Event.builder().type(EventType.OLAHRAGA).name("PANJAT PINANG").build());

        EventRequestDto payload = modelMapper.map(existing, EventRequestDto.class);
        payload.setName(expectedName);
        payload.setDescription(expectedDesc);

        given().contentType(ContentType.JSON)
                .body(payload)
                .when()
                .post(Endpoint.EVENTS)
                .prettyPeek()
                .then()
                .statusCode(200)
                .body("data.id", notNullValue())
                .body("data.name", equalTo(expectedName))
                .body("data.description", equalTo(expectedDesc));
    }
}
