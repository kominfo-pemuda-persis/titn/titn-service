package id.or.persis.pemuda.titn.repository;

import id.or.persis.pemuda.titn.entity.ParticipantRecap;
import id.or.persis.pemuda.titn.entity.Status;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Project: titn-service
 * <p>
 * User: hasan.sanusi Email: hasan.sanusi@dansmultipro.com
 * Telegram: @hasansanusi Date: 13/06/24 Time: 19.38
 * <p>
 * Created with IntelliJ IDEA
 */
@Repository
public interface ParticipantRecapRepository extends JpaRepository<ParticipantRecap, String> {
    Page<ParticipantRecap> findAll(Specification<ParticipantRecap> specification, Pageable pageable);

    @Query(value = "select t from ParticipantRecap t where t.recapId=:id and t.status=:status and t.isDeleted=false")
    Optional<ParticipantRecap> findByRecapIdAndStatus(String id, Status status);

    @Query(value = "select t from ParticipantRecap t where t.recapId=:id and t.isDeleted=false")
    Optional<ParticipantRecap> findByRecapId(String id);
}
