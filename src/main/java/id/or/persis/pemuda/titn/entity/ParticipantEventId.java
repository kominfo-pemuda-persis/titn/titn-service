package id.or.persis.pemuda.titn.entity;

import jakarta.persistence.*;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * Project: titn-service
 * <p>
 * User: hasan.sanusi
 * Email: hasan.sanusi@dansmultipro.com
 * Telegram: @hasansanusi
 * Date: 27/08/24
 * Time: 07.01
 * <p>
 * Created with IntelliJ IDEA
 */
@Data
@Embeddable
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class ParticipantEventId implements Serializable {
    @Column(name = "event_id")
    private Long eventId;

    @Column(name = "participant_id", length = 100)
    private String participantId;
}
