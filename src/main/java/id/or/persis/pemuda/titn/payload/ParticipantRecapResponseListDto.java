package id.or.persis.pemuda.titn.payload;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import id.or.persis.pemuda.titn.utils.TimeGmtUtils;
import java.time.LocalDateTime;
import java.util.Optional;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * Project: titn-service
 * <p>
 * User: hasan.sanusi Email: hasan.sanusi@dansmultipro.com
 * Telegram: @hasansanusi Date: 11/06/24 Time: 19.58
 * <p>
 * Created with IntelliJ IDEA
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ParticipantRecapResponseListDto {
    private String recapId;
    private String npa;
    private String fullName;
    private String email;
    private String phone;
    private String pcCode;
    private String pcName;
    private String pdCode;
    private String pdName;
    private String pwCode;
    private String pwName;
    private String buktiTransfer;
    private Integer nums;
    private String status;
    protected String approvedBy;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    protected LocalDateTime approvedAt;

    protected String updatedBy;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    protected LocalDateTime updatedAt;

    protected String createdBy;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    protected LocalDateTime createdAt;

    public LocalDateTime getCreatedAt() {
        return Optional.ofNullable(createdAt)
                .map(TimeGmtUtils::convertToGmtPlus7)
                .orElse(createdAt);
    }

    public LocalDateTime getUpdatedAt() {
        return Optional.ofNullable(updatedAt)
                .map(TimeGmtUtils::convertToGmtPlus7)
                .orElse(updatedAt);
    }

    public LocalDateTime getApprovedAt() {
        return Optional.ofNullable(approvedAt)
                .map(TimeGmtUtils::convertToGmtPlus7)
                .orElse(approvedAt);
    }
}
