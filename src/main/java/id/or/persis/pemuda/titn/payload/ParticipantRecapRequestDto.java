package id.or.persis.pemuda.titn.payload;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import id.or.persis.pemuda.titn.utils.validation.ValidateString;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Project: titn-service
 * <p>
 * User: hasan.sanusi Email: hasan.sanusi@dansmultipro.com
 * Telegram: @hasansanusi Date: 11/06/24 Time: 19.58
 * <p>
 * Created with IntelliJ IDEA
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ParticipantRecapRequestDto {
    private String npa;
    private String fullName;
    private String email;
    private String phone;
    private String pcCode;
    private String pcName;
    private String pdCode;
    private String pdName;
    private String pwCode;
    private String pwName;
    private String buktiTransfer;
    private List<String> participants;
    private String dataParticipants;

    @ValidateString(acceptedValues = {"UPLOAD", "MANUAL"})
    private String mode;

    private String nums;
}
