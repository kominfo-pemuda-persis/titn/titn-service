package id.or.persis.pemuda.titn.controller;

public class Endpoint {
    public static final String PARTICIPANTS = "/api/participants";
    public static final String PARTICIPANTS_PUB = "/api/public/participants";
    public static final String EVENTS = "/api/events";
    public static final String RECAPS = "/api/recaps";
    public static final String DASHBOARD = "/api/dashboard";
    public static final String CONFIGURATION = "/api/configuration";
    public static final String E_SYAHADAH = "/api/e-syahadah";
}
