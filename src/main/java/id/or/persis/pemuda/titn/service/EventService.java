package id.or.persis.pemuda.titn.service;

import id.or.persis.pemuda.titn.entity.Event;
import id.or.persis.pemuda.titn.payload.EventRequestDto;
import id.or.persis.pemuda.titn.payload.EventResponseDto;
import id.or.persis.pemuda.titn.repository.EventRepository;
import id.or.persis.pemuda.titn.utils.BaseResponse;
import id.or.persis.pemuda.titn.utils.BusinessException;
import id.or.persis.pemuda.titn.utils.EventType;
import id.or.persis.pemuda.titn.utils.GlobalMessage;
import id.or.persis.pemuda.titn.utils.ResponseBuilder;
import jakarta.validation.ValidationException;
import java.util.EnumSet;
import java.util.List;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.EnumUtils;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@AllArgsConstructor
public class EventService {
    private final EventRepository eventRepository;
    private final ModelMapper modelMapper;

    public BaseResponse<EventResponseDto> create(EventRequestDto request) {
        final long start = System.nanoTime();
        request.setId(null);
        validate(request);
        Event event = convertToEntity(request);
        event = eventRepository.save(event);
        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(
                HttpStatus.OK, ((end - start) / 1000000), GlobalMessage.SUCCESS.message, convertToDto(event));
    }

    public BaseResponse<Page<EventResponseDto>> getAllEvent(Pageable paging) {
        final long start = System.nanoTime();
        Page<EventResponseDto> events = eventRepository.findAll(paging).map(this::convertToDto);
        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(
                HttpStatus.OK, ((end - start) / 1000000), GlobalMessage.SUCCESS.message, events);
    }

    public List<EventResponseDto> getAllEvent() {
        return eventRepository.findAll().stream().map(this::convertToDto).toList();
    }

    public BaseResponse<EventResponseDto> update(EventRequestDto request) {
        final long start = System.nanoTime();
        validate(request);
        Event existingEvent = eventRepository
                .findById(request.getId())
                .orElseThrow(() -> new BusinessException(HttpStatus.BAD_REQUEST, "DATA_NOT_FOUND", "DATA_NOT_FOUND"));
        updateEntity(request, existingEvent);
        eventRepository.save(existingEvent);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(
                HttpStatus.OK, ((end - start) / 1000000), GlobalMessage.SUCCESS.message, convertToDto(existingEvent));
    }

    private EventResponseDto convertToDto(Event result) {
        return modelMapper.map(result, EventResponseDto.class);
    }

    private Event convertToEntity(EventRequestDto eventRequestDto) {
        return modelMapper.map(eventRequestDto, Event.class);
    }

    private Event updateEntity(EventRequestDto eventRequestDto, Event event) {
        modelMapper.map(eventRequestDto, event);
        return event;
    }

    private void validate(EventRequestDto requestDto) {
        String type = requestDto.getType();
        if (!EnumUtils.isValidEnumIgnoreCase(EventType.class, type)) {
            String list =
                    EnumSet.allOf(EventType.class).stream().map(Enum::toString).collect(Collectors.joining(","));
            throw new ValidationException("Type not valid. valid type are ".concat(list));
        }
    }

    public Event findByEventId(Long eventId) {
        return eventRepository
                .findById(eventId)
                .orElseThrow(() -> new BusinessException(HttpStatus.BAD_REQUEST, "EVENT_NOT_FOUND", "EVENT_NOT_FOUND"));
    }

    public List<Event> findByEventIds(List<Long> eventId) {
        return eventRepository.findByIdIn(eventId);
    }
}
