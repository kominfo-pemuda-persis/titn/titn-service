package id.or.persis.pemuda.titn.service;

import id.or.persis.pemuda.titn.entity.ParticipantEvent;
import id.or.persis.pemuda.titn.repository.ParticipantEventRepository;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Project: titn-service
 * <p>
 * User: hasan.sanusi
 * Email: hasan.sanusi@dansmultipro.com
 * Telegram: @hasansanusi
 * Date: 27/08/24
 * Time: 19.55
 * <p>
 * Created with IntelliJ IDEA
 */
@Service
@Slf4j
@AllArgsConstructor
public class ParticipantEventService {
    private final ParticipantEventRepository participantEventRepository;

    public void saveAll(List<ParticipantEvent> participantEvents) {
        participantEventRepository.saveAll(participantEvents);
        log.info("Participant events saved : {}", participantEvents);
    }

    public void deleteByParticipantId(String participantId) {
        List<ParticipantEvent> participantEvents = participantEventRepository.findByIdParticipantId(participantId);
        participantEventRepository.deleteAll(participantEvents);
        log.info("deleteByParticipantId : {}", participantId);
    }
}
