package id.or.persis.pemuda.titn.controller;

import id.or.persis.pemuda.titn.service.QRCodeService;
import id.or.persis.pemuda.titn.utils.QRCodeGenerator;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.time.LocalDateTime;
import javax.imageio.ImageIO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by IntelliJ IDEA. Project : titn-service User: hendisantika Email:
 * hendisantika@gmail.com Telegram : @hendisantika34 Date: 5/21/23 Time: 14:35
 * To change this template use File | Settings | File Templates.
 */
@RestController
public class TestController {
    private static final String QR_CODE_IMAGE_PATH = "./src/main/resources/static/images/";

    @Autowired
    private QRCodeService qrCodeService;

    @GetMapping(value = "/generateAndDownloadQRCode/{codeText}")
    public String download(@PathVariable("codeText") String codeText) throws Exception {
        QRCodeGenerator.generateQRCodeImage(
                codeText, 350, 350, QR_CODE_IMAGE_PATH + codeText + "_" + System.currentTimeMillis() + "_QRCode.png");
        return "Success! " + LocalDateTime.now();
    }

    @GetMapping(value = "/generateQRCode/{codeText}")
    public ResponseEntity<byte[]> generateQRCode(@PathVariable("codeText") String codeText) throws Exception {
        return ResponseEntity.status(HttpStatus.OK).body(QRCodeGenerator.getQRCodeImage(codeText, 350, 350));
    }

    @GetMapping("/v1/qrcode")
    public void generateQRCode(
            HttpServletResponse response,
            @RequestParam String text,
            @RequestParam(defaultValue = "350") int width,
            @RequestParam(defaultValue = "350") int height)
            throws Exception {
        BufferedImage image = qrCodeService.generateQRCode(text, width, height);
        ServletOutputStream outputStream = response.getOutputStream();
        ImageIO.write(image, "png", outputStream);
        outputStream.flush();
        outputStream.close();
    }
}
