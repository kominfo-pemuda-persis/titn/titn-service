package id.or.persis.pemuda.titn.controller;

import static org.springframework.http.ResponseEntity.ok;

import id.or.persis.pemuda.titn.entity.Status;
import id.or.persis.pemuda.titn.payload.*;
import id.or.persis.pemuda.titn.service.ParticipantRecapService;
import id.or.persis.pemuda.titn.utils.BaseResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Project: titn-service
 * <p>
 * User: hasan.sanusi Email: hasan.sanusi@dansmultipro.com
 * Telegram: @hasansanusi Date: 11/06/24 Time: 19.57
 * <p>
 * Created with IntelliJ IDEA
 */
@RestController
@Tag(name = "Participant Recap", description = "Endpoint for managing Participant Recap")
@RequestMapping(value = Endpoint.RECAPS)
@Slf4j
@AllArgsConstructor
public class ParticipantRecapController {
    private final ParticipantRecapService participantRecapService;
    private static final int DEFAULT_PAGE_SIZE = 10;

    @PostMapping
    @Operation(
            summary = "Add New Participant Recap Data",
            description = "Add New Participant Recap Data.",
            tags = {"Participant Recap"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ParticipantRecapResponseDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    public ResponseEntity<BaseResponse<ParticipantRecapResponseDto>> create(
            @RequestBody @Valid ParticipantRecapRequestDto participantRequestDto, HttpServletRequest request) {
        BaseResponse<ParticipantRecapResponseDto> baseResponse =
                participantRecapService.create(participantRequestDto, request);
        return ok(baseResponse);
    }

    @GetMapping
    @Operation(
            summary = "Get all Participant recap Data",
            description = "Get all Participant recap Data.",
            tags = {"Participant Recap"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ParticipantRecapResponseListDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    public ResponseEntity<BaseResponse<Page<ParticipantRecapResponseListDto>>> getParticipants(
            @Parameter(description = "The size of the page to be returned")
                    @RequestParam(required = false, defaultValue = "10")
                    Integer pageSize,
            @Parameter(description = "Zero-based page index") @RequestParam(required = false, defaultValue = "0")
                    Integer pageNo,
            @Parameter(description = "Status   SUBMITTED | APPROVED | REJECTED") @RequestParam(required = false)
                    Status status,
            @Parameter(description = "keyword NPA or fullName or NIK") @RequestParam(required = false) String keyword,
            @Parameter(description = "Zero-based page sort") @RequestParam(required = false, defaultValue = "createdAt")
                    String sortBy,
            @Parameter(description = "return summary pd") @RequestParam(required = false) String pd,
            @Parameter(description = "return summary pw") @RequestParam(required = false) String pw,
            @Parameter(description = "return summary pc") @RequestParam(required = false) String pc,
            @Parameter(description = "Sort Order ASC or DESC ")
                    @RequestParam(value = "sortOrder", required = false, defaultValue = "DESC")
                    String sortOrder,
            HttpServletRequest request) {
        if (pageSize == null) {
            pageSize = DEFAULT_PAGE_SIZE;
        }
        if (pageNo == null) {
            pageNo = 0;
        }
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(Sort.Direction.valueOf(sortOrder), sortBy));
        BaseResponse<Page<ParticipantRecapResponseListDto>> listBaseResponse =
                participantRecapService.getAllParticipantRecap(keyword, status, paging, request, pc, pd, pw);
        return ok(listBaseResponse);
    }

    @PutMapping(value = "/{recapId}")
    @Operation(
            summary = "Update Status Participant Recap Data",
            description = "Update Status Participant Recap Data.",
            tags = {"Participant Recap"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ParticipantRecapResponseDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    public ResponseEntity<BaseResponse<ParticipantRecapResponseDto>> update(
            @RequestBody @Valid StatusRecapRequestDto statusRecapRequestDto,
            @PathVariable String recapId,
            HttpServletRequest request) {
        BaseResponse<ParticipantRecapResponseDto> baseResponse =
                participantRecapService.update(statusRecapRequestDto, recapId, request);
        return ok(baseResponse);
    }

    @DeleteMapping(value = "/{recapId}")
    @Operation(
            summary = "Delete Pemuda Persis Rekap Participant Data",
            description = "Delete Pemuda Persis Rekap  Participant Data.",
            tags = {"Participant Recap"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ParticipantRecapResponseDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    public ResponseEntity<BaseResponse<ParticipantRecapResponseDto>> delete(@PathVariable String recapId) {
        BaseResponse<ParticipantRecapResponseDto> baseResponse = participantRecapService.delete(recapId);
        return ok(baseResponse);
    }

    @PutMapping(value = "/notification/{recapId}")
    @Operation(
            summary = "Send Notification Email and WhatsApps Participant Approved By Recap Data",
            description = "Send Notification Email and WhatsApps Participant Approved By Recap Data.",
            tags = {"Participant Recap"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ParticipantRecapResponseDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    public ResponseEntity<BaseResponse<ParticipantRecapResponseDto>> sendNotification(@PathVariable String recapId) {
        BaseResponse<ParticipantRecapResponseDto> baseResponse = participantRecapService.sendNotification(recapId);
        return ok(baseResponse);
    }
}
