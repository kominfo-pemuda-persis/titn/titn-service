package id.or.persis.pemuda.titn.configuration;

import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;
import id.or.persis.pemuda.titn.payload.AnggotaUser;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.Instant;
import java.util.*;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.filter.OncePerRequestFilter;

@Component
@RequiredArgsConstructor
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    private final JWTVerifier jwtVerifier;
    private final AppConfig appConfig;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        if (validateUrl(request)) {
            filterChain.doFilter(request, response);
            return;
        }
        Optional.ofNullable(request.getHeader(HttpHeaders.AUTHORIZATION))
                .filter(header -> header.startsWith("Bearer "))
                .ifPresentOrElse(
                        header -> doAuth(header, request, response, filterChain),
                        () -> skipFilter(request, response, filterChain));
    }

    @SneakyThrows
    private static void skipFilter(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) {
        filterChain.doFilter(request, response);
    }

    @SneakyThrows
    private void doAuth(
            String header, HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) {
        var jwt = StringUtils.substring(header, 7);

        var loggedInUser = null != SecurityContextHolder.getContext().getAuthentication();
        if (loggedInUser) {
            filterChain.doFilter(request, response);
            return;
        }

        Optional.ofNullable(this.getSubject(jwt))
                .filter(sub -> this.isJwtValid(jwt, sub))
                .ifPresent(npa -> injectAuthenticationContext(npa, request));
        Map<String, Claim> claims = this.getClaim(jwt);
        AnggotaUser user = AnggotaUser.builder()
                .pc(Optional.ofNullable(claims.get("pc")).map(Claim::asString).orElse(null))
                .pw(Optional.ofNullable(claims.get("pw")).map(Claim::asString).orElse(null))
                .pd(Optional.ofNullable(claims.get("pd")).map(Claim::asString).orElse(null))
                .nama(Optional.ofNullable(claims.get("nama"))
                        .map(Claim::asString)
                        .orElse(null))
                .npa(Optional.ofNullable(claims.get("npa")).map(Claim::asString).orElse(null))
                .role(Optional.ofNullable(claims.get("role"))
                        .map(Claim::asString)
                        .orElse(null))
                .build();

        request.setAttribute("user", user);

        filterChain.doFilter(request, response);
    }

    private void injectAuthenticationContext(String npa, HttpServletRequest request) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        var authentication = new UsernamePasswordAuthenticationToken(npa, "titn!secret", authorities);
        authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    public Map<String, Claim> getClaim(String jwt) {
        return decodeJwt(jwt).getClaims();
    }

    public String getSubject(String jwt) {
        return decodeJwt(jwt).getSubject();
    }

    private DecodedJWT decodeJwt(String jwt) {
        return jwtVerifier.verify(jwt);
    }

    public boolean isJwtValid(String jwt, String subject) {
        var jwtNotExpired = Instant.now().isBefore(decodeJwt(jwt).getExpiresAt().toInstant());
        return subject.equals(getSubject(jwt)) && jwtNotExpired;
    }

    protected boolean validateUrl(HttpServletRequest request) {
        String[] whitelistUrl = appConfig.getWhitelistJwtPathUrl().split("\\|");
        List<String> excludeUrlPatterns = new ArrayList<>(Arrays.asList(whitelistUrl));
        AntPathMatcher pathMatcher = new AntPathMatcher();

        return excludeUrlPatterns.stream().anyMatch(p -> pathMatcher.match(p, request.getServletPath()));
    }
}
