package id.or.persis.pemuda.titn.repository;

import id.or.persis.pemuda.titn.entity.ParticipantEvent;
import id.or.persis.pemuda.titn.entity.ParticipantEventId;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Project: titn-service
 * <p>
 * User: hasan.sanusi
 * Email: hasan.sanusi@dansmultipro.com
 * Telegram: @hasansanusi
 * Date: 27/08/24
 * Time: 19.54
 * <p>
 * Created with IntelliJ IDEA
 */
@Repository
public interface ParticipantEventRepository extends JpaRepository<ParticipantEvent, ParticipantEventId> {

    List<ParticipantEvent> findByIdParticipantId(String participantId);
}
