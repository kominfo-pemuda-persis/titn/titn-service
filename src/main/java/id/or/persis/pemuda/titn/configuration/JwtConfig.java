package id.or.persis.pemuda.titn.configuration;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.JWTVerifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JwtConfig {
    private final AppConfig appConfig;

    public JwtConfig(AppConfig appConfig) {
        this.appConfig = appConfig;
    }

    @Bean
    JWTVerifier jwtVerifier() {
        return JWT.require(Algorithm.HMAC256(appConfig.getJwtSecret())).build();
    }
}
