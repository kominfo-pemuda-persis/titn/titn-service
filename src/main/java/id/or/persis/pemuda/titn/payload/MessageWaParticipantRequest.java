package id.or.persis.pemuda.titn.payload;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

/**
 * Project: titn-service
 * <p>
 * User: hasan.sanusi
 * Email: hasan.sanusi@dansmultipro.com
 * Telegram: @hasansanusi
 * Date: 14/07/24
 * Time: 07.08
 * <p>
 * Created with IntelliJ IDEA
 */
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class MessageWaParticipantRequest extends BaseMessageWaRequest {}
