package id.or.persis.pemuda.titn.service;

import static id.or.persis.pemuda.titn.service.ParticipantService.generateParticipantId;
import static id.or.persis.pemuda.titn.utils.ConstansTitnUtils.PESERTA_ESYAHADAH_TEMPLATE_HTML;

import id.or.persis.pemuda.titn.configuration.AppConfig;
import id.or.persis.pemuda.titn.configuration.WhatsAppsConfig;
import id.or.persis.pemuda.titn.entity.*;
import id.or.persis.pemuda.titn.payload.AnggotaUser;
import id.or.persis.pemuda.titn.payload.ConstanUtils;
import id.or.persis.pemuda.titn.payload.EsyahadahRequestDto;
import id.or.persis.pemuda.titn.payload.MessageWhatsAppsRequest;
import id.or.persis.pemuda.titn.repository.ESyahadahHistoryRepository;
import id.or.persis.pemuda.titn.utils.*;
import jakarta.servlet.http.HttpServletRequest;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.modelmapper.ModelMapper;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * Project: titn-service
 * <p>
 * User: hasan.sanusi
 * Email: hasan.sanusi@dansmultipro.com
 * Telegram: @hasansanusi
 * Date: 06/10/24
 * Time: 19.36
 * <p>
 * Created with IntelliJ IDEA
 */
@Service
@Slf4j
@AllArgsConstructor
public class ESyahadahService {
    private final ESyahadahHistoryRepository eSyahadahHistoryRepository;
    private final ParticipantService participantService;
    private final ModelMapper modelMapper;
    private final AppConfig appConfig;
    private final MailService mailService;
    private final MessageWhatsAppsService whatsAppsService;
    private final WhatsAppsConfig whatsAppsConfig;

    public BaseResponse<String> generateEsyahadah(
            EsyahadahRequestDto requestDto, HttpServletRequest httpServletRequest) {
        final long start = System.nanoTime();
        AnggotaUser angotaUser = prepareDataUser(httpServletRequest);
        Participant participant = validateData(requestDto);
        requestDto.setNpa(participant.getNpa());
        EsyahadahHistory eSyahadahHistory = prepareData(requestDto, angotaUser);
        eSyahadahHistory.setParticipantId(participant.getParticipantId());
        eSyahadahHistory.setParticipantId(participant.getParticipantId());
        eSyahadahHistory.setNik(participant.getNik());
        eSyahadahHistory.setFullName(participant.getFullName());
        eSyahadahHistory.setPcName(participant.getPcName());
        eSyahadahHistory.setEmail(participant.getEmail());
        eSyahadahHistory.setPhone(participant.getPhone());
        eSyahadahHistory.setHistoryId(generateParticipantId("SYH"));
        eSyahadahHistoryRepository.save(eSyahadahHistory);
        sendEsyahadahEmail(PESERTA_ESYAHADAH_TEMPLATE_HTML, "EVENT", eSyahadahHistory);
        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(
                HttpStatus.OK, ((end - start) / 1000000), GlobalMessage.SUCCESS.message, null);
    }

    private EsyahadahHistory prepareData(EsyahadahRequestDto requestDto, AnggotaUser angotaUser) {
        EsyahadahHistory esyahadahHistory = modelMapper.map(requestDto, EsyahadahHistory.class);
        esyahadahHistory.setEvent(requestDto.getContest());
        esyahadahHistory.setCreatedBy(angotaUser.getNpa());
        esyahadahHistory.setCreatedAt(LocalDateTime.now());
        esyahadahHistory.setEmailNotificationStatus("IN_PROGRESS");
        esyahadahHistory.setEmailNotificationStatusDesc("Esyahadah lagi Di Proses");
        return esyahadahHistory;
    }

    private Participant validateData(EsyahadahRequestDto request) {
        Participant participant = participantService.findParticipantByIdOrNpa(request.getNpa());
        if (!request.isForce()) {
            List<EsyahadahHistory> esyahadahSummaries = eSyahadahHistoryRepository.findByNpa(request.getNpa());
            if (!esyahadahSummaries.isEmpty()) {
                throw new BusinessException(
                        HttpStatus.BAD_REQUEST,
                        "E_SYAHADAH_ALREADY_EXISTS",
                        "E-Syahadah Sudah Pernah di generate sebanyak " + esyahadahSummaries.size() + " kali");
            }
        }
        return participant;
    }

    private AnggotaUser prepareDataUser(HttpServletRequest request) {
        AnggotaUser anggotaUser = (AnggotaUser) request.getAttribute("user");
        if (anggotaUser == null) {
            throw new BusinessException(HttpStatus.UNAUTHORIZED, "UNAUTHORIZED");
        }
        switch (anggotaUser.getRole().toLowerCase()) {
            case "anggota", "admin_pc": {
                break;
            }
            case "admin_pw": {
                anggotaUser.setPc(null);
                anggotaUser.setPd(null);
                break;
            }
            case "admin_pd": {
                anggotaUser.setPc(null);
                break;
            }
            default: {
                anggotaUser.setPw(null);
                anggotaUser.setPc(null);
                anggotaUser.setPd(null);
                break;
            }
        }
        return anggotaUser;
    }

    public BaseResponse<String> generateEsyahadahPeserta(String participantId, HttpServletRequest request) {
        final long start = System.nanoTime();
        AnggotaUser angotaUser = prepareDataUser(request);
        Participant participant = participantService.findParticipantByIdOrNpa(participantId);
        EsyahadahRequestDto requestDto = EsyahadahRequestDto.builder()
                .npa(participant.getNpa())
                .rank("-")
                .competition(participant.getEvents().stream()
                        .map(ParticipantEvent::getEvent)
                        .map(event -> event.getType().toString())
                        .distinct()
                        .collect(Collectors.joining("| ")))
                .contest(participant.getEvents().stream()
                        .map(ParticipantEvent::getEvent)
                        .map(Event::getDescription)
                        .distinct()
                        .collect(Collectors.joining("| ")))
                .build();
        EsyahadahHistory eSyahadahHistory = prepareData(requestDto, angotaUser);
        eSyahadahHistory.setParticipantId(participant.getParticipantId());
        eSyahadahHistory.setNik(participant.getNik());
        eSyahadahHistory.setFullName(participant.getFullName());
        eSyahadahHistory.setPcName(participant.getPcName());
        eSyahadahHistory.setEmail(participant.getEmail());
        eSyahadahHistory.setPhone(participant.getPhone());
        eSyahadahHistory.setHistoryId(generateParticipantId("SYH"));
        eSyahadahHistoryRepository.save(eSyahadahHistory);
        senEmailEsyahadahAsync(List.of(eSyahadahHistory), false);
        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(
                HttpStatus.OK, ((end - start) / 1000000), GlobalMessage.SUCCESS.message, null);
    }

    public void senEmailEsyahadahAsync(List<EsyahadahHistory> esyahadahSummaries, boolean isEvent) {
        ExecutorService pool = Executors.newFixedThreadPool(10);
        CompletableFuture.runAsync(
                () -> {
                    List<Map<String, Object>> resultList = this.executeEsayhadahSendEmail(esyahadahSummaries, isEvent);
                    log.info("[E-SYAHADAH SEND EMAIL NOTIFICATION ASYNC] with {}", resultList);
                },
                pool);
    }

    private List<String> sendEsyahadahEmail(String template, String type, EsyahadahHistory esyahadahHistory) {
        List<String> result = new ArrayList<>();
        Map<String, Object> content = new HashMap<>();
        content.put("FULL_NAME", esyahadahHistory.getFullName());
        content.put(
                "NPA",
                Optional.ofNullable(esyahadahHistory.getNpa())
                        .map(s -> "NPA : " + s)
                        .orElse("NIK :"
                                .concat(Optional.ofNullable(esyahadahHistory.getNik())
                                        .orElse("-"))));
        if (!"EVENT".equals(type)) {
            content.put(
                    "DESC", esyahadahHistory.getContest().toLowerCase().contains("panitia") ? "PANITIA" : "PESERTA");
        } else {
            content.put("RANK", esyahadahHistory.getRank().toUpperCase());
            content.put("COMPETITION", esyahadahHistory.getCompetition().toUpperCase());
            content.put("CONTEST", esyahadahHistory.getContest());
        }
        content.put("PC_NAME", esyahadahHistory.getPcName());
        String emailFrom = appConfig
                .getEmailFromName()
                .concat("<")
                .concat(appConfig.getEmailFromAddress())
                .concat(">");
        String notificationStatus = "SUCCESS";
        String notificationStatusDesc = "Email Berhasil di kirim";
        try {
            Map<String, Object> data = new HashMap<>(content);
            data.put("IMAGE_ESYAHADAH", new ClassPathResource("/static/images/e-syahadah-rev.png").getInputStream());
            String eSyahadahTemplate = "e_syahadah_event.jrxml";
            if (!"EVENT".equals(type)) {
                eSyahadahTemplate = "e_syahadah_peserta_panitia.jrxml";
            }
            ByteArrayResource nameTagPdf = generateEsyahadagPdf(data, eSyahadahTemplate);
            String baseFileName = "E_SYAHADAH_"
                    .concat(esyahadahHistory.getParticipantId())
                    .concat("_")
                    .concat(esyahadahHistory.getFullName());
            mailService.sendMailEsyahadah(
                    content,
                    "E-Syahadah TITN 2024",
                    esyahadahHistory.getEmail(),
                    template,
                    emailFrom,
                    nameTagPdf,
                    baseFileName);
            log.info(
                    "[SUCCESS_SEND_EMAIL_E_SYAHADAH] with participant_id:{} template {}",
                    esyahadahHistory.getParticipantId(),
                    template);
        } catch (Exception e) {
            log.error("[ERROR_SEND_EMAIL_E-SAYAHADAH]", e);
            notificationStatus = "FAILED";
            notificationStatusDesc = StringUtils.substring(e.getMessage(), 100);
            result.add(esyahadahHistory.getParticipantId());
        } finally {
            this.updateEmailStatus(esyahadahHistory.getHistoryId(), notificationStatus, notificationStatusDesc);
        }
        if (Objects.nonNull(ContactNumberUtil.getValidContactNumber(esyahadahHistory.getPhone())))
            sendNotificationWa(esyahadahHistory.getParticipantId(), esyahadahHistory.getPhone());
        return result;
    }

    private void updateEmailStatus(String idHistory, String notificationStatus, String notificationStatusDesc) {
        eSyahadahHistoryRepository.findByHistoryId(idHistory).ifPresent(esyahadahHistory -> {
            esyahadahHistory.setEmailNotificationStatus(notificationStatus);
            esyahadahHistory.setEmailNotificationStatusDesc(notificationStatusDesc);
            eSyahadahHistoryRepository.save(esyahadahHistory);
        });
    }

    public void sendNotificationWa(String participantId, String phone) {
        MessageWhatsAppsRequest request = prepareWaRequest(participantId, phone);
        List<Map<String, Object>> resultList =
                whatsAppsService.executeSingleSendingMessage(Collections.singletonList(request));
        log.info("[SEND WA Esyhadah]Sending message whatsApps {}", resultList);
    }

    private MessageWhatsAppsRequest prepareWaRequest(String participantId, String phone) {
        return MessageWhatsAppsRequest.builder()
                .message(ConstanUtils.TEMPLATE_ESYAHADAH)
                .participantId(participantId)
                .token(whatsAppsConfig.getToken())
                .type("image")
                .delay(whatsAppsConfig.getDelay())
                .target(ContactNumberUtil.getValidContactNumber(phone))
                .url(whatsAppsConfig.getImageUrl())
                .build();
    }

    private ByteArrayResource generateEsyahadagPdf(Map<String, Object> data, String template) {
        try {
            List<JasperPrint> jasperPrintList = new ArrayList<>();
            jasperPrintList.add(GenerateNameTagUtils.executeJasperPrint(data, template));
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            JRPdfExporter exporter = new JRPdfExporter();
            exporter.setExporterInput(SimpleExporterInput.getInstance(jasperPrintList));
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(byteArrayOutputStream));
            SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
            exporter.setConfiguration(configuration);
            exporter.exportReport();
            byte[] pdfBytes = byteArrayOutputStream.toByteArray();
            return new ByteArrayResource(pdfBytes);
        } catch (Exception e) {
            log.info("[ERROR_GENERATE_ESYAHADAH_PDF] {}", e.getMessage());
        }

        return null;
    }

    public BaseResponse<String> generateEsyahadahByRecapId(String recapId, HttpServletRequest request) {
        final long start = System.nanoTime();
        AnggotaUser angotaUser = prepareDataUser(request);
        List<Participant> participants = participantService.getContactParticipantByRecapIdPeserta(recapId);
        if (participants.isEmpty()) {
            throw new BusinessException(HttpStatus.BAD_REQUEST, "DATA_NOT_FOUND", "Data recap tidak ditemukan");
        }
        List<EsyahadahHistory> esyahadahHistories = participants.stream()
                .map(participant -> {
                    EsyahadahRequestDto requestDto = EsyahadahRequestDto.builder()
                            .npa(participant.getNpa())
                            .rank("-")
                            .competition(participant.getEvents().stream()
                                    .map(ParticipantEvent::getEvent)
                                    .map(event -> event.getType().toString())
                                    .distinct()
                                    .collect(Collectors.joining("| ")))
                            .contest(participant.getEvents().stream()
                                    .map(ParticipantEvent::getEvent)
                                    .map(Event::getDescription)
                                    .distinct()
                                    .collect(Collectors.joining("| ")))
                            .build();
                    EsyahadahHistory eSyahadahHistory = prepareData(requestDto, angotaUser);
                    eSyahadahHistory.setParticipantId(participant.getParticipantId());
                    eSyahadahHistory.setNik(participant.getNik());
                    eSyahadahHistory.setFullName(participant.getFullName());
                    eSyahadahHistory.setPcName(participant.getPcName());
                    eSyahadahHistory.setEmail(participant.getEmail());
                    eSyahadahHistory.setPhone(participant.getPhone());
                    eSyahadahHistory.setHistoryId(generateParticipantId("SYH"));
                    return eSyahadahHistory;
                })
                .toList();
        eSyahadahHistoryRepository.saveAllAndFlush(esyahadahHistories);
        this.senEmailEsyahadahAsync(esyahadahHistories, false);
        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(
                HttpStatus.OK, ((end - start) / 1000000), GlobalMessage.SUCCESS.message, null);
    }

    public List<Map<String, Object>> executeEsayhadahSendEmail(List<EsyahadahHistory> request, boolean isEvent) {
        List<Map<String, Object>> errorList = new ArrayList<>();
        List<CompletableFuture<Map<String, Object>>> futures = new ArrayList<>();
        request.forEach(esyahadahHistory -> {
            CompletableFuture<Map<String, Object>> batchFutures = sendBatchSingle(esyahadahHistory, isEvent);
            futures.add(batchFutures);
        });
        CompletableFuture<Void> batchFuture = CompletableFuture.allOf(futures.toArray(new CompletableFuture[0]));
        try {
            batchFuture.get();
            futures.forEach(future -> {
                try {
                    if (!future.get().isEmpty()) errorList.add(future.get());
                } catch (InterruptedException | ExecutionException e) {
                    log.info("[E-SYAHADAH SEND EMAIL NOTIFICATION ERROR ASYNC] {}", e.getMessage());
                }
            });
        } catch (InterruptedException | ExecutionException e) {
            log.info("[E-SYAHADAH SEND EMAIL NOTIFICATION ASYNC-2] {}", e.getMessage());
        }
        return errorList;
    }

    public CompletableFuture<Map<String, Object>> sendBatchSingle(EsyahadahHistory esyahadahHistory, boolean isEvent) {
        ExecutorService pool = Executors.newFixedThreadPool(10);
        return CompletableFuture.supplyAsync(
                () -> {
                    Map<String, Object> error = new HashMap<>();
                    List<String> result = this.sendEsyahadahEmail(
                            PESERTA_ESYAHADAH_TEMPLATE_HTML,
                            isEvent ? "EVENT" : esyahadahHistory.getContest(),
                            esyahadahHistory);
                    if (!result.isEmpty()) {
                        error.put("message", "E-syahadah Email send failed");
                        error.put("emailList", result);
                    }
                    log.info("[send Esyahadah email  notification] {}", result);
                    return error;
                },
                pool);
    }

    public List<EsyahadahHistory> readExcel(MultipartFile file, AnggotaUser angotaUser) throws IOException {
        List<EsyahadahHistory> esyahadahRequestDtos = new ArrayList<>();
        Workbook workbook = new XSSFWorkbook(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        for (int i = 1; i <= sheet.getLastRowNum(); i++) {
            Row row = sheet.getRow(i);
            if (row != null && !isRowEmpty(row)) {
                String nama = getCellValueAsString(row.getCell(0));
                String npa = getCellValueAsString(row.getCell(1));
                String pdName = getCellValueAsString(row.getCell(2));
                String rank = getCellValueAsString(row.getCell(3));
                String contest = getCellValueAsString(row.getCell(4));
                String kompetisi = getCellValueAsString(row.getCell(5));
                Participant participant = participantService.findParticipantByNpa(npa);
                if (Objects.nonNull(participant)) {
                    EsyahadahHistory esyahadahRequestDto = EsyahadahHistory.builder()
                            .contest(contest)
                            .npa(npa)
                            .rank(rank)
                            .competition(kompetisi)
                            .fullName(nama)
                            .pcName(pdName)
                            .participantId(participant.getParticipantId())
                            .email(participant.getEmail())
                            .nik(participant.getNik())
                            .phone(participant.getPhone())
                            .createdAt(LocalDateTime.now())
                            .createdBy(angotaUser.getNpa())
                            .build();
                    esyahadahRequestDtos.add(esyahadahRequestDto);
                } else {
                    log.info("[read excel file] participant notFound {}", npa);
                }
            }
        }

        workbook.close();
        return esyahadahRequestDtos;
    }

    private boolean isRowEmpty(Row row) {
        for (int cellIndex = 0; cellIndex < row.getLastCellNum(); cellIndex++) {
            Cell cell = row.getCell(cellIndex);
            if (cell != null && cell.getCellType() != CellType.BLANK) {
                return false;
            }
        }
        return true;
    }

    private String getCellValueAsString(Cell cell) {
        if (cell == null) {
            return "";
        }
        switch (cell.getCellType()) {
            case STRING:
                return cell.getStringCellValue();
            case NUMERIC:
                if (DateUtil.isCellDateFormatted(cell)) {
                    return cell.getDateCellValue().toString();
                } else {
                    return String.valueOf((int) cell.getNumericCellValue());
                }
            case BOOLEAN:
                return String.valueOf(cell.getBooleanCellValue());
            case FORMULA:
                return cell.getCellFormula();
            default:
                return "";
        }
    }

    @SneakyThrows
    public BaseResponse<String> generateUpload(MultipartFile file, HttpServletRequest request) {
        final long start = System.nanoTime();
        AnggotaUser angotaUser = prepareDataUser(request);
        List<EsyahadahHistory> esyahadahRequestDtos = readExcel(file, angotaUser);
        if (!esyahadahRequestDtos.isEmpty()) {
            eSyahadahHistoryRepository.saveAllAndFlush(esyahadahRequestDtos);
            this.senEmailEsyahadahAsync(esyahadahRequestDtos, true);
        }
        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(
                HttpStatus.OK, ((end - start) / 1000000), GlobalMessage.SUCCESS.message, null);
    }
}
