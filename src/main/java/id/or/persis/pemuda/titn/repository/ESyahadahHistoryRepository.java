package id.or.persis.pemuda.titn.repository;

import id.or.persis.pemuda.titn.entity.EsyahadahHistory;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Project: titn-service
 * <p>
 * User: hasan.sanusi
 * Email: hasan.sanusi@dansmultipro.com
 * Telegram: @hasansanusi
 * Date: 06/10/24
 * Time: 19.32
 * <p>
 * Created with IntelliJ IDEA
 */
@Repository
public interface ESyahadahHistoryRepository extends JpaRepository<EsyahadahHistory, Long> {
    List<EsyahadahHistory> findByNpa(String npa);

    Optional<EsyahadahHistory> findByHistoryId(String historyId);
}
