package id.or.persis.pemuda.titn.entity;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

/**
 * Project: titn-service
 * <p>
 * User: hasan.sanusi
 * Email: hasan.sanusi@dansmultipro.com
 * Telegram: @hasansanusi
 * Date: 11/08/24
 * Time: 22.18
 * <p>
 * Created with IntelliJ IDEA
 */
@Entity
@Table(name = "t_idea_team")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class IdeaTeam extends Updatable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "event")
    @ToString.Exclude
    @NotFound(action = NotFoundAction.IGNORE)
    private Event event;

    private String team;
}
