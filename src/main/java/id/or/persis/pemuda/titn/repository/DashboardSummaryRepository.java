package id.or.persis.pemuda.titn.repository;

import id.or.persis.pemuda.titn.payload.DashboardPcSummary;
import id.or.persis.pemuda.titn.payload.DashboardSummary;
import id.or.persis.pemuda.titn.payload.DashboardSummaryResponse;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * Project: titn-service
 * <p>
 * User: hasan.sanusi
 * Email: hasan.sanusi@dansmultipro.com
 * Telegram: @hasansanusi
 * Date: 17/07/24
 * Time: 22.02
 * <p>
 * Created with IntelliJ IDEA
 */
@Service
public class DashboardSummaryRepository {
    public static final String baseSummaryQuery = "SELECT\n"
            + "    COUNT(CASE WHEN status = 'SUBMITTED' THEN 1 END) as totalJumlahCalonPeserta,\n"
            + "    COUNT(CASE WHEN (status = 'SUBMITTED' and (npa IS NOT NULL OR npa <> '')) THEN 1 END) as totalJumlahCalonPesertaAnggota,\n"
            + "    COUNT(CASE WHEN (status = 'SUBMITTED' and (npa IS NULL OR npa = '')) THEN 1 END) as totalJumlahCalonPesertaNonAnggota,\n"
            + "    COUNT(CASE WHEN (status = 'APPROVED'OR status='CHECKED_IN') THEN 1 END) as totalJumlahPeserta,\n"
            + "    COUNT(CASE WHEN ((status = 'APPROVED'OR status='CHECKED_IN') and (npa IS NOT NULL OR npa <> '')) THEN 1 END) as totalJumlahPesertaAnggota,\n"
            + "    COUNT(CASE WHEN ((status = 'APPROVED'OR status='CHECKED_IN') and (npa IS NULL OR npa = '')) THEN 1 END) as totalJumlahPesertaNonAnggota, \n"
            + "    COUNT(CASE WHEN status='CHECKED_IN' THEN 1 END) as totalJumlahPesertaChecked,\n"
            + "    COUNT(CASE WHEN (status='CHECKED_IN' and (npa IS NOT NULL OR npa <> '')) THEN 1 END) as totalJumlahPesertaAnggotaChecked,\n"
            + "    COUNT(CASE WHEN status='CHECKED_IN' and (npa IS NULL OR npa = '') THEN 1 END) as totalJumlahPesertaNonAnggotaChecked \n"
            + " from t_participant p ";

    public static final String baseSummaryQueryPanitia = "SELECT\n"
            + "    COUNT(CASE WHEN status = 'SUBMITTED' THEN 1 END) as totalJumlahCalonPanitia,\n"
            + "    COUNT(CASE WHEN (status = 'APPROVED'OR status='CHECKED_IN') THEN 1 END) as totalJumlahPanitia,\n"
            + "    COUNT(CASE WHEN status='CHECKED_IN' THEN 1 END) as totalJumlahPanitiaChecked \n"
            + " from t_participant p ";

    @PersistenceContext
    private EntityManager entityManager;

    public List<DashboardSummary> getEventSummary(String pd, String pc, String pw) {
        String sql = "SELECT\n" + "    e.type,\n"
                + "    e.name,\n"
                + "    COUNT(pe.participant_id) AS value\n"
                + "FROM\n"
                + "    t_participant_event pe\n" + "           JOIN t_event e on e.id=pe.event_id\n"
                + "           JOIN t_participant p on p.participant_id = pe.participant_id ";
        Query query;
        String groupBy = "GROUP BY  e.type, e.name";
        String whereQuery = StringUtils.EMPTY;
        if (!StringUtils.isEmpty(pc)) {
            whereQuery = setWhereQuery(whereQuery, " and p.pc_code=:pc ", "");
        }
        if (!StringUtils.isEmpty(pd)) {
            whereQuery = setWhereQuery(whereQuery, " and p.pd_code=:pd ", StringUtils.isEmpty(whereQuery) ? "" : "  ");
        }
        if (!StringUtils.isEmpty(pw)) {
            whereQuery = setWhereQuery(whereQuery, " and p.pw_code=:pw ", StringUtils.isEmpty(whereQuery) ? "" : " ");
        }

        sql += whereQuery.concat(groupBy);
        query = entityManager.createNativeQuery(sql);
        if (!StringUtils.isEmpty(pc)) {
            query.setParameter("pc", pc);
        }
        if (!StringUtils.isEmpty(pd)) {
            query.setParameter("pd", pd);
        }
        if (!StringUtils.isEmpty(pw)) {
            query.setParameter("pw", pw);
        }
        @SuppressWarnings("unchecked")
        List<Object[]> results = query.getResultList();

        return results.stream()
                .map(result -> DashboardSummary.builder()
                        .type((String) result[0])
                        .name((String) result[1])
                        .value((Long) result[2])
                        .build())
                .toList();
    }

    private String setWhereQuery(String prevValue, String newValue, String and) {
        return StringUtils.isEmpty(prevValue)
                ? " where p.is_deleted=0 ".concat(newValue)
                : prevValue.concat(and).concat(newValue);
    }

    private String setWhereQueryWoPanitia(String prevValue, String newValue, String and) {
        return StringUtils.isEmpty(prevValue)
                ? " where p.participant_id not in(select distinct participant_id from t_participant_event where (event_id=21 or event_id =22 or event_id=23)) and p.is_deleted=0  "
                        .concat(newValue)
                : prevValue.concat(and).concat(newValue);
    }

    private String setWhereQueryPanitia(String prevValue, String newValue, String and) {
        return StringUtils.isEmpty(prevValue)
                ? " where p.participant_id in(select distinct participant_id from t_participant_event where (event_id=21 or event_id =22)) and p.is_deleted=0  "
                        .concat(newValue)
                : prevValue.concat(and).concat(newValue);
    }

    public DashboardSummaryResponse getParticipantSummary(String pd, String pc, String pw) {
        String sql = baseSummaryQuery;
        Query query;
        String whereQuery = StringUtils.EMPTY;
        if (!StringUtils.isEmpty(pc)) {
            whereQuery = setWhereQuery(whereQuery, " and p.pc_code=:pc ", "");
        }
        if (!StringUtils.isEmpty(pd)) {
            whereQuery = setWhereQuery(whereQuery, " and p.pd_code=:pd ", StringUtils.isEmpty(whereQuery) ? "" : " ");
        }
        if (!StringUtils.isEmpty(pw)) {
            whereQuery = setWhereQuery(whereQuery, " and p.pw_code=:pw ", StringUtils.isEmpty(whereQuery) ? "" : " ");
        }

        sql += whereQuery;
        query = entityManager.createNativeQuery(sql);
        if (!StringUtils.isEmpty(pc)) {
            query.setParameter("pc", pc);
        }
        if (!StringUtils.isEmpty(pd)) {
            query.setParameter("pd", pd);
        }
        if (!StringUtils.isEmpty(pw)) {
            query.setParameter("pw", pw);
        }

        @SuppressWarnings("unchecked")
        List<Object[]> results = query.getResultList();

        return results.stream()
                .findFirst()
                .map(result -> DashboardSummaryResponse.builder()
                        .totalJumlahCalonPeserta((Long) result[0])
                        .totalJumlahCalonPesertaAnggota((Long) result[1])
                        .totalJumlahCalonPesertaNonAnggota((Long) result[2])
                        .totalJumlahPeserta((Long) result[3])
                        .totalJumlahPesertaAnggota((Long) result[4])
                        .totalJumlahPesertaNonAnggota((Long) result[5])
                        .totalJumlahPesertaChecked((Long) result[6])
                        .totalJumlahPesertaAnggotaChecked((Long) result[7])
                        .totalJumlahPesertaNonAnggotaChecked((Long) result[8])
                        .build())
                .orElse(DashboardSummaryResponse.builder().build());
    }

    public DashboardSummaryResponse getParticipantWoPanitiaSummary(String pd, String pc, String pw) {
        String sql = baseSummaryQuery;
        Query query;
        String whereQuery =
                " where p.participant_id not in(select distinct participant_id from t_participant_event where (event_id=21 or event_id =22 or event_id=23)) and p.is_deleted=0 ";
        if (!StringUtils.isEmpty(pc)) {
            whereQuery = setWhereQueryWoPanitia(whereQuery, " and p.pc_code=:pc ", "");
        }
        if (!StringUtils.isEmpty(pd)) {
            whereQuery = setWhereQueryWoPanitia(
                    whereQuery, " and p.pd_code=:pd ", StringUtils.isEmpty(whereQuery) ? "" : " ");
        }
        if (!StringUtils.isEmpty(pw)) {
            whereQuery = setWhereQueryWoPanitia(
                    whereQuery, " and p.pw_code=:pw ", StringUtils.isEmpty(whereQuery) ? "" : " ");
        }

        sql += whereQuery;
        query = entityManager.createNativeQuery(sql);
        if (!StringUtils.isEmpty(pc)) {
            query.setParameter("pc", pc);
        }
        if (!StringUtils.isEmpty(pd)) {
            query.setParameter("pd", pd);
        }
        if (!StringUtils.isEmpty(pw)) {
            query.setParameter("pw", pw);
        }

        @SuppressWarnings("unchecked")
        List<Object[]> results = query.getResultList();

        return results.stream()
                .findFirst()
                .map(result -> DashboardSummaryResponse.builder()
                        .totalJumlahCalonPeserta((Long) result[0])
                        .totalJumlahCalonPesertaAnggota((Long) result[1])
                        .totalJumlahCalonPesertaNonAnggota((Long) result[2])
                        .totalJumlahPeserta((Long) result[3])
                        .totalJumlahPesertaAnggota((Long) result[4])
                        .totalJumlahPesertaNonAnggota((Long) result[5])
                        .totalJumlahPesertaChecked((Long) result[6])
                        .totalJumlahPesertaAnggotaChecked((Long) result[7])
                        .totalJumlahPesertaNonAnggotaChecked((Long) result[8])
                        .build())
                .orElse(DashboardSummaryResponse.builder().build());
    }

    public List<DashboardPcSummary> getClothesSizeSummary(String pd, String pc, String pw) {
        String sql = "SELECT\n" + "    pc_code,\n"
                + "    pc_name,\n"
                + "    clothes_size,\n"
                + "    COUNT(*) AS jumlah\n"
                + "FROM\n"
                + "    t_participant p \n";
        Query query;
        String groupBy = "GROUP BY\n" + "    pc_code,\n" + "    pc_name,\n" + "    clothes_size\n";
        String whereQuery =
                " where (p.status = 'APPROVED' OR p.status='CHECKED_IN') and p.participant_id not in(select distinct participant_id from t_participant_event where (event_id=21 or event_id =22 or event_id=23)) and p.is_deleted=0 ";
        ;
        if (!StringUtils.isEmpty(pc)) {
            whereQuery = setWhereQuery(whereQuery, " and p.pc_code=:pc ", "");
        }
        if (!StringUtils.isEmpty(pd)) {
            whereQuery = setWhereQuery(whereQuery, " and p.pd_code=:pd ", StringUtils.isEmpty(whereQuery) ? "" : "  ");
        }
        if (!StringUtils.isEmpty(pw)) {
            whereQuery = setWhereQuery(whereQuery, " and p.pw_code=:pw ", StringUtils.isEmpty(whereQuery) ? "" : " ");
        }

        String orderBy = " ORDER BY\n" + "    pc_code,pc_name, clothes_size";

        sql += whereQuery.concat(groupBy).concat(orderBy);
        query = entityManager.createNativeQuery(sql);
        if (!StringUtils.isEmpty(pc)) {
            query.setParameter("pc", pc);
        }
        if (!StringUtils.isEmpty(pd)) {
            query.setParameter("pd", pd);
        }
        if (!StringUtils.isEmpty(pw)) {
            query.setParameter("pw", pw);
        }
        @SuppressWarnings("unchecked")
        List<Object[]> results = query.getResultList();

        return results.stream()
                .map(result -> DashboardPcSummary.builder()
                        .pcCode((String) result[0])
                        .pcName((String) result[1])
                        .clothesSize((String) result[2])
                        .value((Long) result[3])
                        .build())
                .toList();
    }

    public DashboardSummaryResponse getParticipantPanitiaSummary(String pd, String pc, String pw) {
        String sql = baseSummaryQueryPanitia;
        Query query;
        String whereQuery =
                " where p.participant_id in(select distinct participant_id from t_participant_event where (event_id=21 or event_id =22)) and p.is_deleted=0 ";
        if (!StringUtils.isEmpty(pc)) {
            whereQuery = setWhereQueryPanitia(whereQuery, " and p.pc_code=:pc ", "");
        }
        if (!StringUtils.isEmpty(pd)) {
            whereQuery =
                    setWhereQueryPanitia(whereQuery, " and p.pd_code=:pd ", StringUtils.isEmpty(whereQuery) ? "" : " ");
        }
        if (!StringUtils.isEmpty(pw)) {
            whereQuery =
                    setWhereQueryPanitia(whereQuery, " and p.pw_code=:pw ", StringUtils.isEmpty(whereQuery) ? "" : " ");
        }

        sql += whereQuery;
        query = entityManager.createNativeQuery(sql);
        if (!StringUtils.isEmpty(pc)) {
            query.setParameter("pc", pc);
        }
        if (!StringUtils.isEmpty(pd)) {
            query.setParameter("pd", pd);
        }
        if (!StringUtils.isEmpty(pw)) {
            query.setParameter("pw", pw);
        }

        @SuppressWarnings("unchecked")
        List<Object[]> results = query.getResultList();

        return results.stream()
                .findFirst()
                .map(result -> DashboardSummaryResponse.builder()
                        .totalJumlahCalonPanitia((Long) result[0])
                        .totalJumlahPanitia((Long) result[1])
                        .totalJumlahPanitiaChecked((Long) result[2])
                        .build())
                .orElse(DashboardSummaryResponse.builder().build());
    }

    public DashboardSummaryResponse getParticipantPengunjungSummary(String pd, String pc, String pw) {
        String sql = baseSummaryQueryPanitia;
        Query query;
        String whereQuery =
                " where p.participant_id in(select distinct participant_id from t_participant_event where event_id=23) and p.is_deleted=0 ";
        if (!StringUtils.isEmpty(pc)) {
            whereQuery = setWhereQueryPanitia(whereQuery, " and p.pc_code=:pc ", "");
        }
        if (!StringUtils.isEmpty(pd)) {
            whereQuery =
                    setWhereQueryPanitia(whereQuery, " and p.pd_code=:pd ", StringUtils.isEmpty(whereQuery) ? "" : " ");
        }
        if (!StringUtils.isEmpty(pw)) {
            whereQuery =
                    setWhereQueryPanitia(whereQuery, " and p.pw_code=:pw ", StringUtils.isEmpty(whereQuery) ? "" : " ");
        }

        sql += whereQuery;
        query = entityManager.createNativeQuery(sql);
        if (!StringUtils.isEmpty(pc)) {
            query.setParameter("pc", pc);
        }
        if (!StringUtils.isEmpty(pd)) {
            query.setParameter("pd", pd);
        }
        if (!StringUtils.isEmpty(pw)) {
            query.setParameter("pw", pw);
        }

        @SuppressWarnings("unchecked")
        List<Object[]> results = query.getResultList();

        return results.stream()
                .findFirst()
                .map(result -> DashboardSummaryResponse.builder()
                        .totalJumlahCalonPengunjung((Long) result[0])
                        .totalJumlahPengunjung((Long) result[1])
                        .totalJumlahPengujungChecked((Long) result[2])
                        .build())
                .orElse(DashboardSummaryResponse.builder().build());
    }
}
