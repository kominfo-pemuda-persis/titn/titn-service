package id.or.persis.pemuda.titn.service;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import id.or.persis.pemuda.titn.configuration.AppConfig;
import id.or.persis.pemuda.titn.configuration.WhatsAppsConfig;
import id.or.persis.pemuda.titn.entity.*;
import id.or.persis.pemuda.titn.payload.*;
import id.or.persis.pemuda.titn.repository.ParticipantRepository;
import id.or.persis.pemuda.titn.utils.*;
import jakarta.servlet.http.HttpServletRequest;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import javax.imageio.ImageIO;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
public class ParticipantService {
    private static final String BASE_PREFIX_IMAGE_NAME_TAG = "images/idcard/";
    public static final String CALON_PESERTA_TEMPLATE_HTML = "calon-peserta-template.html";
    public static final String PESERTA_TEMPLATE_HTML = "peserta-template.html";
    public static final String URL_DEFAULT_IMAGE =
            "https://kominfo-ana-online-dev2.s3-ap-southeast-1.amazonaws.com/static/default.png";
    private final ParticipantRepository participantRepository;
    private final ModelMapper modelMapper;
    private final MailService mailService;
    private final AppConfig appConfig;
    private final EventService eventService;
    private final StorageService storageService;
    private final ParticipantEventService participantEventService;
    private final WhatsAppsConfig whatsAppsConfig;
    private final MessageWhatsAppsService whatsAppsService;

    public ParticipantService(
            ParticipantRepository participantRepository,
            ModelMapper modelMapper,
            MailService mailService,
            AppConfig appConfig,
            EventService eventService,
            StorageService storageService,
            ParticipantEventService participantEventService,
            WhatsAppsConfig whatsAppsConfig,
            @Lazy MessageWhatsAppsService whatsAppsService) {
        this.participantRepository = participantRepository;
        this.modelMapper = modelMapper;
        this.mailService = mailService;
        this.appConfig = appConfig;
        this.eventService = eventService;
        this.storageService = storageService;
        this.participantEventService = participantEventService;
        this.whatsAppsConfig = whatsAppsConfig;
        this.whatsAppsService = whatsAppsService;
    }

    public static String generateParticipantId(String prefix) {
        int length = 6;
        boolean useLetters = true;
        boolean useNumbers = true;
        return String.format("%s%s", prefix, RandomStringUtils.random(length, useLetters, useNumbers));
    }

    public static ByteArrayResource generateQRCodeImage(String participantId) throws Exception {
        QRCodeWriter barcodeWriter = new QRCodeWriter();
        Map<EncodeHintType, Object> hintMap = new HashMap<>();
        hintMap.put(EncodeHintType.CHARACTER_SET, "UTF-8");
        BitMatrix bitMatrix = barcodeWriter.encode(participantId, BarcodeFormat.QR_CODE, 200, 200, hintMap);
        BufferedImage bufferedImage = MatrixToImageWriter.toBufferedImage(bitMatrix);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(bufferedImage, "png", baos);
        byte[] imageBytes = baos.toByteArray();
        return new ByteArrayResource(imageBytes);
    }

    @SneakyThrows
    private void validation(ParticipantRequestDto participantRequestDto, Participant participant) {
        if (Objects.nonNull(participant)) {
            if (Objects.isNull(participant.getNpa())) {
                participantRequestDto.setNpa(null);
            }
            if (StringUtils.isEmpty(participant.getNpa()) && !StringUtils.isEmpty(participantRequestDto.getNik())) {
                Optional<Participant> participantNik = participantRepository.findByNik(participantRequestDto.getNik());
                if (participantNik.isPresent()
                        && !participantNik.get().getParticipantId().equals(participant.getParticipantId())) {
                    throw new BusinessException(
                            HttpStatus.BAD_REQUEST,
                            "NIK_ALREADY_EXIST",
                            "NIK tersebut sudah terdaftar. Silahkan gunakan NIK lain");
                }
            }

            if (!StringUtils.isEmpty(participant.getNpa())
                    && participantRepository
                            .findByNpa(participantRequestDto.getNpa())
                            .filter(participant1 ->
                                    !participant.getParticipantId().equals(participant1.getParticipantId()))
                            .isPresent()) {
                throw new BusinessException(
                        HttpStatus.BAD_REQUEST,
                        "NPA_ALREADY_EXIST",
                        "NPA tersebut sudah terdaftar. Silahkan gunakan NPA lain");
            }

        } else {
            if (StringUtils.isEmpty(participantRequestDto.getNik())) participantRequestDto.setNik(null);
            if (StringUtils.isEmpty(participantRequestDto.getNpa())) participantRequestDto.setNpa(null);
            if (StringUtils.isEmpty(participantRequestDto.getNpa())
                    && StringUtils.isEmpty(participantRequestDto.getNik())) {
                throw new BusinessException(HttpStatus.BAD_REQUEST, "NPA_OR_NIK_NOT_EMPTY", "NPA_OR_NIK_NOT_EMPTY");
            }

            if ((!StringUtils.isEmpty(participantRequestDto.getNpa())
                    && participantRepository
                            .findByNpa(participantRequestDto.getNpa())
                            .isPresent()))
                throw new BusinessException(
                        HttpStatus.BAD_REQUEST,
                        "DATA_NPA_ALREADY_EXIST",
                        "NPA tersebut sudah terdaftar. Silahkan gunakan NPA lain");

            if (!StringUtils.isEmpty(participantRequestDto.getNik())
                    && participantRepository
                            .findByNik(participantRequestDto.getNik())
                            .isPresent())
                throw new BusinessException(
                        HttpStatus.BAD_REQUEST,
                        "DATA_NIK_ALREADY_EXIST",
                        "NIK tersebut sudah terdaftar. Silahkan gunakan NIK lain");

            if (Objects.nonNull(participantRequestDto.getEventIds())
                    && !participantRequestDto.getEventIds().isEmpty()) {
                List<Long> longs = Arrays.stream(
                                appConfig.getEventIdNonAnggotas().split(","))
                        .map(Long::parseLong)
                        .toList();
                if (StringUtils.isEmpty(participantRequestDto.getNpa()))
                    for (Long eventId : participantRequestDto.getEventIds()) {
                        if (!longs.contains(eventId)) {
                            throw new BusinessException(
                                    HttpStatus.BAD_REQUEST, "EVENT_FOR_ANGGOTA_ONLY", "EVENT_FOR_ANGGOTA_ONLY");
                        }
                    }

            } else {
                participantRequestDto.setEventIds(Collections.singletonList(appConfig.getNoEventId()));
            }
        }
    }

    private Participant convertToEntity(ParticipantRequestDto participantRequestDto) {
        return modelMapper.map(participantRequestDto, Participant.class);
    }

    private ParticipantResponseDto convertToDto(Participant result) {
        ParticipantResponseDto responseDto = modelMapper.map(result, ParticipantResponseDto.class);
        if (Objects.nonNull(result.getEvents()))
            responseDto.setEventDetails(result.getEvents().stream()
                    .map(participantEvent ->
                            modelMapper.map(participantEvent.getEvent(), ParticipantResponseDto.EventDetail.class))
                    .toList());
        return responseDto;
    }

    public BaseResponse<ParticipantResponseDto> create(
            ParticipantRequestDto participantRequestDto, HttpServletRequest request, boolean isPublic) {
        final long start = System.nanoTime();
        AnggotaUser angotaUser = prepareDataUser(request, isPublic);
        validation(participantRequestDto, null);
        Participant participant = convertToEntity(participantRequestDto);
        participant.setParticipantId(generateParticipantId("TITN2024_"));
        List<ParticipantEvent> participantEvents = new ArrayList<>();
        if (Objects.nonNull(participantRequestDto.getEventIds())) {
            if (!participantRequestDto.getEventIds().isEmpty()) {
                List<Event> events = eventService.findByEventIds(participantRequestDto.getEventIds()).stream()
                        .distinct()
                        .toList();
                if (events.isEmpty()) {
                    throw new BusinessException(HttpStatus.BAD_REQUEST, "EVENT_NOT_FOUND", "EVENT_NOT_FOUND");
                }
                if (events.size() != participantRequestDto.getEventIds().size()) {
                    throw new BusinessException(
                            HttpStatus.BAD_REQUEST, "CONTAINS_EVENT_NOT_FOUND", "CONTAINS_EVENT_NOT_FOUND");
                }

                if (events.stream()
                                .filter(event -> event.getType().equals(EventType.OLAHRAGA))
                                .toList()
                                .size()
                        > 1) {
                    throw new BusinessException(
                            HttpStatus.BAD_REQUEST,
                            EventType.OLAHRAGA.toString().concat("_CAN_ONLY_CHOOSE_ONE_OPTION"),
                            EventType.OLAHRAGA.toString().concat("_CAN_ONLY_CHOOSE_ONE_OPTION"));
                }
                if (events.stream()
                                .filter(event -> event.getType().equals(EventType.ETC))
                                .toList()
                                .size()
                        > 1) {
                    throw new BusinessException(
                            HttpStatus.BAD_REQUEST,
                            EventType.ETC.toString().concat("_CAN_ONLY_CHOOSE_ONE_OPTION"),
                            EventType.ETC.toString().concat("_CAN_ONLY_CHOOSE_ONE_OPTION"));
                }
                events.forEach(event -> {
                    ParticipantEvent participantEvent = ParticipantEvent.builder()
                            .id(ParticipantEventId.builder()
                                    .eventId(event.getId())
                                    .participantId(participant.getParticipantId())
                                    .build())
                            .participant(participant)
                            .event(event)
                            .build();
                    participantEvents.add(participantEvent);
                });
            }
        }
        if (Objects.isNull(participant.getPhoto())) participant.setPhoto("default.png");
        participant.setCreatedAt(LocalDateTime.now());
        participant.setStatus(Status.SUBMITTED);
        participant.setCreatedBy(angotaUser.getNama());
        participant.setNpa(Optional.ofNullable(participantRequestDto.getNpa())
                .filter(s -> !StringUtils.isEmpty(s))
                .orElse(null));
        Participant result = participantRepository.save(participant);
        participantEventService.saveAll(participantEvents);
        sendNotificationEmail(participant, CALON_PESERTA_TEMPLATE_HTML, participantEvents, true);
        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(
                HttpStatus.OK, ((end - start) / 1000000), GlobalMessage.SUCCESS.message, convertToDto(result));
    }

    private List<String> sendNotificationEmail(
            Participant participant, String template, List<ParticipantEvent> participantEvents, boolean isWhatApps) {
        List<String> result = new ArrayList<>();
        Map<String, Object> content = new HashMap<>();
        String eventType = Objects.nonNull(participant.getEvents())
                ? participant.getEvents().stream()
                        .map(ParticipantEvent::getEvent)
                        .map(event -> event.getType().toString())
                        .distinct()
                        .collect(Collectors.joining(", "))
                : participantEvents.stream()
                        .map(ParticipantEvent::getEvent)
                        .map(event -> event.getType().toString())
                        .distinct()
                        .collect(Collectors.joining(", "));
        String eventDesc = Objects.nonNull(participant.getEvents())
                ? participant.getEvents().stream()
                        .map(ParticipantEvent::getEvent)
                        .map(Event::getName)
                        .collect(Collectors.joining(", "))
                : participantEvents.stream()
                        .map(ParticipantEvent::getEvent)
                        .map(Event::getName)
                        .collect(Collectors.joining(", "));
        String additionalInfo = " dari PC "
                .concat(participant.getPcName())
                .concat(" - PD ")
                .concat(participant.getPdName())
                .concat(", PW ")
                .concat(participant.getPwName())
                .concat(" ");
        content.put("participantId", participant.getParticipantId());
        content.put("eventType", eventType);
        content.put("eventDesc", eventDesc);
        content.put("participantName", participant.getFullName().concat(additionalInfo));
        if (Objects.nonNull(participantEvents) && !participantEvents.isEmpty())
            participant.setEvents(new HashSet<>(participantEvents));
        String emailFrom = appConfig
                .getEmailFromName()
                .concat("<")
                .concat(appConfig.getEmailFromAddress())
                .concat(">");
        String notificationStatus = "SUCCESS";
        String notificationStatusDesc = "Email Berhasil di kirim";
        boolean isPeserta = PESERTA_TEMPLATE_HTML.equals(template);
        try {

            if (isPeserta) {
                ByteArrayResource streamQRSource = generateQRCodeImage(participant.getParticipantId());
                ByteArrayResource nameTagPdf = generateNameTagPdf(participant, streamQRSource);
                ByteArrayResource old_nameTag = generateNameTag(participant, streamQRSource, "name_tag_old.jrxml");
                String baseFileName = "ID_CARD_"
                        .concat(participant.getParticipantId())
                        .concat("_")
                        .concat(participant.getFullName());
                mailService.sendMailWithQRCodeAndNameTag(
                        content,
                        "Pendaftaran Peserta TITN",
                        participant.getEmail(),
                        template,
                        emailFrom,
                        nameTagPdf,
                        old_nameTag,
                        baseFileName);
                log.info(
                        "[SUCCESS_SEND_EMAIL_REGISTRATION] with participant_id:{} template {}",
                        participant.getParticipantId(),
                        template);
                String prefixFileNameTag =
                        BASE_PREFIX_IMAGE_NAME_TAG.concat("1/").concat(baseFileName.concat(".png"));
                String prefixFileNameTagPdf =
                        BASE_PREFIX_IMAGE_NAME_TAG.concat("pdf/").concat(baseFileName.concat(".pdf"));
                storageService.uploadFile(old_nameTag, prefixFileNameTag);
                storageService.uploadFile(nameTagPdf, prefixFileNameTagPdf);
            } else {
                mailService.sendMail(
                        content, "Pendaftaran Calon Peserta TITN", participant.getEmail(), template, emailFrom);
                log.info(
                        "[SUCCESS_SEND_EMAIL_REGISTRATION_CALON] with participant_id:{} template {}",
                        participant.getParticipantId(),
                        template);
            }
        } catch (Exception e) {
            log.error("[ERROR_SEND_EMAIL_REGISTRATION]", e);
            notificationStatus = "FAILED";
            notificationStatusDesc = StringUtils.substring(e.getMessage(), 100);
            result.add(participant.getParticipantId());
        } finally {
            if (isPeserta) {
                this.updateEmailStatus(participant.getParticipantId(), notificationStatus, notificationStatusDesc);
            }
        }
        if (isWhatApps) {
            if (Objects.nonNull(ContactNumberUtil.getValidContactNumber(participant.getPhone()))) {
                if (isPeserta) sendNotificationWa(participant, eventType, eventDesc);
                else sendNotificationWaRegister(participant, eventType, eventDesc);
            } else {
                this.updateEmailStatus(participant.getParticipantId(), "FAILED", "Nomor telpn tidak valid");
            }
        }
        return result;
    }

    private void sendNotificationWa(Participant participant, String eventType, String eventDesc) {
        MessageWhatsAppsRequest request = prepareWaRequest(participant, eventType, eventDesc);
        List<Map<String, Object>> resultList =
                whatsAppsService.executeSingleSendingMessage(Collections.singletonList(request));
        log.info("Sending message whatsApps {}", resultList);
    }

    public void sendNotificationRecap(ParticipantRecap participantRecap) {
        MessageWhatsAppsRequest request = prepareWaRequestRecap(participantRecap);
        List<Map<String, Object>> resultList =
                whatsAppsService.executeSingleSendingMessage(Collections.singletonList(request));
        log.info("[SEND WA RECAP]Sending message whatsApps {}", resultList);
    }

    private MessageWhatsAppsRequest prepareWaRequestRecap(ParticipantRecap participantRecap) {
        String additionalInfo = " dari PC "
                .concat(participantRecap.getPcName())
                .concat(", PD ")
                .concat(participantRecap.getPdName())
                .concat(", PW ")
                .concat(participantRecap.getPwName())
                .concat(" ");

        return MessageWhatsAppsRequest.builder()
                .message(Optional.ofNullable(whatsAppsConfig.getTemplateWaApprove())
                        .orElse(MessageFormat.format(
                                ConstanUtils.TEMPLATE_RECAP,
                                participantRecap
                                        .getFullName()
                                        .concat(additionalInfo)
                                        .trim())))
                .token(whatsAppsConfig.getToken())
                .type("image")
                .delay(whatsAppsConfig.getDelay())
                .target(ContactNumberUtil.getValidContactNumber(participantRecap.getPhone()))
                .url(whatsAppsConfig.getImageUrl())
                .build();
    }

    private void sendNotificationWaRegister(Participant participant, String eventType, String eventDesc) {
        MessageWhatsAppsRequest request = prepareWaRequestRegister(participant, eventType, eventDesc);
        List<Map<String, Object>> resultList =
                whatsAppsService.executeSingleSendingMessage(Collections.singletonList(request));
        log.info("Sending message whatsApps {}", resultList);
    }

    private MessageWhatsAppsRequest prepareWaRequestRegister(
            Participant participant, String eventType, String eventDesc) {
        String additionalInfo = " dari PC "
                .concat(participant.getPcName())
                .concat(" - PD ")
                .concat(participant.getPdName())
                .concat(", PW ")
                .concat(participant.getPwName())
                .concat(" ");
        return MessageWhatsAppsRequest.builder()
                .message(Optional.ofNullable(whatsAppsConfig.getTemplateWaApprove())
                        .orElse(MessageFormat.format(
                                ConstanUtils.TEMPLATE_REGISTER_PARTICIPANT,
                                participant.getFullName().concat(additionalInfo).trim(),
                                participant.getParticipantId().trim(),
                                eventType.concat(" - ").concat(eventDesc).trim())))
                .participantId(participant.getParticipantId())
                .token(whatsAppsConfig.getToken())
                .type("image")
                .delay(whatsAppsConfig.getDelay())
                .target(ContactNumberUtil.getValidContactNumber(participant.getPhone()))
                .url(whatsAppsConfig.getImageUrl())
                .build();
    }

    public List<Map<String, Object>> executeNotificationEmail(List<Participant> request, boolean isWhatApps) {
        List<Map<String, Object>> errorList = new ArrayList<>();
        List<CompletableFuture<Map<String, Object>>> futures = new ArrayList<>();
        request.forEach(participant -> {
            CompletableFuture<Map<String, Object>> batchFutures = sendBatchSingle(participant, isWhatApps);
            futures.add(batchFutures);
        });
        CompletableFuture<Void> batchFuture = CompletableFuture.allOf(futures.toArray(new CompletableFuture[0]));
        try {
            batchFuture.get();
            futures.forEach(future -> {
                try {
                    if (!future.get().isEmpty()) errorList.add(future.get());
                } catch (InterruptedException | ExecutionException e) {
                    log.info("[SEND EMAIL NOTIFICATION ERROR ASYNC] {}", e.getMessage());
                }
            });
        } catch (InterruptedException | ExecutionException e) {
            log.info("[SEND EMAIL NOTIFICATION ASYNC-2] {}", e.getMessage());
        }
        return errorList;
    }

    public CompletableFuture<Map<String, Object>> sendBatchSingle(Participant participant, boolean isWhatApps) {
        ExecutorService pool = Executors.newFixedThreadPool(10);
        return CompletableFuture.supplyAsync(
                () -> {
                    Map<String, Object> error = new HashMap<>();
                    List<String> result = this.sendNotificationEmail(
                            participant, PESERTA_TEMPLATE_HTML, new ArrayList<>(), isWhatApps);
                    if (!result.isEmpty()) {
                        error.put("message", "Email send failed");
                        error.put("emailList", result);
                    }
                    log.info("[send email notification] {}", result);
                    return error;
                },
                pool);
    }

    private ByteArrayResource generateNameTag(
            Participant participant, ByteArrayResource streamQRSource, String templateName) {
        try {

            Map<String, Object> data = new HashMap<>();
            if ("name_tag_old.jrxml".equals(templateName)) {
                data.put("IMAGE_NAME_TAG", new ClassPathResource("/static/images/id_card_no_qr.png").getInputStream());
                data.put("IMAGE_QR", streamQRSource.getInputStream());
                data.put(
                        "NPA", StringUtils.isEmpty(participant.getNpa()) ? participant.getNik() : participant.getNpa());
                data.put("PD", StringUtils.isEmpty(participant.getPdName()) ? "-" : participant.getPdName());
                String events = participant.getEvents().stream()
                        .map(ParticipantEvent::getEvent)
                        .map(Event::getName)
                        .collect(Collectors.joining(", "));
                data.put("EVENTS", StringUtils.isEmpty(events) ? "-" : events);
                String peserta = setPeserta(participant.getEvents());
                data.put("PESERTA", peserta);
                String urlImage =
                        StringUtils.isEmpty(participant.getPhoto()) ? URL_DEFAULT_IMAGE : participant.getPhoto();
                try {
                    data.put("IMAGE_PARTICIPANT", getImageInputStream(urlImage));
                    log.info("[SUCCESS_GET_IMAGE_PARTICIPANT] {}", urlImage);
                } catch (Exception e) {
                    log.info("[ERROR_GET_IMAGE_PARTICIPANT] {} {}", e.getMessage(), urlImage);
                    data.put("IMAGE_PARTICIPANT", getImageInputStream(URL_DEFAULT_IMAGE));
                }
                data.put("FULL_NAME", participant.getFullName());
            } else {
                if (Objects.nonNull(participant)) {
                    data.put("IMAGE_NAME_TAG", new ClassPathResource("/static/images/img_front.png").getInputStream());
                    data.put(
                            "NPA",
                            StringUtils.isEmpty(participant.getNpa()) ? participant.getNik() : participant.getNpa());
                    data.put("PD", StringUtils.isEmpty(participant.getPdName()) ? "-" : participant.getPdName());
                    String events = participant.getEvents().stream()
                            .map(ParticipantEvent::getEvent)
                            .map(Event::getName)
                            .collect(Collectors.joining(", "));
                    data.put("EVENTS", StringUtils.isEmpty(events) ? "-" : events);
                    String peserta = setPeserta(participant.getEvents());
                    data.put("PESERTA", peserta);
                    String urlImage =
                            StringUtils.isEmpty(participant.getPhoto()) ? URL_DEFAULT_IMAGE : participant.getPhoto();
                    try {
                        data.put("IMAGE_PARTICIPANT", getImageInputStream(urlImage));
                        log.info("[SUCCESS_GET_IMAGE_PARTICIPANT] {}", urlImage);
                    } catch (Exception e) {
                        log.info("[ERROR_GET_IMAGE_PARTICIPANT] {} {}", e.getMessage(), urlImage);
                        data.put("IMAGE_PARTICIPANT", getImageInputStream(URL_DEFAULT_IMAGE));
                    }
                    data.put("FULL_NAME", participant.getFullName());
                } else {
                    data.put("IMAGE_NAME_TAG", new ClassPathResource("/static/images/img_back.png").getInputStream());
                    data.put("IMAGE_QR", streamQRSource.getInputStream());
                    templateName = "QR_tag.jrxml";
                }
            }

            return new ByteArrayResource(GenerateNameTagUtils.executeNameTag(data, templateName));
        } catch (Exception e) {
            log.info("[ERROR_GENERATE_NAME_TAG] {}", e.getMessage());
        }
        return null;
    }

    private ByteArrayResource generateNameTagPdf(Participant participant, ByteArrayResource streamQRSource) {
        try {
            List<JasperPrint> jasperPrintList = new ArrayList<>();
            Map<String, Object> front = prepareNameTag(participant, streamQRSource);
            jasperPrintList.add(GenerateNameTagUtils.executeJasperPrint(front, "name_tag.jrxml"));
            Map<String, Object> back = prepareNameTag(null, streamQRSource);
            jasperPrintList.add(GenerateNameTagUtils.executeJasperPrint(back, "QR_tag.jrxml"));
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            JRPdfExporter exporter = new JRPdfExporter();
            exporter.setExporterInput(SimpleExporterInput.getInstance(jasperPrintList));
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(byteArrayOutputStream));
            SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
            exporter.setConfiguration(configuration);
            exporter.exportReport();
            byte[] pdfBytes = byteArrayOutputStream.toByteArray();
            return new ByteArrayResource(pdfBytes);
        } catch (Exception e) {
            log.info("[ERROR_GENERATE_NAME_TAG_PDF] {}", e.getMessage());
        }

        return null;
    }

    private Map<String, Object> prepareNameTag(Participant participant, ByteArrayResource streamQRSource)
            throws IOException {
        Map<String, Object> data = new HashMap<>();
        if (Objects.nonNull(participant)) {
            data.put("IMAGE_NAME_TAG", new ClassPathResource("/static/images/img_front.png").getInputStream());
            data.put("NPA", StringUtils.isEmpty(participant.getNpa()) ? participant.getNik() : participant.getNpa());
            data.put("PD", StringUtils.isEmpty(participant.getPdName()) ? "-" : participant.getPdName());
            String events = participant.getEvents().stream()
                    .map(ParticipantEvent::getEvent)
                    .map(Event::getName)
                    .collect(Collectors.joining(", "));
            data.put("EVENTS", StringUtils.isEmpty(events) ? "-" : events);
            String peserta = setPeserta(participant.getEvents());
            data.put("PESERTA", peserta);
            String urlImage = StringUtils.isEmpty(participant.getPhoto()) ? URL_DEFAULT_IMAGE : participant.getPhoto();
            try {
                data.put("IMAGE_PARTICIPANT", getImageInputStream(urlImage));
                log.info("[SUCCESS_GET_IMAGE_PARTICIPANT] {}", urlImage);
            } catch (Exception e) {
                log.info("[ERROR_GET_IMAGE_PARTICIPANT] {} {}", e.getMessage(), urlImage);
                data.put("IMAGE_PARTICIPANT", getImageInputStream(URL_DEFAULT_IMAGE));
            }
            data.put("FULL_NAME", participant.getFullName());
        } else {
            data.put("IMAGE_NAME_TAG", new ClassPathResource("/static/images/img_back.png").getInputStream());
            data.put("IMAGE_QR", streamQRSource.getInputStream());
        }

        return data;
    }

    public static InputStream getImageInputStream(String imageUrl) throws IOException {
        URL url = new URL(imageUrl);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setConnectTimeout(5000);
        connection.setReadTimeout(5000);
        connection.setRequestMethod("GET");
        connection.connect();
        if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
            throw new IOException("Failed to fetch image, HTTP response code: " + connection.getResponseCode());
        }

        return connection.getInputStream();
    }

    private String setPeserta(Set<ParticipantEvent> events) {
        String peserta = "PESERTA";
        for (ParticipantEvent event : events) {
            if ("PANITIA".equalsIgnoreCase(event.getEvent().getName())) {
                peserta = "PANITIA";
                break;
            }
            if ("OFFICIAL".equalsIgnoreCase(event.getEvent().getName())) {
                peserta = "OFFICIAL";
                break;
            }
        }
        return peserta;
    }

    @Transactional(readOnly = true)
    public BaseResponse<Page<ParticipantResponseDto>> getAllParticipant(FilterParticipantDto filter) {
        final long start = System.nanoTime();
        AnggotaUser angotaUser = prepareDataUser(filter.getRequest(), false);
        if (Objects.nonNull(angotaUser.getPw())) {
            filter.setPw(angotaUser.getPw());
        }
        if (Objects.nonNull(angotaUser.getPc())) {
            filter.setPc(angotaUser.getPc());
        }
        if (Objects.nonNull(angotaUser.getPd())) {
            filter.setPd(angotaUser.getPd());
        }
        Page<Participant> participants = participantRepository.findAll(
                bySearch(
                        filter.getKeyword(),
                        Optional.ofNullable(filter.getStatus())
                                .map(status1 -> status1.code)
                                .orElse(null),
                        filter.getIsAnggota(),
                        filter.getPc(),
                        filter.getPd(),
                        filter.getPw(),
                        filter.getIsRecap(),
                        filter.getRecapId(),
                        filter.getIsSpinner()),
                filter.getPaging());

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(
                HttpStatus.OK,
                ((end - start) / 1000000),
                GlobalMessage.SUCCESS.message,
                participants.map(this::convertToDto));
    }

    public Specification<Participant> bySearch(
            String keyword,
            String status,
            Boolean isAnggota,
            String pc,
            String pd,
            String pw,
            Boolean isRecap,
            String recapId,
            Boolean isSpinner) {
        keyword = Optional.ofNullable(keyword)
                .filter(s -> !StringUtils.isEmpty(s))
                .orElse(null);
        if (Objects.isNull(isAnggota))
            return SpecificationHelper.searchAttributeEqual("status", status)
                    .and(SpecificationHelper.searchAttributeBoolean("isDeleted", false))
                    .and(SpecificationHelper.searchAttributeBoolean("isSpinner", isSpinner))
                    .and(SpecificationHelper.searchAttributeEqual("pcCode", pc))
                    .and(SpecificationHelper.searchAttributeEqual("pwCode", pw))
                    .and(SpecificationHelper.searchAttributeEqual("pdCode", pd))
                    .and(SpecificationHelper.searchNestedEquals("recap", "recapId", recapId))
                    .and(SpecificationHelper.searchAttributeIsNull("recap", isRecap))
                    .and((SpecificationHelper.searchAttributeContains("fullName", keyword))
                            .or(SpecificationHelper.searchAttributeContains("email", keyword))
                            .or(SpecificationHelper.searchAttributeContains("pcName", keyword))
                            .or(SpecificationHelper.searchAttributeContains("pcCode", keyword))
                            .or(SpecificationHelper.searchAttributeContains("pdCode", keyword))
                            .or(SpecificationHelper.searchAttributeContains("pdName", keyword))
                            .or(SpecificationHelper.searchAttributeContains("pwCode", keyword))
                            .or(SpecificationHelper.searchAttributeContains("pwName", keyword))
                            .or(SpecificationHelper.searchAttributeContains("pwCode", keyword))
                            .or(SpecificationHelper.searchAttributeContains("emailNotificationStatus", keyword))
                            .or(SpecificationHelper.searchAttributeContains("waNotificationStatus", keyword))
                            .or(SpecificationHelper.eventLike("name", keyword))
                            .or(SpecificationHelper.eventLike("description", keyword))
                            .or(SpecificationHelper.searchAttributeContains("clothesSize", keyword))
                            .or(SpecificationHelper.searchAttributeContains("npa", keyword)));

        if (isAnggota)
            return SpecificationHelper.searchAttributeEqual("status", status)
                    .and(SpecificationHelper.searchAttributeBoolean("isDeleted", false))
                    .and(SpecificationHelper.searchAttributeNotNull("npa"))
                    .and(SpecificationHelper.searchNotEqual("npa", ""))
                    .and(SpecificationHelper.searchAttributeEqual("pcCode", pc))
                    .and(SpecificationHelper.searchAttributeEqual("pwCode", pw))
                    .and(SpecificationHelper.searchAttributeEqual("pdCode", pd))
                    .and(SpecificationHelper.searchAttributeBoolean("isSpinner", isSpinner))
                    .and(SpecificationHelper.searchNestedEquals("recap", "recapId", recapId))
                    .and(SpecificationHelper.searchAttributeIsNull("recap", isRecap))
                    .and((SpecificationHelper.searchAttributeContains("fullName", keyword))
                            .or(SpecificationHelper.searchAttributeContains("email", keyword))
                            .or(SpecificationHelper.searchAttributeContains("pcName", keyword))
                            .or(SpecificationHelper.searchAttributeContains("pcCode", keyword))
                            .or(SpecificationHelper.searchAttributeContains("pdCode", keyword))
                            .or(SpecificationHelper.searchAttributeContains("pdName", keyword))
                            .or(SpecificationHelper.searchAttributeContains("pwCode", keyword))
                            .or(SpecificationHelper.searchAttributeContains("pwName", keyword))
                            .or(SpecificationHelper.searchAttributeContains("pwCode", keyword))
                            .or(SpecificationHelper.searchAttributeContains("emailNotificationStatus", keyword))
                            .or(SpecificationHelper.searchAttributeContains("waNotificationStatus", keyword))
                            .or(SpecificationHelper.eventLike("name", keyword))
                            .or(SpecificationHelper.eventLike("description", keyword))
                            .or(SpecificationHelper.searchAttributeContains("clothesSize", keyword))
                            .or(SpecificationHelper.searchAttributeContains("npa", keyword)));
        else
            return SpecificationHelper.searchAttributeEqual("status", status)
                    .and(SpecificationHelper.searchAttributeBoolean("isDeleted", false))
                    .and(SpecificationHelper.searchAttributeEqual("pcCode", pc))
                    .and(SpecificationHelper.searchAttributeEqual("pwCode", pw))
                    .and(SpecificationHelper.searchAttributeEqual("pdCode", pd))
                    .and(SpecificationHelper.searchAttributeBoolean("isSpinner", isSpinner))
                    .and(SpecificationHelper.searchNestedEquals("recap", "recapId", recapId))
                    .and(SpecificationHelper.searchAttributeIsNull("recap", isRecap))
                    .and(SpecificationHelper.searchAttributeIsNull("npa")
                            .or(SpecificationHelper.searchAttributeEqual("npa", "")))
                    .and((SpecificationHelper.searchAttributeContains("fullName", keyword))
                            .or(SpecificationHelper.searchAttributeContains("email", keyword))
                            .or(SpecificationHelper.searchAttributeContains("pcName", keyword))
                            .or(SpecificationHelper.searchAttributeContains("pcCode", keyword))
                            .or(SpecificationHelper.searchAttributeContains("pdCode", keyword))
                            .or(SpecificationHelper.searchAttributeContains("pdName", keyword))
                            .or(SpecificationHelper.searchAttributeContains("pwCode", keyword))
                            .or(SpecificationHelper.searchAttributeContains("pwName", keyword))
                            .or(SpecificationHelper.searchAttributeContains("pwCode", keyword))
                            .or(SpecificationHelper.searchAttributeContains("emailNotificationStatus", keyword))
                            .or(SpecificationHelper.searchAttributeContains("waNotificationStatus", keyword))
                            .or(SpecificationHelper.eventLike("name", keyword))
                            .or(SpecificationHelper.eventLike("description", keyword))
                            .or(SpecificationHelper.searchAttributeContains("clothesSize", keyword)));
    }

    public BaseResponse<ParticipantResponseDto> update(
            ParticipantRequestDto participantRequestDto, String participantId, HttpServletRequest request) {
        AnggotaUser angotaUser = prepareDataUser(request, false);
        final long start = System.nanoTime();
        Participant participant = participantRepository
                .findByParticipantIdDeletedFalse(participantId)
                .orElseThrow(() -> new BusinessException(HttpStatus.BAD_REQUEST, "DATA_NOT_FOUND", "DATA_NOT_FOUND"));
        if ("anggota".equalsIgnoreCase(angotaUser.getRole())
                && !participant.getNpa().equalsIgnoreCase(angotaUser.getNpa()))
            throw new BusinessException(HttpStatus.UNAUTHORIZED, "UNAUTHORIZED_ACCESS", "UNAUTHORIZED_ACCESS");

        validation(participantRequestDto, participant);
        Participant participantUpdate = convertToEntityUpdate(participantRequestDto, participant);
        List<ParticipantEvent> participantEvents = new ArrayList<>();
        if (Objects.nonNull(participantRequestDto.getEventIds())) {
            if (!participantRequestDto.getEventIds().isEmpty()) {
                List<Event> events = eventService.findByEventIds(participantRequestDto.getEventIds()).stream()
                        .distinct()
                        .toList();
                if (events.isEmpty()) {
                    throw new BusinessException(HttpStatus.BAD_REQUEST, "EVENT_NOT_FOUND", "EVENT_NOT_FOUND");
                }

                if (events.size() != participantRequestDto.getEventIds().size()) {
                    throw new BusinessException(
                            HttpStatus.BAD_REQUEST, "CONTAINS_EVENT_NOT_FOUND", "CONTAINS_EVENT_NOT_FOUND");
                }

                if (events.stream()
                                .filter(event -> event.getType().equals(EventType.OLAHRAGA))
                                .toList()
                                .size()
                        > 1) {
                    throw new BusinessException(
                            HttpStatus.BAD_REQUEST,
                            EventType.OLAHRAGA.toString().concat("_CAN_ONLY_CHOOSE_ONE_OPTION"),
                            EventType.OLAHRAGA.toString().concat("_CAN_ONLY_CHOOSE_ONE_OPTION"));
                }
                if (events.stream()
                                .filter(event -> event.getType().equals(EventType.ETC))
                                .toList()
                                .size()
                        > 1) {
                    throw new BusinessException(
                            HttpStatus.BAD_REQUEST,
                            EventType.ETC.toString().concat("_CAN_ONLY_CHOOSE_ONE_OPTION"),
                            EventType.ETC.toString().concat("_CAN_ONLY_CHOOSE_ONE_OPTION"));
                }
                events.forEach(event -> {
                    ParticipantEvent participantEvent = ParticipantEvent.builder()
                            .id(ParticipantEventId.builder()
                                    .eventId(event.getId())
                                    .participantId(participant.getParticipantId())
                                    .build())
                            .participant(participant)
                            .event(event)
                            .build();
                    participantEvents.add(participantEvent);
                });
            }
        }
        if (Objects.isNull(participant.getPhoto())) participantUpdate.setPhoto("default.png");
        participantUpdate.setUpdatedAt(LocalDateTime.now());
        participantUpdate.setUpdatedBy(angotaUser.getNama());
        participantRepository.save(participantUpdate);
        if (!participantEvents.isEmpty()) {
            participantEventService.deleteByParticipantId(participantId);
            participantEventService.saveAll(participantEvents);
        }

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(
                HttpStatus.OK, ((end - start) / 1000000), GlobalMessage.SUCCESS.message, null);
    }

    private Participant convertToEntityUpdate(ParticipantRequestDto participantRequestDto, Participant participant) {
        if (StringUtils.isEmpty(participantRequestDto.getNik()) && !StringUtils.isEmpty(participant.getNik())) {
            participantRequestDto.setNik(participant.getNik());
        }
        if (StringUtils.isEmpty(participantRequestDto.getNpa()) && !StringUtils.isEmpty(participant.getNpa())) {
            participantRequestDto.setNpa(participant.getNpa());
        }
        modelMapper.map(participantRequestDto, participant);
        return participant;
    }

    @Transactional
    public BaseResponse<ParticipantResponseDto> delete(String participantId, HttpServletRequest request) {
        final long start = System.nanoTime();
        prepareDataUser(request, false);
        Participant participant = participantRepository
                .findById(participantId)
                .orElseThrow(() -> new BusinessException(HttpStatus.BAD_REQUEST, "DATA_NOT_FOUND", "DATA_NOT_FOUND"));
        String idParticipant = StringUtils.isEmpty(participant.getNpa()) ? participant.getNik() : participant.getNpa();
        participantEventService.deleteByParticipantId(participantId);
        participantRepository.delete(participant);
        log.info("[SUCCESS_DELETED_PARTICIPANT] with NPA/NIK :{} ", idParticipant);
        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(
                HttpStatus.OK, ((end - start) / 1000000), GlobalMessage.SUCCESS.message, null);
    }

    @Transactional(readOnly = true)
    public BaseResponse<ParticipantResponseDto> getDetailByParticipantId(
            String participantId, HttpServletRequest request) {
        final long start = System.nanoTime();
        AnggotaUser angotaUser = prepareDataUser(request, false);
        Participant participant = participantRepository
                .findByParticipantIdDeletedFalse(participantId)
                .orElseThrow(() -> new BusinessException(HttpStatus.BAD_REQUEST, "DATA_NOT_FOUND", "DATA_NOT_FOUND"));
        if (Objects.nonNull(angotaUser.getPw()) && !participant.getPwCode().equals(angotaUser.getPw())) {
            throw new BusinessException(HttpStatus.UNAUTHORIZED, "UNAUTHORIZED_ACCESS", "UNAUTHORIZED_ACCESS");
        }
        if (Objects.nonNull(angotaUser.getPc()) && !participant.getPcCode().equals(angotaUser.getPc())) {
            throw new BusinessException(HttpStatus.UNAUTHORIZED, "UNAUTHORIZED_ACCESS", "UNAUTHORIZED_ACCESS");
        }
        if (Objects.nonNull(angotaUser.getPd()) && !participant.getPdCode().equals(angotaUser.getPd())) {
            throw new BusinessException(HttpStatus.UNAUTHORIZED, "UNAUTHORIZED_ACCESS", "UNAUTHORIZED_ACCESS");
        }
        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(
                HttpStatus.OK, ((end - start) / 1000000), GlobalMessage.SUCCESS.message, convertToDto(participant));
    }

    @Transactional(readOnly = true)
    public List<Participant> findAllByIdList(List<String> participantIdList) {
        return participantRepository.findAllById(participantIdList);
    }

    public void updateRecapByParticipantIds(String npa, List<String> participants) {
        participantRepository.updateRecapByParticipantIds(npa, participants, LocalDateTime.now());
    }

    public void updateStatusParticipant(String recapId, Status status) {
        participantRepository.updateStatusByRecapId(recapId, status.code, LocalDateTime.now());
        if (Status.APPROVED.equals(status)) {
            senEmailAsync(recapId, true);
        }
    }

    @Async
    public void senEmailAsync(String recapId, boolean isWhatApps) {
        ExecutorService pool = Executors.newFixedThreadPool(10);
        CompletableFuture.runAsync(
                () -> {
                    try {
                        Thread.sleep(100);
                        List<Participant> participantList =
                                participantRepository.findContactParticipantByRecapId(recapId);
                        List<Map<String, Object>> resultList =
                                this.executeNotificationEmail(participantList, isWhatApps);
                        log.info("[SEND EMAIL NOTIFICATION ASYNC] with {}", resultList);
                    } catch (InterruptedException e) {
                        log.error("[SEND EMAIL NOTIFICATION ASYNC] error", e);
                    }
                },
                pool);
    }

    public void updateParticipantRecapId(String recapId) {
        participantRepository.updateByRecapId(recapId, LocalDateTime.now());
    }

    public BaseResponse<CheckinResponse> checkIn(String participantId, Status status) {
        final long start = System.nanoTime();
        Participant participant = participantRepository
                .findByParticipantIdDeletedFalse(participantId)
                .orElseThrow(() -> new BusinessException(HttpStatus.BAD_REQUEST, "DATA_NOT_FOUND", "DATA_NOT_FOUND"));
        if (Status.CHECKED_IN.equals(participant.getStatus()))
            throw new BusinessException(
                    HttpStatus.BAD_REQUEST, "AL_READY_CHECK_IN", "Peserta sudah melakukan Check-In");
        if (Status.SUBMITTED.equals(participant.getStatus()) || Status.REJECTED.equals(participant.getStatus()))
            throw new BusinessException(
                    HttpStatus.BAD_REQUEST, "CHECK_IN_FOR_PESERTA", "Hanya Peserta yang bisa Check-In");
        participant.setUpdatedAt(LocalDateTime.now());
        participant.setUpdatedBy("System");
        participant.setStatus(status);
        participantRepository.save(participant);
        CheckinResponse result = CheckinResponse.builder()
                .participantId(participantId)
                .checkedAt(participant.getUpdatedAt())
                .fullName(participant.getFullName())
                .status(status)
                .build();
        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(
                HttpStatus.OK, ((end - start) / 1000000), GlobalMessage.SUCCESS.message, result);
    }

    public List<List<String>> getAllParticipantApproved(int maxContactNumber) {
        Pageable pageable = PageRequest.of(0, maxContactNumber);
        Slice<Participant> slice;
        List<String> contactList = new ArrayList<>();
        List<CompletableFuture<List<String>>> futures = new ArrayList<>();
        do {
            log.info("[PAGE SLICE] {}", pageable);
            slice = participantRepository.findAllByParticipantApprove(pageable);
            CompletableFuture<List<String>> batchFutures = executeAsync(slice);
            pageable = slice.nextPageable();
            futures.add(batchFutures);
        } while (slice.hasNext());
        CompletableFuture<Void> batchFuture = CompletableFuture.allOf(futures.toArray(new CompletableFuture[0]));
        try {
            batchFuture.get();
            futures.forEach(future -> {
                try {
                    if (!future.get().isEmpty()) contactList.addAll(future.get());
                } catch (InterruptedException | ExecutionException e) {
                    log.info("[GET CONTACT ERROR ASYNC] {}", e.getMessage());
                }
            });
        } catch (InterruptedException | ExecutionException e) {
            log.info("[GET CONTACT ERROR ASYNC-2] {}", e.getMessage());
        }
        if (contactList.isEmpty())
            throw new BusinessException(HttpStatus.BAD_REQUEST, "PARTICIPANT_NOT_FOUND", "PARTICIPANT_NOT_FOUND");
        return ListUtils.partition(contactList.stream().distinct().toList(), maxContactNumber);
    }

    public CompletableFuture<List<String>> executeAsync(Slice<Participant> slice) {
        ExecutorService pool = Executors.newFixedThreadPool(10);
        return CompletableFuture.supplyAsync(
                () -> {
                    List<String> contactList = new ArrayList<>();
                    slice.getContent().forEach(participant -> {
                        String contact = ContactNumberUtil.getValidContactNumber(participant.getPhone());
                        if (Objects.nonNull(contact)) {
                            contactList.add(contact);
                        }
                    });
                    log.info(
                            "[GET DATA CONTACT] size batch  {}",
                            slice.getContent().size());
                    return contactList;
                },
                pool);
    }

    public List<Participant> getContactParticipantByRecapId(String recapId) {
        return participantRepository.findContactParticipantByRecapId(recapId);
    }

    public List<Participant> getContactParticipantByRecapIdPeserta(String recapId) {
        return participantRepository.findContactParticipantByRecapIdPeserta(recapId);
    }

    @Transactional(readOnly = true)
    public BaseResponse<ParticipantResponseDto> participantByNpa(String npa) {
        final long start = System.nanoTime();
        Participant participant = participantRepository
                .findByNpa(npa)
                .orElseThrow(() ->
                        new BusinessException(HttpStatus.BAD_REQUEST, "DATA_NPA_NOT_FOUND", "DATA_NPA_NOT_FOUND"));
        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(
                HttpStatus.OK, ((end - start) / 1000000), GlobalMessage.SUCCESS.message, convertToDto(participant));
    }

    private AnggotaUser prepareDataUser(HttpServletRequest request, boolean isPublic) {
        if (isPublic) return AnggotaUser.builder().nama("system").build();
        AnggotaUser anggotaUser = (AnggotaUser) request.getAttribute("user");
        if (anggotaUser == null) {
            throw new BusinessException(HttpStatus.UNAUTHORIZED, "UNAUTHORIZED");
        }
        switch (anggotaUser.getRole().toLowerCase()) {
            case "anggota", "admin_pc": {
                break;
            }
            case "admin_pw": {
                anggotaUser.setPc(null);
                anggotaUser.setPd(null);
                break;
            }
            case "admin_pd": {
                anggotaUser.setPc(null);
                break;
            }
            default: {
                anggotaUser.setPw(null);
                anggotaUser.setPc(null);
                anggotaUser.setPd(null);
                break;
            }
        }
        return anggotaUser;
    }

    private MessageWhatsAppsRequest prepareWaRequest(Participant participant, String eventType, String eventDesc) {
        String additionalInfo = " dari PC "
                .concat(participant.getPcName())
                .concat(" - PD ")
                .concat(participant.getPdName())
                .concat(", PW ")
                .concat(participant.getPwName())
                .concat(" ");
        return MessageWhatsAppsRequest.builder()
                .message(Optional.ofNullable(whatsAppsConfig.getTemplateWaApprove())
                        .orElse(MessageFormat.format(
                                ConstanUtils.TEMPLATE_APPROVE_PARTICIPANT,
                                participant.getFullName().concat(additionalInfo).trim(),
                                participant.getParticipantId().trim(),
                                eventType.concat(" - ").concat(eventDesc).trim())))
                .participantId(participant.getParticipantId())
                .token(whatsAppsConfig.getToken())
                .type("image")
                .delay(whatsAppsConfig.getDelay())
                .target(ContactNumberUtil.getValidContactNumber(participant.getPhone()))
                .url(whatsAppsConfig.getImageUrl())
                .build();
    }

    public BaseResponse<ParticipantResponseDto> sendNotificationByParticipantId(
            String participantId, String type, boolean isForce) {
        final long start = System.nanoTime();
        Participant participant = participantRepository
                .findByParticipantIdDeletedFalse(participantId)
                .orElseThrow(() -> new BusinessException(HttpStatus.BAD_REQUEST, "DATA_NOT_FOUND", "DATA_NOT_FOUND"));
        if (!Status.APPROVED.equals(participant.getStatus())) {
            throw new BusinessException(
                    HttpStatus.BAD_REQUEST, "DATA_NOT_FOUND", "Notification hanya untuk statusnya sudah approve");
        }
        if ("SUCCESS".equalsIgnoreCase(participant.getEmailNotificationStatus())
                && "EMAIL".equalsIgnoreCase(type)
                && !isForce) {
            throw new BusinessException(
                    HttpStatus.BAD_REQUEST, "DATA_NOT_FOUND", "Email Notification sudah sukses terkirim sebelumnya");
        }
        if ("SUCCESS".equalsIgnoreCase(participant.getWaNotificationStatus())
                && "WA".equalsIgnoreCase(type)
                && !isForce) {
            throw new BusinessException(
                    HttpStatus.BAD_REQUEST, "DATA_NOT_FOUND", "Wa Notification sudah sukses terkirim sebelumnya");
        }
        switch (type) {
            case "EMAIL": {
                executeNotificationEmail(Collections.singletonList(participant), false);
                break;
            }
            case "WA": {
                if (Objects.isNull(ContactNumberUtil.getValidContactNumber(participant.getPhone())))
                    throw new BusinessException(
                            HttpStatus.BAD_REQUEST, "PHONE_NUMBER_NOT_VALID", "Nomor kontak tidak valid");
                this.sendNotificationWa(participant);
                break;
            }
            case "ALL": {
                executeNotificationEmail(Collections.singletonList(participant), false);
                this.sendNotificationWa(participant);
                break;
            }
            default:
                throw new BusinessException(
                        HttpStatus.BAD_REQUEST, "TYPE_NOT_FOUND", "type hanya untuk EMAIL,WA atau ALL");
        }
        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(
                HttpStatus.OK, ((end - start) / 1000000), GlobalMessage.SUCCESS.message, null);
    }

    private void sendNotificationWa(Participant participant) {
        String eventType = participant.getEvents().stream()
                .map(ParticipantEvent::getEvent)
                .map(event -> event.getType().toString())
                .distinct()
                .collect(Collectors.joining(", "));
        String eventDesc = participant.getEvents().stream()
                .map(ParticipantEvent::getEvent)
                .map(Event::getName)
                .collect(Collectors.joining(", "));
        sendNotificationWa(participant, eventType, eventDesc);
    }

    @Async
    public void updateWaStatus(String participantId, String status, String statusDesc) {
        CompletableFuture.runAsync(() -> participantRepository.updateStatusWaByParticipantId(
                participantId, status, statusDesc, LocalDateTime.now()));
    }

    @Async
    public void updateEmailStatus(String participantId, String status, String statusDesc) {
        CompletableFuture.runAsync(() -> participantRepository.updateStatusEmailByParticipantId(
                participantId, status, statusDesc, LocalDateTime.now()));
    }

    public BaseResponse<CheckinResponse> spinner(String participantId) {
        final long start = System.nanoTime();
        Participant participant = participantRepository
                .findByParticipantIdDeletedFalse(participantId)
                .orElseThrow(() -> new BusinessException(HttpStatus.BAD_REQUEST, "DATA_NOT_FOUND", "DATA_NOT_FOUND"));
        if (!Status.CHECKED_IN.equals(participant.getStatus()))
            throw new BusinessException(
                    HttpStatus.BAD_REQUEST,
                    "STATUS_PARTICIPANT_NOT_APPROVED",
                    "Hanya untuk Peserta yang sudah melakukan check in");
        if (participant.isSpinner())
            throw new BusinessException(HttpStatus.BAD_REQUEST, "SPINNER_ALREADY_USED", "Hanya bisa satu kali Spinner");
        participant.setUpdatedAt(LocalDateTime.now());
        participant.setUpdatedBy("System");
        participant.setSpinner(true);
        participantRepository.save(participant);
        CheckinResponse result = CheckinResponse.builder()
                .participantId(participantId)
                .spinnerAt(participant.getUpdatedAt())
                .fullName(participant.getFullName())
                .build();
        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(
                HttpStatus.OK, ((end - start) / 1000000), GlobalMessage.SUCCESS.message, result);
    }

    @Transactional
    public Participant findParticipantByIdOrNpa(String participantId) {
        return participantRepository
                .findByNpaOrId(participantId)
                .orElseThrow(() -> new BusinessException(HttpStatus.BAD_REQUEST, "DATA_NOT_FOUND", "DATA_NOT_FOUND"));
    }

    @Transactional
    public Participant findParticipantByNpa(String participantId) {
        return participantRepository.findByNpa(participantId).orElse(null);
    }
}
