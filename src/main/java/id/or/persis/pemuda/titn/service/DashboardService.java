package id.or.persis.pemuda.titn.service;

import static java.util.stream.Collectors.groupingBy;

import id.or.persis.pemuda.titn.payload.AnggotaUser;
import id.or.persis.pemuda.titn.payload.DashboardSummary;
import id.or.persis.pemuda.titn.payload.DashboardSummaryResponse;
import id.or.persis.pemuda.titn.payload.EventResponseDto;
import id.or.persis.pemuda.titn.repository.DashboardSummaryRepository;
import id.or.persis.pemuda.titn.utils.BaseResponse;
import id.or.persis.pemuda.titn.utils.BusinessException;
import id.or.persis.pemuda.titn.utils.ResponseBuilder;
import jakarta.servlet.http.HttpServletRequest;
import java.util.*;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

/**
 * Project: titn-service
 * <p>
 * User: hasan.sanusi
 * Email: hasan.sanusi@dansmultipro.com
 * Telegram: @hasansanusi
 * Date: 17/07/24
 * Time: 20.07
 * <p>
 * Created with IntelliJ IDEA
 */
@Service
@Slf4j
@AllArgsConstructor
public class DashboardService {
    private final DashboardSummaryRepository dashboardSummaryRepository;
    private final EventService eventService;

    public BaseResponse<DashboardSummaryResponse> getSummary(
            String pd, String pc, String pw, HttpServletRequest request) {
        final long start = System.nanoTime();
        AnggotaUser angotaUser = prepareDataUser(request);
        if (Objects.nonNull(angotaUser.getPw())) {
            pw = angotaUser.getPw();
        }
        if (Objects.nonNull(angotaUser.getPc())) {
            pc = angotaUser.getPc();
        }
        if (Objects.nonNull(angotaUser.getPd())) {
            pd = angotaUser.getPd();
        }
        DashboardSummaryResponse summaryResponse = executeSummary(pd, pc, pw);
        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", summaryResponse);
    }

    private DashboardSummaryResponse executeSummary(String pd, String pc, String pw) {
        DashboardSummaryResponse summaryResponse = dashboardSummaryRepository.getParticipantSummary(pd, pc, pw);
        summaryResponse.setEventSummary(getEventSummary(dashboardSummaryRepository.getEventSummary(pd, pc, pw)));
        return summaryResponse;
    }

    private DashboardSummaryResponse executeSummaryPanitia(String pd, String pc, String pw) {
        DashboardSummaryResponse summaryResponse =
                dashboardSummaryRepository.getParticipantWoPanitiaSummary(pd, pc, pw);
        DashboardSummaryResponse panitiaSummary = dashboardSummaryRepository.getParticipantPanitiaSummary(pd, pc, pw);
        summaryResponse.setTotalJumlahPanitia(panitiaSummary.getTotalJumlahPanitia());
        summaryResponse.setTotalJumlahCalonPanitia(panitiaSummary.getTotalJumlahCalonPanitia());
        summaryResponse.setTotalJumlahPanitiaChecked(panitiaSummary.getTotalJumlahPanitiaChecked());
        summaryResponse.setEventSummary(getEventSummary(dashboardSummaryRepository.getEventSummary(pd, pc, pw)));
        summaryResponse.setClothesSizeSummary(dashboardSummaryRepository.getClothesSizeSummary(pd, pc, pw));
        DashboardSummaryResponse pengunjungSummary =
                dashboardSummaryRepository.getParticipantPengunjungSummary(pd, pc, pw);
        summaryResponse.setTotalJumlahPengunjung(pengunjungSummary.getTotalJumlahPengunjung());
        summaryResponse.setTotalJumlahCalonPengunjung(pengunjungSummary.getTotalJumlahCalonPengunjung());
        summaryResponse.setTotalJumlahPengujungChecked(pengunjungSummary.getTotalJumlahPengujungChecked());
        return summaryResponse;
    }

    public List<DashboardSummaryResponse.Type> getEventSummary(List<DashboardSummary> dashboardSummaryList) {
        List<DashboardSummaryResponse.Type> eventSummary = new ArrayList<>();
        Map<String, Long> mapValue = new HashMap<>();
        dashboardSummaryList.forEach(dashboardSummary -> mapValue.put(
                dashboardSummary.getType().concat(dashboardSummary.getName()), dashboardSummary.getValue()));
        eventService.getAllEvent().stream()
                .collect(groupingBy(EventResponseDto::getType))
                .forEach((eventType, eventResponseDtos) -> {
                    List<DashboardSummaryResponse.Name> values = eventResponseDtos.stream()
                            .map(eventResponseDto -> DashboardSummaryResponse.Name.builder()
                                    .name(eventResponseDto.getName())
                                    .value(Optional.ofNullable(mapValue.get(eventResponseDto
                                                    .getType()
                                                    .name()
                                                    .concat(eventResponseDto.getName())))
                                            .orElse(0L))
                                    .build())
                            .toList();

                    eventSummary.add(DashboardSummaryResponse.Type.builder()
                            .name(eventType.name())
                            .values(values)
                            .build());
                });
        return eventSummary;
    }

    private AnggotaUser prepareDataUser(HttpServletRequest request) {
        AnggotaUser anggotaUser = (AnggotaUser) request.getAttribute("user");
        if (anggotaUser == null) {
            throw new BusinessException(HttpStatus.UNAUTHORIZED, "UNAUTHORIZED");
        }
        switch (anggotaUser.getRole().toLowerCase()) {
            case "anggota":
                throw new BusinessException(HttpStatus.UNAUTHORIZED, "UNAUTHORIZED");
            case "admin_pw": {
                anggotaUser.setPc(null);
                anggotaUser.setPd(null);
                break;
            }
            case "admin_pd": {
                anggotaUser.setPc(null);
                break;
            }
            case "admin_pc": {
                break;
            }
            default: {
                anggotaUser.setPw(null);
                anggotaUser.setPc(null);
                anggotaUser.setPd(null);
                break;
            }
        }
        anggotaUser.setNama(Optional.ofNullable(request.getParameter("nama")).orElse(anggotaUser.getNpa()));
        return anggotaUser;
    }

    public BaseResponse<DashboardSummaryResponse> getSummaryPanitia(
            String pd, String pc, String pw, HttpServletRequest request) {
        final long start = System.nanoTime();
        AnggotaUser angotaUser = prepareDataUser(request);
        if (Objects.nonNull(angotaUser.getPw())) {
            pw = angotaUser.getPw();
        }
        if (Objects.nonNull(angotaUser.getPc())) {
            pc = angotaUser.getPc();
        }
        if (Objects.nonNull(angotaUser.getPd())) {
            pd = angotaUser.getPd();
        }
        DashboardSummaryResponse summaryResponse = executeSummaryPanitia(pd, pc, pw);
        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", summaryResponse);
    }
}
