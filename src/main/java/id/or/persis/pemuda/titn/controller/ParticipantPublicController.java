package id.or.persis.pemuda.titn.controller;

import static org.springframework.http.ResponseEntity.ok;

import id.or.persis.pemuda.titn.entity.Status;
import id.or.persis.pemuda.titn.payload.CheckinResponse;
import id.or.persis.pemuda.titn.payload.ParticipantRequestDto;
import id.or.persis.pemuda.titn.payload.ParticipantResponseDto;
import id.or.persis.pemuda.titn.service.ParticipantService;
import id.or.persis.pemuda.titn.utils.BaseResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@Tag(name = "Participant", description = "Endpoint for managing Participant")
@RequestMapping(value = Endpoint.PARTICIPANTS_PUB)
@Slf4j
@AllArgsConstructor
public class ParticipantPublicController {
    private final ParticipantService participantService;

    @PostMapping
    @Operation(
            summary = "Add New Pemuda Persis Participant Data",
            description = "Add New Pemuda Persis Participant Data.",
            tags = {"Participant"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ParticipantResponseDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    public ResponseEntity<BaseResponse<ParticipantResponseDto>> create(
            @RequestBody @Valid ParticipantRequestDto participantRequestDto) {
        BaseResponse<ParticipantResponseDto> baseResponse =
                participantService.create(participantRequestDto, null, true);
        return ok(baseResponse);
    }

    @PatchMapping(value = "/{participantId}")
    @Operation(
            summary = "Checkin Pemuda Persis Participant Data",
            description = "Checkin Pemuda Persis Participant Data.",
            tags = {"Participant"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ParticipantResponseDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    public ResponseEntity<BaseResponse<CheckinResponse>> checkin(@PathVariable String participantId) {
        return ok(participantService.checkIn(participantId, Status.CHECKED_IN));
    }

    @PatchMapping(value = "/spinner/{participantId}")
    @Operation(
            summary = "Checkin Pemuda Persis Participant Data",
            description = "Checkin Pemuda Persis Participant Data.",
            tags = {"Participant"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ParticipantResponseDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    public ResponseEntity<BaseResponse<CheckinResponse>> spinner(@PathVariable String participantId) {
        return ok(participantService.spinner(participantId));
    }
}
