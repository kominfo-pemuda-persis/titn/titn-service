package id.or.persis.pemuda.titn.payload;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Project: titn-service
 * <p>
 * User: hasan.sanusi
 * Email: hasan.sanusi@dansmultipro.com
 * Telegram: @hasansanusi
 * Date: 23/08/24
 * Time: 07.46
 * <p>
 * Created with IntelliJ IDEA
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AnggotaUser {
    private String npa;
    private String nama;
    private String role;
    private String pw;
    private String pd;
    private String pc;
}
