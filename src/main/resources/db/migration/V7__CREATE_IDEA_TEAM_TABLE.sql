CREATE TABLE t_idea_team
(
    id            BIGINT AUTO_INCREMENT NOT NULL,
    version       INT NOT NULL,
    updated_by    VARCHAR(255) NULL,
    updated_at    datetime NULL,
    created_by    VARCHAR(255) NULL,
    created_at    datetime NULL,
    name          VARCHAR(255) NULL,
    `description` VARCHAR(255) NULL,
    event         BIGINT NULL,
    team          VARCHAR(255) NULL,
    CONSTRAINT pk_t_idea_team PRIMARY KEY (id)
);

CREATE TABLE t_idea_team_detail
(
    id             BIGINT AUTO_INCREMENT NOT NULL,
    version        INT NOT NULL,
    updated_by     VARCHAR(255) NULL,
    updated_at     datetime NULL,
    created_by     VARCHAR(255) NULL,
    created_at     datetime NULL,
    participant_id VARCHAR(255) NULL,
    idea           BIGINT NULL,
    is_captain     BIT(1) NULL,
    CONSTRAINT pk_t_idea_team_detail PRIMARY KEY (id)
);