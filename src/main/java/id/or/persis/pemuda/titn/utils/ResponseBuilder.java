package id.or.persis.pemuda.titn.utils;

import org.springframework.http.HttpStatus;

public class ResponseBuilder<T> {
    public static <T> T buildResponse(HttpStatus status, long took, String message, Object data) {
        return (T) BaseResponse.builder()
                .status(status)
                .data(data)
                .message(message)
                .took(took)
                .build();
    }
}
