package id.or.persis.pemuda.titn.entity;

import jakarta.persistence.*;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

@Entity
@Table(name = "t_participant")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class Participant extends Updatable {
    @Id
    @Column(name = "participant_id", nullable = false)
    private String participantId;

    private String nik;

    private String npa;

    private String email;

    private String fullName;

    private String phone;

    @Enumerated(EnumType.STRING)
    private ClothSize clothesSize;

    private String pcCode;

    private String pcName;

    private String pdCode;

    private String pdName;

    private String pwCode;

    private String pwName;

    @Enumerated(EnumType.STRING)
    private Status status;

    private String photo;

    @Column(name = "notif_email_status")
    private String emailNotificationStatus;

    @Column(name = "notif_email_status_desc")
    private String emailNotificationStatusDesc;

    @Column(name = "notif_wa_status")
    private String waNotificationStatus;

    @Column(name = "notif_wa_status_desc")
    private String walNotificationStatusDesc;

    @Column(name = "is_deleted")
    private boolean isDeleted;

    @Column(name = "is_spinner")
    private boolean isSpinner;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "recap_id")
    @ToString.Exclude
    @NotFound(action = NotFoundAction.IGNORE)
    private ParticipantRecap recap;

    @OneToMany(mappedBy = "participant")
    private Set<ParticipantEvent> events;

    @PrePersist
    @PreUpdate
    private void convertFullNameToUpperCase() {
        this.fullName = this.fullName.toUpperCase();
    }
}
