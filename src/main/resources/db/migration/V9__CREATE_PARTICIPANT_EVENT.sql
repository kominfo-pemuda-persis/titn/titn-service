create table t_participant_event
(
    event_id       bigint       not null,
    participant_id varchar(100) not null,
    PRIMARY KEY (event_id, participant_id),
    constraint t_participant_event_fk
        foreign key (event_id) references t_event (id),
    constraint t_participant_fk
        foreign key (participant_id) references t_participant (participant_id)
);

