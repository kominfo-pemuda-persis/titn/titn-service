package id.or.persis.pemuda.titn.repository;

import id.or.persis.pemuda.titn.entity.Event;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EventRepository extends JpaRepository<Event, Long> {
    List<Event> findByIdIn(List<Long> ids);
}
