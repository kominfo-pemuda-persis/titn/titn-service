package id.or.persis.pemuda.titn.utils;

/**
 * Project: titn-service
 * <p>
 * User: hasan.sanusi
 * Email: hasan.sanusi@dansmultipro.com
 * Telegram: @hasansanusi
 * Date: 20/10/24
 * Time: 20.34
 * <p>
 * Created with IntelliJ IDEA
 */
public class ConstansTitnUtils {
    public static final String PESERTA_ESYAHADAH_TEMPLATE_HTML = "peserta-esyhadah-template.html";
}
