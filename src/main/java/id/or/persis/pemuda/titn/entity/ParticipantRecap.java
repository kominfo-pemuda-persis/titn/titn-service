package id.or.persis.pemuda.titn.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import jakarta.persistence.*;
import java.time.LocalDateTime;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * Project: titn-service
 * <p>
 * User: hasan.sanusi Email: hasan.sanusi@dansmultipro.com
 * Telegram: @hasansanusi Date: 13/06/24 Time: 19.33
 * <p>
 * Created with IntelliJ IDEA
 */
@Entity
@Table(name = "t_participant_recap")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class ParticipantRecap extends Updatable {
    @Id
    @Column(name = "recap_id", nullable = false)
    private String recapId;

    private String npa;

    private String email;

    private String fullName;

    private String phone;

    private String pcCode;

    private String pcName;

    private String pdCode;

    private String pdName;

    private String pwCode;

    private Integer nums;

    private String pwName;

    @Enumerated(EnumType.STRING)
    private Status status;

    private String buktiTransfer;

    private String rejectReason;

    private String approvedBy;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @UpdateTimestamp
    protected LocalDateTime approvedAt;

    @Column(name = "is_deleted")
    private boolean isDeleted;

    @PrePersist
    @PreUpdate
    private void convertFullNameToUpperCase() {
        this.fullName = this.fullName.toUpperCase();
    }
}
