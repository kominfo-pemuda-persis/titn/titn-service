create table t_participant_recap
(
    recap_id       varchar(100)                                                   not null
        primary key,
    npa            varchar(10)                                                    null,
    full_name      varchar(200)                                                   not null,
    phone          varchar(18)                                                    null,
    email          varchar(100)                                                   null,
    pc_code        varchar(8)                                                     null,
    pd_code        varchar(8)                                                     null,
    pw_code        varchar(8)                                                     null,
    pc_name        varchar(100)                                                   null,
    pd_name        varchar(100)                                                   null,
    pw_name        varchar(100)                                                   null,
    nums           int                                                            null,
    bukti_transfer varchar(100)                                                   null,
    created_at     timestamp(6)                                                   null,
    created_by     varchar(100)                                                   null,
    updated_at     timestamp(6)                                                   null,
    updated_by     varchar(100)                                                   null,
    approved_at     timestamp(6)                                                   null,
    approved_by     varchar(100)                                                   null,
    version        int                                                            null,
    is_deleted     tinyint(1)                                 default 0           null,
    status         enum ('SUBMITTED', 'APPROVED', 'REJECTED') default 'SUBMITTED' null,
    reject_reason  varchar(200)                                                   null
);

