create table t_configuration
(
    id            VARCHAR(100) NOT NULL primary key ,
    name          VARCHAR(100) NULL,
    `description` VARCHAR(255) NULL,
    value         VARCHAR(255) NULL,
    created_at     timestamp(6)                                                   null,
    created_by     varchar(100)                                                   null,
    updated_at     timestamp(6)                                                   null,
    updated_by     varchar(100)                                                   null,
    version        int
);
