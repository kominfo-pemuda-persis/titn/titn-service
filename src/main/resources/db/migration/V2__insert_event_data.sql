INSERT INTO t_event (name, type, description, created_by, updated_by, version) VALUES
('Futsal', 'OLAHRAGA', 'Event for Futsal competition', 'system', 'system', 0),
('Bola Voli', 'OLAHRAGA', 'Event for Bola Voli competition', 'system', 'system', 0),
('Tenis Meja', 'OLAHRAGA', 'Event for Tenis Meja competition', 'system', 'system', 0),
('Bulu Tangkis', 'OLAHRAGA', 'Event for Bulu Tangkis competition', 'system', 'system', 0),
('Panahan', 'OLAHRAGA', 'Event for Panahan competition', 'system', 'system', 0),
('Eksibisi Surulkan', 'OLAHRAGA', 'Event for Eksibisi Surulkan', 'system', 'system', 0),
('Balap Egrang', 'OLAHRAGA', 'Event for Balap Egrang competition', 'system', 'system', 0),
('Tarik Tambang', 'OLAHRAGA', 'Event for Tarik Tambang competition', 'system', 'system', 0),
('Lari Marathon', 'OLAHRAGA', 'Event for Lari Marathon competition', 'system', 'system', 0);

INSERT INTO t_event (name, type, description, created_by, updated_by, version) VALUES ('Musabaqoh Manhaj', 'ILMIAH',
                                                                                       'Event for Musabaqoh Manhaj',
                                                                                       'system', 'system', 0),
                                                                                      ('Fahmul Quran', 'ILMIAH',
                                                                                       'Event for Fahmul Quran',
                                                                                       'system', 'system', 0),
                                                                                      ('Qiroatul Kutub', 'ILMIAH',
                                                                                       'Event for Qiroatul Kutub',
                                                                                       'system', 'system', 0),
('Lomba Menulis Ilmiah/Kreatif', 'ILMIAH', 'Event for Lomba Menulis Ilmiah/Kreatif', 'system', 'system', 0),
('Lomba Debat', 'ILMIAH', 'Event for Lomba Debat', 'system', 'system', 0),
('Lomba Business Plan', 'ILMIAH', 'Event for Lomba Business Plan', 'system', 'system', 0),
('Lomba Pemetaan Dakwah', 'ILMIAH', 'Event for Lomba Pemetaan Dakwah', 'system', 'system', 0),
                                                                                      ('Lomba Digital Planning/Coding',
                                                                                       'ILMIAH',
                                                                                       'Event for Lomba Digital Planning/Coding',
                                                                                       'system', 'system', 0),
                                                                                      ('Lomba Konten Kreatif', 'ILMIAH',
                                                                                       'Event for Lomba Konten Kreatif',
                                                                                       'system', 'system', 0),
('Lomba Pembuatan Program', 'ILMIAH', 'Event for Lomba Pembuatan Program', 'system', 'system', 0);

INSERT INTO t_event (name, type, description, created_by, updated_by, version) VALUES
('Tidak mengikuti event apapun', 'ETC', 'Tidak mengikuti event apapun', 'system', 'system', 0),
('Panitia', 'ETC', 'Panitia', 'system', 'system', 0);


