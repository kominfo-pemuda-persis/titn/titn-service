package id.or.persis.pemuda.titn.service;

import id.or.persis.pemuda.titn.configuration.WhatsAppsConfig;
import id.or.persis.pemuda.titn.payload.*;
import id.or.persis.pemuda.titn.utils.BaseResponse;
import id.or.persis.pemuda.titn.utils.BusinessException;
import id.or.persis.pemuda.titn.utils.ResponseBuilder;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

/**
 * Project: titn-service
 * <p>
 * User: hasan.sanusi
 * Email: hasan.sanusi@dansmultipro.com
 * Telegram: @hasansanusi
 * Date: 14/07/24
 * Time: 07.17
 * <p>
 * Created with IntelliJ IDEA
 */
@Service
@Slf4j
public class MessageWhatsAppsService {
    public static final String IMAGE = "image";
    private final ParticipantService participantService;
    private final CoreApiService coreApiService;
    private final WhatsAppsConfig whatsAppsConfig;

    public MessageWhatsAppsService(
            ParticipantService participantService, CoreApiService coreApiService, WhatsAppsConfig whatsAppsConfig) {
        this.participantService = participantService;
        this.coreApiService = coreApiService;
        this.whatsAppsConfig = whatsAppsConfig;
    }

    public BaseResponse<MessageWaParticipantRequest> sendBlastMessageParticipant(MessageWaParticipantRequest request) {
        final long start = System.nanoTime();
        if (IMAGE.equalsIgnoreCase(request.getMessageType()) && StringUtils.isEmpty(request.getImageUrl())) {
            throw new BusinessException(HttpStatus.BAD_REQUEST, "IMAGE_URL_IS_NOT_EMPTY", "IMAGE_URL_IS_NOT_EMPTY");
        }
        List<List<String>> sendTos = participantService.getAllParticipantApproved(whatsAppsConfig.getMaxRequest());
        List<Map<String, Object>> resultList = executeSendingMessage(prepareDataBlast(request), sendTos);
        final long end = System.nanoTime();
        if (resultList.isEmpty()) {
            return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", request);
        }
        log.info("[PARTIAL_FAILED] with {}", resultList);
        return ResponseBuilder.buildResponse(
                HttpStatus.PARTIAL_CONTENT, ((end - start) / 1000000), "PARTIAL_FAILED", resultList);
    }

    private List<Map<String, Object>> executeSendingMessage(
            MessageWhatsAppsRequest request, List<List<String>> senTos) {
        List<Map<String, Object>> errorList = new ArrayList<>();
        List<CompletableFuture<Map<String, Object>>> futures = new ArrayList<>();
        senTos.forEach(contactList -> {
            CompletableFuture<Map<String, Object>> batchFutures = sendBatch(contactList, request);
            futures.add(batchFutures);
        });
        CompletableFuture<Void> batchFuture = CompletableFuture.allOf(futures.toArray(new CompletableFuture[0]));
        try {
            batchFuture.get();
            futures.forEach(future -> {
                try {
                    if (!future.get().isEmpty()) errorList.add(future.get());
                } catch (InterruptedException | ExecutionException e) {
                    log.info("[SEND MESSAGE ERROR ASYNC] {}", e.getMessage());
                }
            });
        } catch (InterruptedException | ExecutionException e) {
            log.info("[SEND MESSAGE ERROR ASYNC-2] {}", e.getMessage());
        }
        return errorList;
    }

    private CompletableFuture<Map<String, Object>> sendBatch(
            List<String> contactList, MessageWhatsAppsRequest request) {
        return executeAsync(contactList, request);
    }

    public CompletableFuture<Map<String, Object>> executeAsync(
            List<String> contactList, MessageWhatsAppsRequest messageWhatsAppsRequest) {
        ExecutorService pool = Executors.newFixedThreadPool(10);
        return CompletableFuture.supplyAsync(
                () -> {
                    Map<String, Object> error = new HashMap<>();
                    messageWhatsAppsRequest.setTarget(String.join(",", contactList));
                    MessageWhatsAppsResponse appsResponse =
                            coreApiService.executeSendMessageWa(messageWhatsAppsRequest);
                    if (!appsResponse.isStatus()) {
                        error.put("message", appsResponse.getMessage());
                        error.put("contactList", contactList);
                    }
                    log.info("[send blast message wa] {}", appsResponse);
                    return error;
                },
                pool);
    }

    private MessageWhatsAppsRequest prepareDataBlast(BaseMessageWaRequest request) {
        return MessageWhatsAppsRequest.builder()
                .token(whatsAppsConfig.getToken())
                .delay(Optional.ofNullable(request.getDelay())
                        .filter(integer -> integer > 0)
                        .map(String::valueOf)
                        .orElse(whatsAppsConfig.getDelay()))
                .message(request.getMessage())
                .type(request.getMessageType())
                .url(request.getImageUrl())
                .build();
    }

    public List<Map<String, Object>> executeSingleSendingMessage(List<MessageWhatsAppsRequest> request) {
        List<Map<String, Object>> errorList = new ArrayList<>();
        List<CompletableFuture<Map<String, Object>>> futures = new ArrayList<>();
        request.forEach(messageWhatsAppsRequests -> {
            CompletableFuture<Map<String, Object>> batchFutures = sendBatchSingle(messageWhatsAppsRequests);
            futures.add(batchFutures);
        });
        CompletableFuture<Void> batchFuture = CompletableFuture.allOf(futures.toArray(new CompletableFuture[0]));
        try {
            batchFuture.get();
            futures.forEach(future -> {
                try {
                    if (!future.get().isEmpty()) errorList.add(future.get());
                } catch (InterruptedException | ExecutionException e) {
                    log.info("[SEND MESSAGE ERROR ASYNC] {}", e.getMessage());
                }
            });
        } catch (InterruptedException | ExecutionException e) {
            log.info("[SEND MESSAGE ERROR ASYNC-2] {}", e.getMessage());
        }
        return errorList;
    }

    public CompletableFuture<Map<String, Object>> sendBatchSingle(MessageWhatsAppsRequest messageWhatsAppsRequest) {
        ExecutorService pool = Executors.newFixedThreadPool(10);
        return CompletableFuture.supplyAsync(
                () -> {
                    Map<String, Object> result = new HashMap<>();
                    String status = "SUCCESS";
                    String statusDescription = "Whats apps sent successfully";
                    try {
                        MessageWhatsAppsResponse appsResponse =
                                coreApiService.executeSendMessageWa(messageWhatsAppsRequest);
                        log.info("[send message wa] {}", appsResponse);
                        if (!appsResponse.isStatus()) {
                            result.put("message", appsResponse.getMessage());
                            result.put("contactList", messageWhatsAppsRequest.getTarget());
                            status = "FAILED";
                            statusDescription = appsResponse.getMessage();
                        } else {
                            result.put("message", "success");
                            result.put("contactList", messageWhatsAppsRequest.getTarget());
                        }
                        return result;
                    } catch (Exception e) {
                        log.info("[ERROR send message wa] {}", e.getMessage());
                        result.put("message", StringUtils.substring(e.getMessage(), 0, 100));
                        result.put("contactList", messageWhatsAppsRequest.getTarget());
                        status = "FAILED";
                        statusDescription = StringUtils.substring(e.getMessage(), 0, 100);
                        return result;
                    } finally {
                        if (!messageWhatsAppsRequest.getMessage().contains("Calon")
                                && Objects.nonNull(messageWhatsAppsRequest.getParticipantId())) {
                            participantService.updateWaStatus(
                                    messageWhatsAppsRequest.getParticipantId(), status, statusDescription);
                        }
                    }
                },
                pool);
    }
}
