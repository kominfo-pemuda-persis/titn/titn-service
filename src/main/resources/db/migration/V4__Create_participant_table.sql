create table t_participant
(
    participant_id varchar(100)                                                   not null
        primary key,
    nik            varchar(16)                                                    null,
    npa            varchar(10)                                                    null,
    full_name      varchar(200)                                                   not null,
    phone          varchar(18)                                                    null,
    email          varchar(100)                                                   null,
    clothes_size   enum ('S', 'M', 'L','XL','XXL','XXXL')                         null,
    pc_code        varchar(8)                                                     null,
    pd_code        varchar(8)                                                     null,
    pw_code        varchar(8)                                                     null,
    pc_name        varchar(100)                                                   null,
    pd_name        varchar(100)                                                   null,
    pw_name        varchar(100)                                                   null,
    photo          varchar(200)                                                   null,
    event          bigint                                                         null,
    created_at     timestamp(6)                                                   null,
    created_by     varchar(100)                                                   null,
    updated_at     timestamp(6)                                                   null,
    updated_by     varchar(100)                                                   null,
    version        int                                                            null,
    is_deleted     tinyint(1)                                 default 0           null,
    status         enum ('SUBMITTED', 'APPROVED', 'REJECTED','CHECKED_IN') default 'SUBMITTED' null,
    recap_id       varchar(100)                                                   null,
    constraint email
        unique (email),
    constraint t_participant_t_event_id_fk
        foreign key (event) references t_event (id),
    constraint t_participant_t_participant_recap_null_fk
        foreign key (recap_id) references t_participant_recap (recap_id)
);