package id.or.persis.pemuda.titn.repository;

import id.or.persis.pemuda.titn.entity.Configuration;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Project: titn-service
 * <p>
 * User: hasan.sanusi
 * Email: hasan.sanusi@dansmultipro.com
 * Telegram: @hasansanusi
 * Date: 11/08/24
 * Time: 22.59
 * <p>
 * Created with IntelliJ IDEA
 */
@Repository
public interface ConfigurationRepository extends JpaRepository<Configuration, String> {}
