package id.or.persis.pemuda.titn.repository;

import id.or.persis.pemuda.titn.entity.Participant;
import id.or.persis.pemuda.titn.entity.Status;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface ParticipantRepository extends JpaRepository<Participant, String> {
    @Query(value = "select t from Participant t where t.nik=:nik and t.isDeleted=false")
    Optional<Participant> findByNik(String nik);

    @Query(value = "select t from Participant t where t.email=:email and t.isDeleted=false")
    Optional<Participant> findByEmail(String email);

    @Query(value = "select t from Participant t where t.npa=:npa and t.isDeleted=false")
    Optional<Participant> findByNpa(String npa);

    @Query(
            value =
                    "select t from Participant t join fetch t.events  where ( t.npa=:npa or t.participantId=:npa ) and t.isDeleted=false and (t.status = 'APPROVED'OR t.status='CHECKED_IN')")
    Optional<Participant> findByNpaOrId(String npa);

    @Query(
            value =
                    "select t from Participant t JOIN FETCH t.events where t.participantId=:participantId and t.isDeleted=false")
    Optional<Participant> findByParticipantIdDeletedFalse(String participantId);

    @Query(
            value =
                    "select t from Participant t where t.participantId=:participantId and t.isDeleted=false and t.status=:status")
    Optional<Participant> findByParticipantIdAndStatusAndDeletedFalse(String participantId, Status status);

    @Modifying
    @Query(
            value =
                    "update t_participant p set p.status =:status , p.updated_at=:updatedAt where p.recap_id=:recapId and p.is_deleted=false",
            nativeQuery = true)
    void updateStatusByRecapId(String recapId, String status, LocalDateTime updatedAt);

    @Modifying
    @Query(
            value =
                    "update t_participant p set p.recap_id =:recapId , p.updated_at=:updatedAt where p.participant_id in (:participantIds) and p.is_deleted=false",
            nativeQuery = true)
    void updateRecapByParticipantIds(String recapId, List<String> participantIds, LocalDateTime updatedAt);

    @Query(value = "select t from Participant t where t.npa in(:npaIdList)  and t.isDeleted=false")
    List<Participant> findAllByNpaList(List<String> npaIdList);

    Page<Participant> findAll(Specification<Participant> specification, Pageable pageable);

    @Modifying
    @Query(
            value =
                    "update t_participant p set p.recap_id =null , p.updated_at=:updatedAt where p.recap_id =:recapId and p.is_deleted=false",
            nativeQuery = true)
    void updateByRecapId(String recapId, LocalDateTime updatedAt);

    @Query("select t from Participant t where t.status='APPROVED'")
    Slice<Participant> findAllByParticipantApprove(Pageable pageable);

    @Query("select t from Participant t JOIN FETCH t.events where t.recap.recapId=:recapId ")
    List<Participant> findContactParticipantByRecapId(String recapId);

    @Query(
            "select t from Participant t JOIN FETCH t.events where t.recap.recapId=:recapId and (t.status = 'APPROVED'OR t.status='CHECKED_IN')")
    List<Participant> findContactParticipantByRecapIdPeserta(String recapId);

    @Transactional
    @Modifying
    @Query(
            value =
                    "update t_participant p set p.notif_wa_status =:status, p.notif_wa_status_desc =:statusDesc , p.updated_at=:updatedAt where p.participant_id=:participantId and p.is_deleted=false",
            nativeQuery = true)
    void updateStatusWaByParticipantId(String participantId, String status, String statusDesc, LocalDateTime updatedAt);

    @Transactional
    @Modifying
    @Query(
            value =
                    "update t_participant p set p.notif_email_status =:status, p.notif_email_status_desc =:statusDesc , p.updated_at=:updatedAt where p.participant_id=:participantId and p.is_deleted=false",
            nativeQuery = true)
    void updateStatusEmailByParticipantId(
            String participantId, String status, String statusDesc, LocalDateTime updatedAt);
}
