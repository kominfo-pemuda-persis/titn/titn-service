package id.or.persis.pemuda.titn.payload;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Project: titn-service
 * <p>
 * User: hasan.sanusi
 * Email: hasan.sanusi@dansmultipro.com
 * Telegram: @hasansanusi
 * Date: 17/07/24
 * Time: 20.10
 * <p>
 * Created with IntelliJ IDEA
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class DashboardSummaryResponse {
    private long totalJumlahCalonPeserta;
    private long totalJumlahCalonPesertaAnggota;
    private long totalJumlahCalonPesertaNonAnggota;
    private long totalJumlahPeserta;
    private long totalJumlahPesertaAnggota;
    private long totalJumlahPesertaNonAnggota;
    private long totalJumlahPesertaAnggotaChecked;
    private long totalJumlahPesertaNonAnggotaChecked;
    private long totalJumlahPesertaChecked;
    private Long totalJumlahPanitiaChecked;
    private Long totalJumlahPanitia;
    private Long totalJumlahCalonPanitia;
    private Long totalJumlahPengujungChecked;
    private Long totalJumlahPengunjung;
    private Long totalJumlahCalonPengunjung;

    private List<Type> eventSummary;

    private List<DashboardPcSummary> clothesSizeSummary;

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Builder
    public static class Type {
        private String name;
        private List<Name> values;
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Builder
    public static class Name {
        private String name;
        private Long value;
    }
}
