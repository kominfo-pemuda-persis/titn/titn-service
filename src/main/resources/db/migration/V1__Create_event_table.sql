CREATE TABLE t_event (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    name varchar(100) NOT NULL,
    type ENUM('OLAHRAGA', 'ILMIAH', 'ETC') NOT NULL,
    description TEXT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    updated_by varchar(100) NULL,
    version int NULL,
    created_by varchar(100) NULL
);
