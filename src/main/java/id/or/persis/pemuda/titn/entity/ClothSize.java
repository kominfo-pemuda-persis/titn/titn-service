package id.or.persis.pemuda.titn.entity;

/**
 * Created by IntelliJ IDEA. Project : titn-service User: hendisantika Email:
 * hendisantika@gmail.com Telegram : @hendisantika34 Date: 6/9/24 Time: 17:07 To
 * change this template use File | Settings | File Templates.
 */
public enum ClothSize {
    S("S", "Small"),
    M("M", "Middle"),
    L("L", "Large"),
    XL("XL", "Xtra Large"),
    XXL("XXL", "Double Xtra Large"),
    XXXL("XXXL", "Tripple Xtra Large");

    public final String code;
    public final String desc;

    ClothSize(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
