package id.or.persis.pemuda.titn.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Created by IntelliJ IDEA. Project : titn-service User: hendisantika Email:
 * hendisantika@gmail.com Telegram : @hendisantika34 Date: 5/21/23 Time: 13:35
 * To change this template use File | Settings | File Templates.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CreateAccountRequest {
    String name;
    String mobile;
    String email;
    String password;
}
