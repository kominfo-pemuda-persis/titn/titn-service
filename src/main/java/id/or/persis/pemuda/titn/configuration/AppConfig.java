package id.or.persis.pemuda.titn.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties("apps.titn")
public class AppConfig {
    private String whitelistCrossUrl;
    private String whitelistCrossMethod;
    private String emailFromAddress;
    private String emailFromName;
    private String baseUrl;
    private String apiCheckin;
    private String eventIdNonAnggotas;
    private Long noEventId;
    private String jwtSecret;
    private String whitelistJwtPathUrl;
}
