package id.or.persis.pemuda.titn.payload;

public class ConstanUtils {
    public static final String TEMPLATE_APPROVE_PARTICIPANT =
            "Selamat  *{0}*, antum telah terdaftar sebagai Peserta TITN 14 Pemuda Persis dengan Kode Peserta *{1}* untuk event *{2}*. Semoga bisa mengikuti kegiatan dengan tertib, aman dan lancar sampai tuntas dan tetap jalin silaturrahim.\n"
                    + "\n"
                    + "Panitia TITN 14 \n"
                    + "Pemuda Persis\n"
                    + "\n"
                    + "https://titn2024.persis.or.id";
    public static final String TEMPLATE_REGISTER_PARTICIPANT =
            "Selamat *{0}*, antum telah terdaftar sebagai Calon Peserta TITN 14 Pemuda Persis dengan Kode Calon Peserta *{1}* untuk event *{2}*. Semoga bisa mengikuti kegiatan dengan tertib, aman dan lancar sampai tuntas dan tetap jalin silaturrahim.\n"
                    + "\n"
                    + "Panitia TITN 14 \n"
                    + "Pemuda Persis.\n"
                    + "\n"
                    + "أنا مسلم قبل كل شيء\n"
                    + "\n"
                    + "https://titn2024.persis.or.id";
    public static final String TEMPLATE_RECAP =
            "Selamat *{0}*, Recap Peserta TITN 2024 Pemuda PERSIS telah kami terima. Ikuti terus status approvalnya.\n"
                    + "Semoga bisa mengikuti kegiatan dengan tertib, aman dan lancar sampai tuntas dan tetap jalin silaturrahim.\n"
                    + "\n"
                    + "Panitia TITN 14 \n"
                    + "Pemuda Persis.\n"
                    + "\n"
                    + "أنا مسلم قبل كل شيء\n"
                    + "\n"
                    + "https://titn2024.persis.or.id";

    public static final String TEMPLATE_ESYAHADAH = "*بسم الله الرحمن الرحيم*" + "\n"
            + "Alhamdulillah, segala puji bagi Allah yang telah memberikan kemudahan kepada kita semua dalam menjalankan setiap aktivitas dan acara yang kita laksanakan. Kami dari panitia *TITN 2024 Pemuda PERSIS* mengucapkan terima kasih atas partisipasi dan kontribusi Anda dalam kegiatan ini\n"
            + "\n"
            + "Sebagai tanda penghargaan dan apresiasi kami, dengan senang hati kami lampirkan *sertifikat* partisipasi Anda. Semoga kegiatan ini menjadi bekal ilmu dan amal yang bermanfaat, serta menjadi bagian dari ibadah yang diridhai Allah SWT.\n"
            + "\n"
            + "*Panitia TITN 14*"
            + "\n"
            + "*Pemuda Persis 2024*\n"
            + "\n"
            + "*أنا مسلم قبل كل شيء*"
            + "\n"
            + "https://titn2024.persis.or.id";
}
