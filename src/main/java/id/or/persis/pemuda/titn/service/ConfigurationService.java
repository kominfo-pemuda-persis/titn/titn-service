package id.or.persis.pemuda.titn.service;

import id.or.persis.pemuda.titn.payload.ConfigurationResponseDto;
import id.or.persis.pemuda.titn.repository.ConfigurationRepository;
import id.or.persis.pemuda.titn.utils.BaseResponse;
import id.or.persis.pemuda.titn.utils.BusinessException;
import id.or.persis.pemuda.titn.utils.GlobalMessage;
import id.or.persis.pemuda.titn.utils.ResponseBuilder;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

/**
 * Project: titn-service
 * <p>
 * User: hasan.sanusi
 * Email: hasan.sanusi@dansmultipro.com
 * Telegram: @hasansanusi
 * Date: 11/08/24
 * Time: 23.02
 * <p>
 * Created with IntelliJ IDEA
 */
@Service
public class ConfigurationService {
    private final ConfigurationRepository configurationRepository;

    public ConfigurationService(ConfigurationRepository configurationRepository) {
        this.configurationRepository = configurationRepository;
    }

    public BaseResponse<ConfigurationResponseDto> getExpiredRegistration() {
        final long start = System.nanoTime();
        ConfigurationResponseDto configuration = configurationRepository
                .findById("EXPIRED_REGISTRATION")
                .map(data -> {
                    String status;
                    try {
                        LocalDateTime localDateTime = LocalDateTime.parse(
                                data.getValue(), DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss"));
                        status = localDateTime.isAfter(LocalDateTime.now()) ? "ACTIVE" : "EXPIRED";
                    } catch (Exception e) {
                        throw new BusinessException("EXPIRED_REGISTRATION_NOT_VALID_VALUE");
                    }
                    return ConfigurationResponseDto.builder()
                            .name(data.getName())
                            .value(data.getValue())
                            .status(status)
                            .build();
                })
                .orElseThrow(() -> new BusinessException("EXPIRED_REGISTRATION_NOT_CONFIGURE"));

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(
                HttpStatus.OK, ((end - start) / 1000000), GlobalMessage.SUCCESS.message, configuration);
    }
}
