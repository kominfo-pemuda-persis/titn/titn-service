create index t_event_nm_index
    on t_event (name);

create index t_event_type_index
    on t_event (type);

create index t_participant_em_index
    on t_participant (email);

create index t_participant_full_index
    on t_participant (full_name);

create index t_participant_nik_index
    on t_participant (nik);

create index t_participant_npa_index
    on t_participant (npa);

create index t_participant_pc_index
    on t_participant (pc_code);

create index t_participant_pcn_index
    on t_participant (pc_name);

create index t_participant_pd_index
    on t_participant (pd_code);

create index t_participant_pdn_index
    on t_participant (pd_name);

create index t_participant_ph_index
    on t_participant (phone);

create index t_participant_pw_index
    on t_participant (pw_code);

create index t_participant_pwn_index
    on t_participant (pw_name);

create index t_participant_st_index
    on t_participant (status);
create index t_participant_recap_em_index
    on t_participant_recap (email);

create index t_participant_recap_full_index
    on t_participant_recap (full_name);

create index t_participant_recap_pc_index
    on t_participant_recap (pc_code);

create index t_participant_recap_pcn_index
    on t_participant_recap (pc_name);

create index t_participant_recap_pd_index
    on t_participant_recap (pd_code);

create index t_participant_recap_pdn_index
    on t_participant_recap (pd_name);

create index t_participant_recap_ph_index
    on t_participant_recap (phone);

create index t_participant_recap_pw_index
    on t_participant_recap (pw_code);

create index t_participant_recap_pwn_index
    on t_participant_recap (pw_name);

create index t_participant_recap_st_index
    on t_participant_recap (status);