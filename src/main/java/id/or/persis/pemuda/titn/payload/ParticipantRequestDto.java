package id.or.persis.pemuda.titn.payload;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import id.or.persis.pemuda.titn.utils.validation.ValidateString;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class ParticipantRequestDto {
    private String participantId;
    private String npa;

    @NotEmpty(message = "FULL_NAME_NOT_EMPTY")
    private String fullName;

    @NotEmpty(message = "EMAIL_NOT_EMPTY")
    @Email(message = "EMAIL_NOT_VALID")
    private String email;

    private String phone;
    private String nik;
    private List<Long> eventIds;
    private String photo;

    @NotEmpty(message = "CLOTH_SIZE_NOT_EMPTY")
    @ValidateString(
            acceptedValues = {"S", "M", "L", "XL", "XXL", "XXXL"},
            message = "CLOTHES_SIZE_MUST_MATCH:{acceptedValues}")
    private String clothesSize;

    private String pcCode;
    private String pcName;
    private String pdCode;
    private String pdName;
    private String pwCode;
    private String pwName;
}
